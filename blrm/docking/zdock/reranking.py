import glob

import numpy
from matplotlib.pyplot import plot, show, legend

from blrm.configuration import zdock_dbd5_contacts_directory, pairpred_docking_directory, \
    zdock_decoys_bm5_output_directory


def give_contacts(line):
    parts = line.split()[1:]
    receptor_res = set()
    ligand_res = set()
    for part in parts:
        receptor, ligand = part.split("_")
        receptor_res.add(receptor)
        ligand_res.add(ligand)
    return ligand_res, receptor_res


def give_patch_residue_dict(complex_code):
    patch_file_name = pairpred_docking_directory + "{}_patches.csv".format(complex_code)
    dictionary = {}
    with open(patch_file_name) as patch_file:
        for line in patch_file:
            parts = line.strip().split(",")
            residues_list = []
            for part in parts[2:]:
                chain_residue_id = "{}-{}".format(part.split("-")[1], part.split("-")[2])
                residues_list.append(chain_residue_id)
            if parts[0] == "ligand":
                dictionary["l_{}".format(parts[1].strip())] = residues_list
            else:
                dictionary["r_{}".format(parts[1].strip())] = residues_list
    return dictionary


def load_pairpred_residue_contacts(complex_code, first_n=20):
    ligand_contact, receptor_contact = {}, {}
    for i in range(first_n):
        ligand_contact[i] = []
        receptor_contact[i] = []
    scores_file = pairpred_docking_directory + "{}_residue_scores.csv".format(complex_code)
    with open(scores_file) as f:
        for i, line in enumerate(f):
            l_patch, r_patch = line.split(",")[:2]
            l_patch = "-".join(l_patch.split("-")[1:])
            r_patch = "-".join(r_patch.split("-")[1:])
            l_patch, r_patch = l_patch.strip(), r_patch.strip()
            for j in range(i, first_n):
                ligand_contact[j].append(l_patch)
                receptor_contact[j].append(r_patch)
    return ligand_contact, receptor_contact

# def load_pairpred_contacts(complex_code, first_n=5):
#     ligand_contact, receptor_contact = {}, {}
#     for i in range(first_n):
#         ligand_contact[i] = []
#         receptor_contact[i] = []
#     patch_scores_file = pairpred_docking_directory + "{}_patch_scores.csv".format(complex_code)
#     patch_residue_dict = give_patch_residue_dict(complex_code)
#     with open(patch_scores_file) as patch_scores_file:
#         for i, line in enumerate(patch_scores_file):
#             l_patch, r_patch = line.split(",")[:2]
#             for j in range(i, first_n):
#                 ligand_contact[j].extend(patch_residue_dict["l_{}".format(int(float(l_patch)))])
#                 receptor_contact[j].extend(patch_residue_dict["r_{}".format(int(float(r_patch)))])
#     return ligand_contact, receptor_contact


def compute_jaccard_index(contacts, pairpred_contacts):
    a = set(contacts)
    b = set(pairpred_contacts)
    return len(a.intersection(b)) / float(len(a.union(b)))


def read_rmsds_file(zdock_output_file_name):
    rmsds = {}
    with open("{}{}.zd3.0.2.fg.fixed.out.rmsds".format(zdock_decoys_bm5_output_directory,
                                                       zdock_output_file_name)) as rmsd_file:
        for line in rmsd_file:
            prediction, rmsd = line.split()[:2]
            rmsds[int(prediction)] = float(rmsd)
    return rmsds


def jaccard_index_reranking(n=20):
    untouched_percentage = []
    for i in range(n):
        untouched_percentage = []
        reranking = {}
        untouched_reranking = {}
        for output_file_name in glob.glob(zdock_dbd5_contacts_directory + "*"):
            new_ranks = numpy.zeros((2000, 2))
            complex_code = output_file_name.split("/")[-1].split(".")[0]
            rmsds = read_rmsds_file(complex_code)
            pairpred_ligand_contacts, pairpred_receptor_contacts = load_pairpred_residue_contacts(complex_code)
            with open(output_file_name) as output_file:
                j = 0
                for line in output_file:
                    ligand_contacts, receptor_contacts = give_contacts(line)
                    jaccard_index = 0.5 * compute_jaccard_index(ligand_contacts, pairpred_ligand_contacts[
                        i]) + 0.5 * compute_jaccard_index(receptor_contacts, pairpred_receptor_contacts[i])
                    new_ranks[j, :] = j, jaccard_index
                    j += 1
            new_ranks = new_ranks[new_ranks[:, 1].argsort()][:, 0]
            reranking[complex_code] = []
            untouched_reranking[complex_code] = []
            for index in range(new_ranks.shape[0]):
                reranking[complex_code].append(rmsds[new_ranks[index] + 1])
                untouched_reranking[complex_code].append(rmsds[index + 1])

        table = numpy.array(reranking.values())
        untouched_table = numpy.array(untouched_reranking.values())
        n_complexes = table.shape[0]
        flags = numpy.zeros((n_complexes, 1))
        untouched_flags = numpy.zeros((n_complexes, 1))
        percentage = []
        for j in range(2000):
            indices = numpy.where(table[:, j] < 5)
            untouched_indices = numpy.where(untouched_table[:, j] < 5)
            flags[indices] += 1
            flags[numpy.where(flags > 1)] = 1
            untouched_flags[untouched_indices] += 1
            untouched_flags[numpy.where(untouched_flags > 1)] = 1
            percentage.append(100 * numpy.sum(flags) / float(n_complexes))
            untouched_percentage.append(100 * numpy.sum(untouched_flags) / float(n_complexes))

        # print percentage
        # print untouched_percentage
        plot(percentage, label="Top {} Patches".format(i + 1))
    plot(untouched_percentage, label="Regular")
    legend()
    show()


def read_decoys_contacts(contacts_file):
    with open(contacts_file) as output_file:
        j = 0
        for line in output_file:
            ligand_contacts, receptor_contacts = give_contacts(line)


def push_top_k(new_ranks, k):
    final_rank = numpy.zeros(new_ranks.shape)
    final_rank[:k] = new_ranks[:k]
    top_k_set = set(new_ranks[:k])
    i = 0
    pointer = 0
    for i in range(new_ranks.shape[0]):
        if i not in top_k_set:
            final_rank[k + pointer] = int(i)
            pointer += 1
    return final_rank


def jaccard_index_pushing(n=5, top_k=10):
    """
    :param n: 1 to n top scoring patches of blrm will be considered for the interface intersection
    @type top_k: integer the number of higher decoys to push up in the list
    """
    area_under_the_curves = []
    untouched_percentage = []
    for i in range(n):
        print i
        untouched_percentage = []
        reranking = {}
        original_ranking = {}
        for output_file_name in glob.glob(zdock_dbd5_contacts_directory + "*"):
            new_ranks = numpy.zeros((2000, 2))
            complex_code = output_file_name.split("/")[-1].split(".")[0]
            rmsds = read_rmsds_file(complex_code)
            pairpred_ligand_contacts, pairpred_receptor_contacts = load_pairpred_residue_contacts(complex_code)
            with open(output_file_name) as output_file:
                j = 0
                for line in output_file:
                    ligand_contacts, receptor_contacts = give_contacts(line)
                    ligand_score = compute_jaccard_index(ligand_contacts, pairpred_ligand_contacts[i])
                    receptor_score = compute_jaccard_index(receptor_contacts, pairpred_receptor_contacts[i])
                    jaccard_index = 0.5 * (ligand_score + receptor_score)
                    new_ranks[j, :] = j, jaccard_index
                    j += 1
            new_ranks = new_ranks[new_ranks[:, 1].argsort()][:, 0]
            new_ranks = push_top_k(new_ranks, top_k)
            reranking[complex_code] = []
            original_ranking[complex_code] = []
            for index in range(new_ranks.shape[0]):
                reranking[complex_code].append(rmsds[new_ranks[index] + 1])
                original_ranking[complex_code].append(rmsds[index + 1])

        table = numpy.array(reranking.values())
        untouched_table = numpy.array(original_ranking.values())
        n_complexes = table.shape[0]
        flags = numpy.zeros((n_complexes, 1))
        untouched_flags = numpy.zeros((n_complexes, 1))
        percentage = []
        for j in range(2000):
            indices = numpy.where(table[:, j] < 5)
            untouched_indices = numpy.where(untouched_table[:, j] < 5)
            flags[indices] += 1
            flags[numpy.where(flags > 1)] = 1
            untouched_flags[untouched_indices] += 1
            untouched_flags[numpy.where(untouched_flags > 1)] = 1
            percentage.append(100 * numpy.sum(flags) / float(2000 * n_complexes))
            untouched_percentage.append(100 * numpy.sum(untouched_flags) / float(2000 * n_complexes))
        area_under_the_curves.append(numpy.sum(percentage))
        # print percentage
        # print untouched_percentage
        plot(percentage, label="Top {} Patches".format(i + 1))
    plot(untouched_percentage, label="Regular")
    area_under_the_curves.append(numpy.sum(untouched_percentage))
    print area_under_the_curves
    legend()
    show()


def main():
    # jaccard_index_reranking()
    jaccard_index_pushing()
    # l = numpy.array([12.0, 10.0, 5.0, 4.0, 6.0, 0.0, 3.0, 11.0, 2.0, 9.0, 7.0, 8.0, 1.])
    # print l
    # print list(push_top_k(l, 3))


if __name__ == '__main__':
    main()
