import glob

import os

from blrm.configuration import fast_pl_script, zdock_decoys_bm5_output_directory, zdock_dbd5_contacts_directory


def rerun():
    for zdock_output_file in glob.glob("{}/*.fixed.out".format(zdock_decoys_bm5_output_directory)):
        complex_code = zdock_output_file.split("/")[-1][:4]
        receptor = "{}{}_r_u.pdb".format(zdock_decoys_bm5_output_directory, complex_code)
        ligand = "{}{}_l_u.pdb".format(zdock_decoys_bm5_output_directory, complex_code)
        contact_file = "{}{}_all.out".format(zdock_dbd5_contacts_directory, complex_code)
        command = "{} {} {} {} 1 50000 > {}".format(fast_pl_script,
                                                    zdock_output_file,
                                                    receptor,
                                                    ligand,
                                                    contact_file)
        os.system(command)


def main():
    rerun()


if __name__ == '__main__':
    main()
