
import glob

import numpy
from PyML.evaluators.roc import roc
from matplotlib.pyplot import plot, show, legend
from numpy import zeros
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve

from blrm.configuration import pairpred_docking_directory
from blrm.data.dbd.complex_full_id_mapper import ComplexFullIDMapper

def main():
    for residue_score_file_name in glob.glob(pairpred_docking_directory + "*_residue_scores.csv"):
        with open(residue_score_file_name) as residue_score_file:
            complex_code = residue_score_file_name.split("/")[-1][:4]
            print complex_code
            bound_residue_score_file_name = residue_score_file_name.replace("_residue_scores.csv",
                                                                            "_residue_scores_bound.csv")
            bound_residue_score_file = open(bound_residue_score_file_name, "w")
            mapper = ComplexFullIDMapper(None).load(complex_code)
            for line in  residue_score_file:
                parts = line.split(", ")
                ligand_res, receptor_res, score = parts
                bound_ligand_res = mapper[ligand_res]
                bound_receptor_res = mapper[receptor_res]
                bound_residue_score_file.write("{}, {}, {}".format(bound_ligand_res, bound_receptor_res, score))
            bound_residue_score_file.close()


if __name__ == '__main__':
    main()
