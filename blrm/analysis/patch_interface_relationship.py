import numpy as np
import pickle
from blrm.model.protein import Protein
from blrm.configuration import database_pdb_directory, categories_pickle_filename
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.patch_extractor import PatchExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor



def compute_patch_overlaps_with_interface(complexes, configurations):
    overlaps = {}
    for configuration in configurations:
        avg = 0
        for a_complex in complexes:
            proteins = ["{}_l_u".format(a_complex), "{}_r_u".format(a_complex)]
            for protein in proteins:
                protein_object = Protein(*PDB.read_pdb_file(database_pdb_directory + protein + ".pdb"))
                PatchExtractor(protein_object, **configuration).extract()
                interface = TrueInterfaceExtractor(None).load(protein)
                patches = PatchExtractor(None, **configuration).load(protein)
                avg += len(np.unique(patches[np.where(interface > 0)[0]]))
                # todo recheck inteface patch computations
        overlaps[str(configuration)] = avg / (2.0 * len(complexes))
        print configuration, overlaps[str(configuration)]
    return overlaps


def main():
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    complexes = training
    configurations = [
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 1.0,
        #     'ss_weight': 0.0,
        #     'seq_weight': 0.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 0.0,
        #     'ss_weight': 1.0,
        #     'seq_weight': 0.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 0.0,
        #     'ss_weight': 0.0,
        #     'seq_weight': 1.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 1.0,
        #     'ss_weight': 1.0,
        #     'seq_weight': 0.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 0.0,
        #     'ss_weight': 1.0,
        #     'seq_weight': 1.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 1.0,
        #     'ss_weight': 0.0,
        #     'seq_weight': 1.0,
        # },
        # {
        #     'verbose': False,
        #     'pymol': True,
        #     'dist_weight': 1.0,
        #     'ss_weight': 1.0,
        #     'seq_weight': 1.0,
        # },
        {
            'verbose': False,
            'pymol': True,
            'dist_weight': 0.7,
            'ss_weight': 0.2,
            'seq_weight': 0.1,
            'seed': 10000
        },
        {
            'verbose': False,
            'pymol': True,
            'dist_weight': 0.7,
            'ss_weight': 0.2,
            'seq_weight': 0.1,
            'seed': 1000
        },
        {
            'verbose': False,
            'pymol': True,
            'dist_weight': 0.7,
            'ss_weight': 0.2,
            'seq_weight': 0.1,
            'seed': 100
        },
        {
            'verbose': False,
            'pymol': True,
            'dist_weight': 0.7,
            'ss_weight': 0.2,
            'seq_weight': 0.1,
            'seed': 10
        },
        {
            'verbose': False,
            'pymol': True,
            'dist_weight': 0.7,
            'ss_weight': 0.2,
            'seq_weight': 0.1,
            'seed': 1
        }
    ]
    results = compute_patch_overlaps_with_interface(complexes, configurations)
    print results

if __name__ == '__main__':
    main()
