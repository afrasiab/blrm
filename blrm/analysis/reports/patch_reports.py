import numpy as np
import pickle
from collections import Counter
from blrm.model.protein import Protein
from blrm.configuration import categories_pickle_filename
from blrm.configuration import verbose, database_pdb_directory, results_prediction_directory
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.patch_extractor import PatchExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def patch_true_interface_overlap():
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    distribution = {}
    for complex_code in training + testing:
        distribution[complex_code] = 0.
        proteins = [complex_code + "_l_u", complex_code + "_r_u"]
        for protein in proteins:
            true_interface = TrueInterfaceExtractor(None).load(protein)
            patches = PatchExtractor(None).load(protein)[0]

            n = len(np.unique(patches))
            sizes = np.array([sum(patches == c) for c in range(n)])
            difference = 0.0
            count = 0
            for i in range(n):
                patch_mask = np.zeros(true_interface.shape)
                patch_mask[(patches == i)] = 1
                n_p = float(np.sum(patch_mask))
                patch_intersection = np.sum(true_interface.T.dot(patch_mask))
                if patch_intersection > 0:
                    count += 1
                    difference += (n_p - patch_intersection) / n_p
            difference /= float(count)
            distribution[complex_code] += (difference / 2.)
    print np.median(distribution)
    print np.mean(distribution)
    print np.std(distribution)
    print np.max(distribution)
    print np.min(distribution)
    print distribution


def patch_size_distribution():
    import matplotlib.pyplot as plt
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    mean_distribution = []
    std_distribution = []
    complexes = training
    for complex_code in complexes:
        proteins = [complex_code + "_l_u", complex_code + "_r_u"]
        for protein in proteins:
            # protein_object = Protein(*PDB.read_pdb_file(database_pdb_directory + protein + ".pdb"))
            # PatchExtractor(protein_object, mean_patch_size=2).extract()
            patch = list(PatchExtractor(None, mean_patch_size=2, other_kernels=None).load(protein))
            counter = Counter(patch)
            patches_sizes = counter.values()

            mean = np.mean(patches_sizes)
            std = np.std(patches_sizes)
            print_patch_info(protein, mean, std, patches_sizes)
            mean_distribution.append(mean)
            std_distribution.append(std)
    print np.mean(mean_distribution)
    print np.mean(std_distribution)
    plt.hist(mean_distribution, bins='auto')
    plt.title("Protein-wise Mean Patch Size Distribution with Spectral Clustering ( Mean Patch Size = 2)")
    plt.xlabel("Mean Patch Size")
    plt.ylabel("Number of proteins")
    plt.show()
    plt.figure()
    plt.hist(std_distribution, bins='auto')
    plt.xlabel("Patch Size Standard Deviation")
    plt.ylabel("Number of proteins")
    plt.title("Protein-wise Patch Size Standard Deviation Distribution with Spectral Clustering ( Mean Patch Size = 2)")
    plt.show()


def print_patch_info(protein, mean, std, patches_sizes):
    print "{:^45}".format("\n  Protein :{}".format(protein))
    print u"  Avg. Patch Size: {:.3f}{}{:.3f}".format(mean, u"\u00B1", std)
    sizes = dict(Counter(patches_sizes))
    print "_" * 45
    print "| {:^20}|{:^20} |".format('Patch Size', 'Number of Patches')
    print "_" * 45
    for k, v in sizes.iteritems():
        print "| {:^20}|{:^20} |".format(k, v)
    print "_" * 45


def true_interface_distribution():
    import matplotlib.pyplot as plt
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    distribution = []
    for complex_code in training + testing:
        proteins = [complex_code + "_l_u"]
        for protein in proteins:
            true_interface = TrueInterfaceExtractor(None).load(protein)
            distribution.append(int(true_interface.T.dot(true_interface)))
    print np.median(distribution)
    print np.mean(distribution)
    print np.std(distribution)
    print np.max(distribution)
    print np.min(distribution)
    plt.hist(distribution, bins='auto')
    plt.title("Ligand's Interface Distribution")
    plt.show()
    print list(distribution)
    print np.histogram(distribution)


if __name__ == "__main__":
    patch_size_distribution()
