import numpy as np
import pickle
import warnings
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from matplotlib.pyplot import plot, legend, show
from blrm.model.protein import Protein
from blrm.configuration import categories_pickle_filename, database_pdb_directory
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor



def collect_rasa_distribution():
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    dbd4, new_dbd5 = map(list, categories['DBD4_DBD5'])
    dbd4.extend(new_dbd5)
    complexes = dbd4
    all_res_rasas = []
    interacting_res_rasas = []
    non_interacting_res_rasas = []
    for a_complex in complexes:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", PDBConstructionWarning)
            ligand_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + a_complex + "_l_b.pdb"))
            receptor_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + a_complex + "_r_b.pdb"))
            for protein in [ligand_bound, receptor_bound]:
                StrideSecondaryStructureExtractor(protein).extract()

        print "Working on {}".format(a_complex)
        proteins = ["{}_l_b".format(a_complex), "{}_r_b".format(a_complex)]
        for protein in proteins:
            interface = TrueInterfaceExtractor(None).load(protein)
            rasas = StrideSecondaryStructureExtractor(None).load(protein)
            all_res_rasas.extend(list(rasas))
            interacting_res_rasas.extend(list(rasas[np.where(interface>0.0)]))
            non_interacting_res_rasas.extend(list(rasas[np.where(interface==0.0)]))

    hist, bins = np.histogram(all_res_rasas)
    dist = hist / float(np.sum(hist))
    print(dist)
    plot(bins[:-1], dist, label="All Residues")
    hist, bins = np.histogram(interacting_res_rasas)
    dist = hist / float(np.sum(hist))
    print(dist)
    plot(bins[:-1], dist, label="Interacting Residues")
    hist, bins = np.histogram(non_interacting_res_rasas)
    dist = hist / float(np.sum(hist))
    print(dist)
    plot(bins[:-1], dist, label="Non-Interacting Residues")
    legend()
    show()

if __name__ == '__main__':
    collect_rasa_distribution()