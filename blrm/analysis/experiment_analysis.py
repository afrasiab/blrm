import matplotlib
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor

matplotlib.use('pdf')

from blrm.data.dbd.feature_extractors.residue_id_extractor import ResidueIDExtractor
import itertools
from PyML.evaluators.roc import roc
from sklearn.metrics import average_precision_score
from blrm.utils.pretty_print import Print

from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from Bio.PDB.PDBExceptions import PDBConstructionWarning
import matplotlib.pyplot as plt
import glob
import os
from os.path import basename
import warnings
from PyML.utils.misc import ProgressBar
from matplotlib.backends.backend_pdf import PdfPages
import cPickle
from blrm.configuration import verbose, database_pdb_directory
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.fixed_neighbourhood_extractor import FixedNeighbourhoodExtractor
import numpy as np
from blrm.data.dbd.feature_extractors.patch_extractor import PatchExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor
from blrm.model.protein import Protein
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import f1_score

__author__ = 'basir'


class ExperimentAnalysis:
    """
     This class will calculate accuracy measures both at
     residue level and patch level from a set of pyml result objects.
    """
    # complex-wise residue-level measures
    patch_level_rfpps = {}
    patch_overlap = {}
    patch_pair_rankings = {}
    high_score_patch_size = {}
    top_patch = {}
    # complex-wise residue-level measures
    residue_level_rfpps = {}
    residue_level_aucs = {}
    residue_level_auprcs = {}
    residue_level_mccs = {}
    residue_level_f1s = {}
    patch_res_rfpps = {}
    # complex-wise residue pair scores in each complex
    residue_pairs_scores = {}
    patch_pairs_scores = {}

    def __init__(self, experiment):
        self.experiment = experiment

    def _initialize(self, **kwargs):
        # complex-wise patch-level measures
        self.pyml_results = [self.experiment]
        self.patch_level_rfpps = {}
        self.top_patch_pair = {}
        self.patch_overlap = {}
        self.patch_pair_rankings = {}
        self.patch_res_rfpps = {}
        self.high_score_patch_size = {}
        # complex-wise residue-level measures
        self.residue_level_aucs = {}
        self.auprcs = {}
        self.mccs = {}
        self.f1s = {}
        self.rfpps = {}
        self.complex_scores = {}
        # setting the default parameters
        if 'patch_sizes' not in kwargs:
            kwargs['patch_sizes'] = [5]
        if 'smoothing_method' not in kwargs:
            kwargs['smoothing_method'] = None
        if 'f1_average_method' not in kwargs:
            kwargs['f1_average_method'] = 'binary'
        if 'use_pp_kernel' not in kwargs:
            kwargs['use_pp_kernel'] = True
        if 'patch_rfpp_def' not in kwargs:
            kwargs['patch_rfpp_def'] = 'def1'
        if 'surface_threshold' not in kwargs:
            kwargs['surface_threshold'] = 0.5
        if 'n_top_patches' not in kwargs:
            kwargs['n_top_patches'] = 3

    def collect_stats(self, **kwargs):
        self._initialize(**kwargs)
        for i, results_object in enumerate(self.experiment):
            Print.print_info("Analyzing result set {0}".format(i + 1))
            # since we dont want to change the original results object we work with this list
            scores = self.smooth_scores(i, **kwargs)
            self.separate_residue_pair_scores(results_object, scores)
            self.collect_residue_level_accuracy_measures(**kwargs)
            print self.residue_level_aucs
            print np.mean(self.residue_level_aucs.values())
            self.collect_patch_level_accuracy_measures(**kwargs)

    @staticmethod
    def save_plots(result_directory, patch_res_rfpps, patch_rfpps, rfpps):
        with PdfPages(result_directory + 'rfpps.pdf') as pdf:
            values = [rfpps.values(), patch_rfpps.values(), patch_res_rfpps.values()]
            lbl = ["Residue level RFPP histogram", "Patch level RFPP histogram",
                   "Residue-level counterpart of Patch level RFPP"]
            for i, value in enumerate(values):
                plt.rc('text', usetex=False)
                plt.figure()
                plt.hist(value)
                plt.title(lbl[i])
                pdf.savefig()
                plt.close()

    def collect_patch_level_accuracy_measures(self, **kwargs):
        rfpp_def = kwargs['patch_rfpp_def']
        use_pp_kernel = kwargs['use_pp_kernel']
        patch_sizes = kwargs['patch_sizes']
        for patch_size in patch_sizes:
            Print.print_info("Calculating patch RFPPs of size {}".format(patch_size))
            self.patch_level_rfpps[patch_size] = {}
            self.patch_pairs_scores[patch_size] = {}
            p = ProgressBar("\tComputing patch-level RFPPs")
            for complex_index, cmplx in enumerate(self.complex_scores):
                if verbose:
                    p.progress((complex_index + 1) / float(len(self.complex_scores)))
                ligand, receptor = cmplx + "_l_u", cmplx + "_r_u"
                l_n, r_n = len(ResidueIDExtractor(None).load(ligand)), len(ResidueIDExtractor(None).load(receptor))
                l_pp_scores, r_pp_scores = self.get_pairpred_score_kernel(l_n, r_n) if use_pp_kernel else None, None
                # loading patches
                l_patches = PatchExtractor(None, mean_patch_size=patch_size, other_kernels=l_pp_scores).load(ligand)
                r_patches = PatchExtractor(None, mean_patch_size=patch_size, other_kernels=r_pp_scores).load(receptor)
                # assigning score to pairs of patches from the scores of pairs of residues in it.
                patches = (l_patches, r_patches)
                self.patch_pairs_scores[patch_size][cmplx] = self._get_patch_pair_scores(cmplx, patches)
                self.patch_level_rfpps[patch_size][cmplx] = self._get_patch_rfpp(cmplx, patches, patch_size, rfpp_def)
                # self.residue_level_converted_rfpps[patch_size][cmplx] = self._get_residue_converted

    def push_top_k(self, ranks, n):
        top_k_set = set(ranks)
        for i in range(n):
            if i not in top_k_set:
                ranks.append(i)
        return ranks

    def separate_residue_pair_scores(self, result, scores):
        full_id_cache = {}
        Print.print_info("Separating each complex scores...")
        for i, the_id in enumerate(result.patternID):
            l_coded, r_coded = the_id.split(":")
            l_p, r_p = l_coded.split("-")[0], r_coded.split("-")[0]
            if l_p not in full_id_cache:
                full_id_cache[l_p] = list(ResidueFullIDExtractor(None).load(l_p))
                full_id_cache[r_p] = list(ResidueFullIDExtractor(None).load(r_p))
            l_full_ids = full_id_cache[l_p]
            r_full_ids = full_id_cache[r_p]
            l = l_full_ids.index(l_coded)
            r = r_full_ids.index(r_coded)
            complex_code = l_p[:-4]
            if complex_code not in self.complex_scores:
                self.complex_scores[complex_code] = []
            if np.isnan(scores[i]):
                raise ValueError("scores of {} is nan".format(the_id))
            self.complex_scores[complex_code].append((l, r, scores[i], result.Y[i], result.givenY[i]))
        for complex_code in self.complex_scores:
            table = np.array(self.complex_scores[complex_code])
            table = table[(-table[:, 2]).argsort()]
            self.complex_scores[complex_code] = table

    def collect_residue_level_accuracy_measures(self, **kwargs):
        p = ProgressBar("\tComputing residue-level RFPPs")
        for i, complex_code in enumerate(self.complex_scores):
            if verbose:
                p.progress((i + 1) / float(len(self.complex_scores)))
            table = self.complex_scores[complex_code]
            scores = list(table[:, 2])
            labels = list(table[:, 4])
            predictions = list(table[:, 3])
            self.residue_level_rfpps[complex_code] = np.min(np.where(labels == 1)) + 1
            self.residue_level_aucs[complex_code] = roc(scores, labels)[2]
            self.residue_level_mccs[complex_code] = matthews_corrcoef(labels, predictions)
            self.residue_level_f1s[complex_code] = f1_score(labels, predictions, average=kwargs['f1_average_method'])
            self.residue_level_auprcs[complex_code] = average_precision_score(labels, scores)

    def smooth_scores(self, index, **kwargs):
        pyml_result = self.pyml_results[index]
        smoothed_scores = np.copy(pyml_result.decisionFunc)
        method = 'mean'
        # method = kwargs['smoothing_method']
        if method is None:
            return smoothed_scores
        elif method != 'max' and method != 'mean':
            raise ValueError('method can be None, max or mean')
        else:
            neighbourhood_cache = {}
            full_id_cache = {}
            p = ProgressBar("\tSmoothing the PAIRpred scores")
            neighbourhood_extractor = FixedNeighbourhoodExtractor(None)
            n = float(neighbourhood_extractor.get_neighbourhood_number())
            pattern_id_dictionary = dict(zip(pyml_result.patternID, range(len(pyml_result.patternID))))
            for i, the_id in enumerate(pyml_result.patternID):
                if verbose:
                    p.progress((i + 1) / float(len(pyml_result.patternID)))
                l_coded, r_coded = the_id.split(":")
                l_p, r_p = l_coded.split("-")[0], r_coded.split("-")[0]

                if l_p not in neighbourhood_cache:
                    neighbourhood_cache[l_p] = neighbourhood_extractor.load(l_p)
                    neighbourhood_cache[r_p] = neighbourhood_extractor.load(r_p)
                    full_id_cache[l_p] = list(ResidueFullIDExtractor(None).load(l_p))
                    full_id_cache[r_p] = list(ResidueFullIDExtractor(None).load(r_p))
                l_neighbours = neighbourhood_cache[l_p]
                r_neighbours = neighbourhood_cache[r_p]
                l_full_ids = full_id_cache[l_p]
                r_full_ids = full_id_cache[r_p]
                l = l_full_ids.index(l_coded)
                r = r_full_ids.index(r_coded)
                r_score = 0
                l_score = 0
                for l_index in l_neighbours[l]:
                    e_id = "{}:{}".format(l_full_ids[int(l_index)], r_full_ids[r])
                    if e_id in pattern_id_dictionary:
                        index = pattern_id_dictionary[e_id]
                        l_score += pyml_result.decisionFunc[index]
                for r_index in r_neighbours[r]:
                    e_id = "{}:{}".format(l_full_ids[l], r_full_ids[int(r_index)])
                    if e_id in pattern_id_dictionary:
                        index = pattern_id_dictionary[e_id]
                        r_score += pyml_result.decisionFunc[index]
                new_score = (l_score + r_score) / (2 * n)
                if method == 'max':
                    smoothed_scores[i] = new_score if new_score < smoothed_scores[i] else smoothed_scores[i]
                else:
                    smoothed_scores[i] = ((l_score / n) + (r_score / n)) / 2
            return smoothed_scores

    def create_pymol_files(self):
        for i, result in enumerate(self.pyml_results):
            result_directory = self.pred_directory + "{0}/".format(i)
            patch_pickle_file = result_directory + "pymol_patch_pred.pickle"
            res_pickle_file = result_directory + "pymol_reside_pred.pickle"
            top_patch_pickle_file = result_directory + "top_patch.pickle"

            patches_colors = {}
            residue_colors = {}
            proteins = [basename(name).replace("_patch.npy", "") for name in
                        glob.glob(result_directory + "*_patch.npy")]
            for protein in proteins:
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore", PDBConstructionWarning)
                    p = Protein(*PDB.read_pdb_file(database_pdb_directory + protein + ".pdb"))
                    patches_colors[protein] = {}
                    residue_colors[protein] = {}
                    patch_mask = np.load(result_directory + protein + "_patch.npy")
                    residue_mask = np.load(result_directory + protein + "_res.npy")
                    for j, residue in enumerate(p.biopython_residues):
                        patches_colors[protein][(residue.parent.id, residue.id[1])] = int(patch_mask[j])
                        residue_colors[protein][(residue.parent.id, residue.id[1])] = int(residue_mask[j])
            cPickle.dump(patches_colors, open(patch_pickle_file, "w"))
            cPickle.dump(residue_colors, open(res_pickle_file, "w"))
            cPickle.dump(self.top_patch, open(top_patch_pickle_file, "w"))

    def export_to_csv(self, complex_scores, patch_pair_rankings, patch_size, use_pp_scores):
        csv_directory = self.output_directory + "csv/"

        if not os.path.exists(csv_directory):
            os.mkdir(csv_directory)
        for complex_code in complex_scores:
            f_r_s = open(csv_directory + complex_code + "_residue_scores.csv", "w")
            f_p_s = open(csv_directory + complex_code + "_patch_scores.csv", "w")
            f_p = open(csv_directory + complex_code + "_patches.csv", "w")

            scores = np.array(complex_scores[complex_code])
            scores = scores[(-scores[:, 2]).argsort()]

            ligand, receptor = complex_code + "_l_u", complex_code + "_r_u"
            l_ids, r_ids = ResidueFullIDExtractor(None).load(ligand), ResidueFullIDExtractor(None).load(receptor)
            l_pp_scores = r_pp_scores = None
            if use_pp_scores:
                l_pp_scores = r_pp_scores = []
            l_patches = PatchExtractor(None, mean_patch_size=patch_size, other_kernels=l_pp_scores).load(ligand)
            r_patches = PatchExtractor(None, mean_patch_size=patch_size, other_kernels=r_pp_scores).load(receptor)

            for score in scores:
                f_r_s.write("{0}, {1}, {2}\n".format(l_ids[int(score[0])], r_ids[int(score[1])], score[2]))
            f_r_s.close()
            for (protein, patches, ids) in [("ligand", l_patches, l_ids), ("receptor", r_patches, r_ids)]:
                for i in list(np.unique(patches)):
                    residue_indices = list(np.where(patches == i)[0])
                    f_p.write("{0}, {1}".format(protein, i))
                    for index in residue_indices:
                        f_p.write(", {0}".format(ids[index]))
                    f_p.write("\n")
            f_p.close()
            for entry in patch_pair_rankings[complex_code]:
                f_p_s.write("{0}, {1}, {2}\n".format(entry[0], entry[1], entry[2]))
            f_p_s.close()

    def get_pairpred_score_kernel(self, ligand_n, receptor_n):
        ligand = {}
        receptor = {}
        for l_i, r_i, s, _, _ in self.complex_scores:
            if l_i not in ligand:
                ligand[l_i] = 0.
            if r_i not in receptor:
                receptor[r_i] = 0.
            ligand[l_i] += s
            receptor[r_i] += s
        for i in ligand:
            ligand[i] /= float(len(receptor))
        for i in receptor:
            receptor[i] /= float(len(ligand))
        kernels = []
        for c, dic in enumerate([ligand, receptor]):
            normalizer = max(dic.values()) - min(dic.values())
            n = [ligand_n, receptor_n][c]
            kernel = np.zeros((n, n))

            for i in dic:
                for j in dic:
                    kernel[i, j] = np.exp(-((dic[i] - dic[j]) // float(normalizer)) ** 2 / 2 * 0.5)
            if np.isnan(np.sum(kernel)):
                raise ValueError("Kernel CONTAINS NAN")
            kernels.append(kernel)
        return kernels[0], kernels[1]

    def _get_patch_pair_scores(self, complex_code, patches, first_n=1, surface_threshold=-1):
        # loading surface accessibilities
        l_patches, r_patches = patches
        l_rasa = StrideSecondaryStructureExtractor(None).load("{}_l_u".format(complex_code))
        r_rasa = StrideSecondaryStructureExtractor(None).load("{}_r_u".format(complex_code))
        patch_pair_rankings = {}
        # assigning scores to the pairs of patches based on residue-level scores
        for entry in self.complex_scores[complex_code]:
            l_r, r_r = int(entry[0]), int(entry[1])
            l_key, r_key = "l_{}".format(l_r), "r_{}".format(r_r)
            p_l, p_r = l_patches[l_r], r_patches[r_r]
            if (p_l, p_r) not in patch_pair_rankings:
                patch_pair_rankings[(p_l, p_r)] = (0., set())
            visited_residues = patch_pair_rankings[(p_l, p_r)][1]
            if visited_residues is None or l_key in visited_residues or r_key in visited_residues:
                continue
            elif len(visited_residues) < 2 * first_n:
                # only consider surface residues
                if surface_threshold > 0 and (l_rasa[l_r] < surface_threshold or r_rasa[r_r] < surface_threshold):
                    continue
                visited_residues.add(l_key)
                visited_residues.add(r_key)
                score = patch_pair_rankings[(p_l, p_r)][0]
                patch_pair_rankings[(p_l, p_r)] = (score + entry[2] / first_n, visited_residues)
            elif len(visited_residues) == 2 * first_n:
                patch_pair_rankings[(p_l, p_r)] = (patch_pair_rankings[(p_l, p_r)][0], None)
        rankings = np.zeros((len(patch_pair_rankings), 3))

        for complex_index, (l_p, r_p) in enumerate(patch_pair_rankings):
            rankings[complex_index, :] = l_p, r_p, patch_pair_rankings[(l_p, r_p)][0]
        return rankings[(-rankings[:, 2]).argsort()]

    def _get_patch_rfpp(self, complex_code, patches, patch_size, patch_rfpp_def):
        if patch_rfpp_def not in {'def1', 'def2', 'def3'}:
            raise ValueError("rfpp definition should be either def1, def2 or def3")
        residue_labels = {}
        l_patches, r_patches = patches
        for l_r, r_r, _, _, label in self.residue_pairs_scores[complex_code]:
            residue_labels[(l_r, r_r)] = label
        l_true_interface = TrueInterfaceExtractor(None).load("{}_l_u".format(complex_code))
        r_true_interface = TrueInterfaceExtractor(None).load("{}_r_u".format(complex_code))
        cumulative_l_p, cumulative_r_p = set(), set()
        # computing patch RFPP based on the definition
        for i, l_p, r_p, _ in enumerate(self.patch_pairs_scores[patch_size][complex_code]):
            p_l_indices = list(np.where(l_patches == l_p)[0])
            p_r_indices = list(np.where(r_patches == r_p)[0])
            if patch_rfpp_def == 'def1':
                l_patch_mask, r_patch_mask = np.zeros(l_true_interface.shape), np.zeros(r_true_interface.shape)
                l_patch_mask[l_patches == l_p] = 1
                r_patch_mask[r_patches == r_p] = 1
                l_overlap = np.sum(l_true_interface.T.dot(l_patch_mask))
                r_overlap = np.sum(r_true_interface.T.dot(r_patch_mask))
                if l_overlap > 1 and r_overlap > 1 :
                    return i+1
            elif patch_rfpp_def == 'def2':
                for (l_r, r_r) in itertools.product(p_l_indices, p_r_indices):
                    if (l_r, r_r) in residue_labels and residue_labels[(l_r, r_r)] == 1:
                        return i+1
            elif patch_rfpp_def == 'def3':
                cumulative_l_p.add(p_l_indices)
                cumulative_r_p.add(p_r_indices)
                for (l_r, r_r) in itertools.product(cumulative_l_p, cumulative_r_p):
                    if (l_r, r_r) in residue_labels and residue_labels[(l_r, r_r)] == 1:
                        return i+1

