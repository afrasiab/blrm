import cPickle
import pickle
import numpy as np
from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.configuration import results_directory, categories_pickle_filename
# from pairpred.configuration import complex_classes

# from pairpred.data.dbd.dbd5 import DBD5
def load_and_analyze(experiment_name, patch_sizes):
    # categories = pickle.load(open(categories_pickle_filename, 'rb'))
    # training, testing = map(list, categories['DBD4_DBD5'])
    # datasets = [DBD5(list(set(training + testing)))]
    # easy, med, hard = categories['Difficulty']
    # dic = {'Easy': easy, 'Medium': med, 'Hard': hard}
    results = cPickle.load(file(results_directory + "{}results.pickle".format(experiment_name)))
    directory = results_directory + experiment_name
    result = results.values()[0][0]
    print "Analyzing {}".format(experiment_name)
    print result
    for i in patch_sizes:
        rfpps, patch_rfpps, patch_res_rfpps, aucs = ExperimentAnalysis(result).collect_stats(
            smooth=True,
                                                                                        patch_size=i,
                                                                                        export=False)
        print "Medians\t{0}\t{1}\t{2}\t{3}".format(np.median(rfpps.values()),
                                                   np.median(patch_rfpps.values()),
                                                   np.median(patch_res_rfpps.values()),
                                                   np.median(aucs.values()))


if __name__ == '__main__':
    # load_and_analyze("DBD4-CV-SINGLE-GAMMA=1.6, C=100/", [2, 5, 10, 15])
    load_and_analyze("DBD4-DBD5-ONE-GAMMA/", [2])
# rfpps, patch_rfpps, patch_res_rfpps, aucs = Analyzer(results.values()[0][0],
#                                                      results_directory + "DBD4-CV-Pre/").analyze(smooth=True)

# Analyzer(results.values()[0][0], results_directory + "DBD4-five fold CV profile only - test /").analyze(smooth=True)
# #
# dic = categories['Classes']
#
# for p_class in np.sort(list(set(dic.values()))):
#     a_rfpp = []
#     a_p_rfpp = []
#     a_r_p_rfpp = []
#     a_auc = []
#     for comp in rfpps.keys():
#         if dic[comp] == p_class:
#             a_rfpp.append(rfpps[comp])
#             a_p_rfpp.append(patch_rfpps[comp])
#             a_r_p_rfpp.append(patch_res_rfpps[comp])
#             a_auc.append(aucs[comp])
#     if len(a_rfpp) > 0:
#         print "{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(complex_classes[p_class], np.median(a_rfpp), np.median(a_p_rfpp),
#                                 np.median(a_r_p_rfpp), np.median(a_auc), int(100 * len(a_auc) / float(len(rfpps))))
#
# for category in np.sort(dic.keys()):
#     r_ranks = []
#     p_ranks = []
#     r_p_ranks = []
#     c_aucs = []
#     # print category
#     for comp in rfpps.keys():
#         if comp in dic[category]:
#             r_ranks.append(rfpps[comp])
#             p_ranks.append(patch_rfpps[comp])
#             r_p_ranks.append(patch_res_rfpps[comp])
#             c_aucs.append(aucs[comp])
#             # print comp, rfpps[comp], patch_rfpps[comp], patch_res_rfpps[comp]
#
#     print "{0}\t{1}\t{2}\t{3}\t{4}\t{5}".format(category, np.mean(r_ranks), np.mean(p_ranks), np.mean(r_p_ranks),
#                                                 np.mean(c_aucs), int(100 * len(c_aucs) / float(len(rfpps))))
#
