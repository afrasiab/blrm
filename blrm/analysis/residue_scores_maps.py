import glob

import numpy
from PyML.evaluators.roc import roc
from matplotlib.pyplot import plot, show, legend
from numpy import zeros
from sklearn.metrics import average_precision_score
from sklearn.metrics import precision_recall_curve

from blrm.configuration import pairpred_docking_directory
from blrm.data.dbd.examples_extractor import ExampleExtractor, read_pdbs
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor


def read_patch_file(patch_file_name):
    ligand_patches = {}
    receptor_patches = {}
    with open(patch_file_name) as patch_file:
        patches = patch_file.readlines()
        for patch in patches:
            parts = patch.split(",")
            if parts[0].strip() == "ligand":
                dic = ligand_patches
            else:
                dic = receptor_patches
            dic[parts[1].strip()] = []
            for part in parts[2:]:
                dic[parts[1].strip()].append(part.strip())
    return ligand_patches, receptor_patches


def read_residue_file(residue_file_name):
    socres = {}
    with open(residue_file_name) as residue_file:
        i = 0
        lines = residue_file.readlines()
        residue_keys = {}
        residue_scores = {}
        for line in lines:
            parts = line.split(",")
            residue_scores[i] = float(parts[2])
            key = "{}_{}".format(parts[0].strip(), parts[1].strip())
            residue_keys[key] = i
            socres[key] = residue_scores[i]
            i += 1
        return residue_scores, residue_keys, socres


def give_surface(keys, scores, id_index_dict, surface_thrsh=0.5):
    surface = []
    non_surface = []
    surface_scores = []
    non_surface_scores = []
    for i, key in enumerate(keys):
        parts = key.split("_")
        r1 = "{}_{}_{}".format(parts[0], parts[1], parts[2])
        r2 = "{}_{}_{}".format(parts[3], parts[4], parts[5])
        if id_index_dict[r1] > surface_thrsh and id_index_dict[r2] > surface_thrsh:
            surface.append(key)
            surface_scores.append(scores[i])
        else:
            non_surface.append(key)
            non_surface_scores.append(scores[i])
    surface_scores = numpy.array(surface_scores)
    non_surface_scores = numpy.array(non_surface_scores)
    indices = surface_scores.argsort()
    surface = [surface[i] for i in indices]
    indices = non_surface_scores.argsort()
    non_surface = [non_surface[i] for i in indices]
    surface.extend(non_surface)
    return surface


def main1():
    not_found = []
    rocs = []
    original_rocs = []
    for patch_score_file_name in glob.glob(pairpred_docking_directory + "*_patch_scores.csv"):
        with open(patch_score_file_name) as patch_score_file:
            complex_code = patch_score_file_name.split("/")[-1][:4]
            patch_file_name = patch_score_file_name.replace("_patch_scores.csv", "_patches.csv")
            patch_residue_file_name = patch_score_file_name.replace("_patch_scores.csv", "_patch_residue.csv")
            residue_file_name = patch_score_file_name.replace("_patch_scores.csv", "_residue_scores.csv")
            residue_scores, residue_keys, scores_map = read_residue_file(residue_file_name)
            ligand, receptor = read_patch_file(patch_file_name)
            total_reranked_keys = []
            total_reranked_scores = []

            for record in patch_score_file:
                [l_patch, r_patch, _] = [str(int(float(p))) for p in record.split(",")]
                # n = ligand[l_patch] * receptor[r_patch]
                reranked_scores = []
                reranked_keys = []
                for l_residue in ligand[l_patch]:
                    for r_residue in receptor[r_patch]:
                        key = "{}_{}".format(l_residue, r_residue)
                        if key not in residue_keys:
                            not_found.append(key)
                            continue
                        reranked_scores.append(residue_scores[residue_keys[key]])
                        reranked_keys.append(key)
                reranked_scores = numpy.array(reranked_scores)
                sort_indices = reranked_scores.argsort()
                reranked_keys = [reranked_keys[index] for index in sort_indices]
                total_reranked_keys.extend(numpy.copy(reranked_keys))
                total_reranked_scores.extend(residue_scores)
            pos, neg, _, _ = ExampleExtractor(None).load(complex_code)
            pos = dict(pos)
            scores, labels = [], []
            o_scores, o_labels = [], []
            for i, key in enumerate(total_reranked_keys):
                scores.append(len(total_reranked_keys) - i)
                if key in pos:
                    labels.append(1)
                else:
                    labels.append(0)
            for key in total_reranked_keys:
                o_scores.append(scores_map[key])
                if key in pos:
                    o_labels.append(1)
                else:
                    o_labels.append(0)
            o_scores = numpy.array(o_scores)
            o_labels = numpy.array(o_labels)
            indices = o_scores.argsort()[::-1]
            o_scores = o_scores[indices]
            o_labels = o_labels[indices]
            rocs.append(roc(list(scores), list(labels))[-1])
            original_rocs.append(roc(list(o_scores), list(o_labels))[-1])
            print "{}\t{:.3f}\t{:.3f}".format(complex_code, rocs[-1], original_rocs[-1])
    print numpy.mean(rocs)
    print numpy.mean(original_rocs)


def get_id_dict(complex_code):
    complex_ = read_pdbs(complex_code)
    ps = [complex_.unbound_formation.ligand, complex_.unbound_formation.receptor]
    id_index_dict = {}
    for p in ps:
        rasa = StrideSecondaryStructureExtractor(None).load(p.name)
        for i, r in enumerate(p.residues):
            chain_id = r.residue.parent.id
            res_id = r.residue.id
            key = "{}-{}-{}".format(p.name, chain_id, res_id[1])
            id_index_dict[key] = rasa[i]
    return id_index_dict


def main():
    not_found = []
    rocs = []
    original_rocs = []
    prs = []
    original_prs = []
    for patch_score_file_name in glob.glob(pairpred_docking_directory + "*_patch_scores.csv"):

        with open(patch_score_file_name) as patch_score_file:
            complex_code = patch_score_file_name.split("/")[-1][:4]
            id_index_dict = get_id_dict(complex_code)
            patch_file_name = patch_score_file_name.replace("_patch_scores.csv", "_patches.csv")
            residue_file_name = patch_score_file_name.replace("_patch_scores.csv", "_residue_scores.csv")
            residue_scores, residue_keys, scores_map = read_residue_file(residue_file_name)
            ligand, receptor = read_patch_file(patch_file_name)
            total_reranked_keys = []
            total_reranked_scores = []

            for record in patch_score_file:
                [l_patch, r_patch, _] = [str(int(float(p))) for p in record.split(",")]
                # n = ligand[l_patch] * receptor[r_patch]
                reranked_scores = []
                reranked_keys = []
                for l_residue in ligand[l_patch]:
                    for r_residue in receptor[r_patch]:
                        key = "{}_{}".format(l_residue, r_residue)
                        if key not in residue_keys:
                            not_found.append(key)
                            continue
                        reranked_scores.append(residue_scores[residue_keys[key]])
                        reranked_keys.append(key)
                reranked_keys = give_surface(reranked_keys, reranked_scores, id_index_dict)
                total_reranked_keys.extend(reranked_keys)
            pos, _, _, _ = ExampleExtractor(None).load(complex_code)
            pos = dict(pos)
            scores, labels = [], []
            o_scores, o_labels = [], []
            for i, key in enumerate(total_reranked_keys):
                scores.append(len(total_reranked_keys) - i)
                if key in pos:
                    labels.append(1)
                else:
                    labels.append(0)
            for key in total_reranked_keys:
                o_scores.append(scores_map[key])
                if key in pos:
                    o_labels.append(1)
                else:
                    o_labels.append(0)
            o_scores = numpy.array(o_scores)
            o_labels = numpy.array(o_labels)
            indices = o_scores.argsort()[::-1]
            o_scores = o_scores[indices]
            o_labels = o_labels[indices]
            p, r, _ = precision_recall_curve(labels, scores)
            area = average_precision_score(labels, scores)
            plot(r, p, label='{0} (area = {1:0.2f})'.format(complex_code, area))
            rocs.append(roc(list(scores), list(labels))[-1])
            prs.append(area)
            original_prs.append(average_precision_score(o_labels, o_scores))
            original_rocs.append(roc(list(o_scores), list(o_labels))[-1])
            print "{}\t{:.3f}\t{:.3f}\t{:.3f}\t{:.3f}".format(complex_code, rocs[-1], original_rocs[-1], prs[-1],
                                                              original_prs[-1])
    legend()
    show()
    print numpy.mean(rocs)
    print numpy.mean(original_rocs)
    print numpy.mean(prs)
    print numpy.mean(original_prs)


if __name__ == '__main__':
    main()
