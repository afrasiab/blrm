import sys

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Print:
    def __init__(self):
        pass

    @classmethod
    def print_info(cls, message):
        """

        :rtype : object
        """
        # print Fore.GREEN +  u"\u24D8 {0}".format(message) + Fore.RESET
        print "{0}".format(message)

    @classmethod
    def print_warning(cls, message):
        print "Warning: {0}".format(message)
        # print Fore.YELLOW + u"\u26A0 Warning: {0}".format(message) + Fore.RESET

    @classmethod
    def print_error(cls, message):
        print "ERROR: {0}".format(message)
        # print Fore.RED +  u"\u2622 ERROR: {0}".format(message) + Fore.RESET

    @classmethod
    def print_special(cls, message):
        # print Fore.CYAN + "{0}".format(message) + Fore.RESET
        print "{0}".format(message)

    @classmethod
    def print_info_nn(cls, message):
        sys.stdout.write("{0}".format(message))
# class Print:
#     def __init__(self):
#         pass
#
#     @classmethod
#     def print_info(cls, message):
#         """
#
#         :rtype : object
#         """
#         # print Fore.GREEN +  u"\u24D8 {0}".format(message) + Fore.RESET
#         print u"\u24D8 {0}".format(message)
#
#     @classmethod
#     def print_warning(cls, message):
#         print u"\u26A0 Warning: {0}".format(message)
#         # print Fore.YELLOW + u"\u26A0 Warning: {0}".format(message) + Fore.RESET
#
#     @classmethod
#     def print_error(cls, message):
#         print u"\u2622   ERROR: {0}".format(message)
#         # print Fore.RED +  u"\u2622 ERROR: {0}".format(message) + Fore.RESET
#
#     @classmethod
#     def print_special(cls, message):
#         # print Fore.CYAN + "{0}".format(message) + Fore.RESET
#         print u"\u2605 {0}".format(message)
#
#     @classmethod
#     def print_info_nn(cls, message):
#         sys.stdout.write(u"\u24D8 {0}".format(message))

