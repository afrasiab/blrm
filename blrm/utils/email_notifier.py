import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class EmailNotifier:
    def __init__(self, emails):
        if not isinstance(emails, list):
            raise ValueError()
        for email in emails:
            if not isinstance(email, str):
                raise ValueError()
        self._emails = emails

    def send(self, subject="", text_body="", html_body="", sender="", attachments=None):
        if attachments is None:
            attachments = []
        self._check_attachments(attachments)
        message = MIMEMultipart()
        message['Subject'] = subject
        message['From'] = sender
        message['Date'] = formatdate(localtime=True)
        message['To'] = COMMASPACE.join(self._emails)
        text = text_body
        html = """<html><head></head><body>{}</body></html>""".format(html_body)
        body = MIMEMultipart('alternative')
        if text_body != "":
            body.attach(MIMEText(text, 'plain'))
        if html_body != "":
            body.attach(MIMEText(html, 'html'))
        message.attach(body)
        for attachment in attachments:
            if attachment[-4:].lower() == '.doc':
                attachment_file = MIMEBase('application', 'msword')
            elif attachment[-4:].lower() == '.pdf':
                attachment_file = MIMEBase('application', 'pdf')
            else:
                attachment_file = MIMEBase('application', 'octet-stream')
            attachment_file.set_payload(file(attachment, "rb").read())
            encoders.encode_base64(attachment_file)
            message.attach(attachment_file)
        server_ssl = smtplib.SMTP_SSL("smtp.gmail.com", 465)
        server_ssl.ehlo()
        server_ssl.login("pairpred2", "halfwaythere")
        server_ssl.sendmail(sender, self._emails, message.as_string())
        server_ssl.close()

    @staticmethod
    def _check_attachments(attachments):
        for attachment in attachments:
            if not os.path.exists(attachment):
                raise ValueError("Attachment {} cant be accessed!".format(attachment))


if __name__ == '__main__':
    EmailNotifier(['b.shariat@gmail.com']).send("PAIRPred Experiment 1",
                                                "I am running\n",
                                                "<b><a href=\"www.google.com\">Google</a>",
                                                attachments=["/s/chopin/l/grad/basir/prcurves.pdf"])
