import os

__author__ = 'basir'


def get_filename_parts(file_name):
    (path, name) = os.path.split(file_name)
    n = os.path.splitext(name)[0]
    ext = os.path.splitext(name)[1]
    return path, n, ext
