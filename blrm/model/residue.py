import numpy as np
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Residue:
    def __init__(self, residue):
        self.residue = residue
        self.computed_features = {}
        coordinates = [0, 0, 0]
        for atom in residue.get_list():
            coordinates += atom.get_coord()
        self.center = np.array(coordinates / len(residue.get_list()))
        # self.center = self.center.reshape((1, self.center.size))
        self.coordinates = None

    def get_computed_features(self):
        return self.computed_features.keys()

    def get_vector_form(self, features):
        uncomputed_features = set(features) - set(self.get_computed_features())
        if uncomputed_features != set([]):
            Print.print_error(
                "Following features {0} is still not computed for this residue {1}".format(uncomputed_features, self))
            return None
        temp = None
        for feature in features:

            vector = self.computed_features[feature]
            if temp is None:
                temp = vector
            else:
                temp = np.hstack((vector, temp))
        return temp

    def is_hetero(self):
        return self.residue.id[0].strip() != ''

    def add_feature(self, feature, vector_form):
        # if feature not in self.computed_features:
        self.computed_features[feature] = vector_form

    # else:
    # print_warning("Feature {0} for residue {1} already computed!".format(feature, self))

    def get_feature(self, feature):
        if feature not in self.computed_features:
            Print.print_error("Feature {0} is not computed!".format(feature))
        else:
            return self.computed_features[feature]

    def get_coordinates(self):
        if self.coordinates is None:
            atoms = self.residue.get_list()
            self.coordinates = np.zeros((len(atoms), 3))
            for index, atom in enumerate(atoms):
                self.coordinates[index, :] = atom.get_coord()
        return self.coordinates
