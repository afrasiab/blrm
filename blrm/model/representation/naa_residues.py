import glob
import numpy as np
import os
import warnings
from Bio.PDB import NeighborSearch
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import basename
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from blrm.configuration import three2one, amino_acids, graph_representations_directory, graph_representation_radii, \
    database_pdb_directory
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.b_value_extractor import BValueExtractor
from blrm.data.dbd.feature_extractors.d1_category_shape_distribution import D1CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_ml_plain_shape_distribution import D1MultiLevelShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_plain_shape_distribution import D1PlainShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_atoms_shape_distribution import \
    D1SurfaceAtomsShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_shape_distribution import D1SurfaceShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_surface_category_shape_distribution import \
    D1SurfaceCategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.fixed_neighbourhood_extractor import FixedNeighbourhoodExtractor
from blrm.data.dbd.feature_extractors.half_sphere_amino_acid_composition_extractor import \
    HalfSphereAminoAcidCompositionExtractor
from blrm.data.dbd.feature_extractors.profile_extractor import ProfileExtractor
from blrm.data.dbd.feature_extractors.protein_length_extractor import ProteinLengthExtractor
from blrm.data.dbd.feature_extractors.protrusion_index_extractor import ProtrusionIndexExtractor
from blrm.data.dbd.feature_extractors.residue_depth_extractor import ResidueDepthExtractor
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.feature_extractors.residue_id_extractor import ResidueIDExtractor
from blrm.data.dbd.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor
from blrm.data.dbd.feature_extractors.secondary_srtucture_extractor import DSSPSecondaryStructureExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.model.enums import Features
from blrm.model.representation.abstract_represenation import AbstractRepresentation
from blrm.utils.pretty_print import Print



class NearestAATypeResidues(AbstractRepresentation):
    def __init__(self, protein, vertex_features, chain_code=None, **kwargs):
        AbstractRepresentation.__init__(self, protein, chain_code, **kwargs)
        self._vertex_features = vertex_features

    def load_representation(self, name):
        graph_file, vertex_file = self._get_file_name(name)
        graph_matrix = np.load(graph_file)
        vertex_matrix = np.load(vertex_file)
        return np.hstack((graph_matrix, vertex_matrix))

    def compute_representation(self):
        aa_names = set(three2one.keys())
        pdb_code = self._protein.name

        if self._chain is None:
            graph_file, vertex_file = self._get_file_name(pdb_code)
        else:
            pdb_code = self._chain.parent.parent.id.split("/")[-1]
            chain_id = "{}.{}".format(pdb_code, self._chain.id)
            graph_file, vertex_file = self._get_file_name(chain_id)
        if not os.path.exists(graph_file) or os.path.exists(vertex_file):
            n = len(amino_acids) + 1

            all_res_mat = None
            for feature in self._vertex_features:
                extractor, gamma, _ = feature_dict[feature]
                feature_array = extractor(self._protein).load(pdb_code)
                all_res_mat = feature_array if all_res_mat is None else np.hstack((all_res_mat, feature_array))

            neighbour_search = NeighborSearch(self._protein.atoms)
            final_graph_matrix = np.zeros((len(self._protein.residues), (n * (n - 1) / 2)))
            final_vertex_matrix = np.zeros((len(self._protein.residues), n*all_res_mat.shape[1]))
            # there may be water or other molecules listed as "residues" in the pdb
            # we need this list of indices to remove non residue entries from the
            # final_graph_matrix array
            aa_indices = []
            for i, residue in enumerate(self._protein.biopython_residues):
                aa_indices.append(i)
                # filling the edge matrix
                graph_matrix = np.zeros((n, n))
                atom_coord = get_main_atom_coord(residue)
                nearby_residues = neighbour_search.search(atom_coord, graph_representation_radii[0], "R")
                visited_aa = {}
                # find nearest AA
                for nearby_residue in nearby_residues:
                    aa = nearby_residue.resname
                    if nearby_residue == residue or aa not in aa_names:
                        continue
                    if aa not in visited_aa:
                        visited_aa[aa] = nearby_residue
                    n_atom_coord = get_main_atom_coord(nearby_residue)
                    s_atom_coord = get_main_atom_coord(visited_aa[aa])

                    neighbour_distance = np.linalg.norm(atom_coord - n_atom_coord)
                    shortest_distance = np.linalg.norm(atom_coord - s_atom_coord)
                    if neighbour_distance < shortest_distance:
                        visited_aa[aa] = nearby_residue
                # compute edges
                neighborhood = visited_aa.values()
                for j, aa in enumerate(neighborhood):
                    aa_index = amino_acids.index(three2one[aa.resname])
                    aa_coord = get_main_atom_coord(aa)
                    d_r = np.linalg.norm(aa_coord - atom_coord)
                    graph_matrix[aa_index, n - 1] = graph_matrix[n - 1, aa_index] = np.exp(-d_r * 0.5)
                    for k in range(j):
                        bb = neighborhood[k]
                        bb_coord = get_main_atom_coord(bb)
                        d = np.linalg.norm(aa_coord - bb_coord)
                        bb_index = amino_acids.index(three2one[bb.resname])
                        graph_matrix[bb_index, aa_index] = graph_matrix[aa_index, bb_index] = np.exp(-d * 0.5)
                final_graph_matrix[i, :] = graph_matrix[np.tril_indices(n, -1)]
                # filling the vertex matrix

                vertex_matrix = np.zeros((n, all_res_mat.shape[1]))
                for j, aa in enumerate(neighborhood):
                    if aa in self._protein.biopython_residues:
                        r_i = self._protein.biopython_residues.index(aa)
                        aa_index = amino_acids.index(three2one[aa.resname])
                        vertex_matrix[aa_index, :] = all_res_mat[r_i, :]
                vertex_matrix[n-1, :] = all_res_mat[self._protein.biopython_residues.index(residue), :]
                final_vertex_matrix[i, :] = vertex_matrix.flatten()
            # print final_vertex_matrix.shape, final_graph_matrix.shape
            np.save(graph_file, final_graph_matrix)
            np.save(vertex_file, final_vertex_matrix)
        return True

    @staticmethod
    def _get_file_name(name):
        return (graph_representations_directory + "{0}_adj.npy".format(name),
                graph_representations_directory + "{0}_ver.npy".format(name))


def get_main_atom_coord(residue):
    if 'CA' in residue:
        return residue['CA'].coord
    elif 'CB' in residue:
        return residue['CB'].coord
    elif 'N' in residue:
        return residue['N'].coord
    else:
        c = None
        for a in residue:
            c = a.coord if c is None else c+a.coord
        c /= len(residue)
        return c


def read_pdbs(the_complex):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        ligand_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_b.pdb"))
        receptor_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_b.pdb"))
        ligand_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_u.pdb"))
        receptor_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_u.pdb"))
        bound_formation = ProteinPair(ligand_bound, receptor_bound)
        unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
        return ProteinComplex(the_complex, unbound_formation, bound_formation)


def main():
    features = [
        Features.SHORT_PROFILE,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.HALF_SPHERE_EXPOSURE,
        Features.PROTRUSION_INDEX,
        Features.RESIDUE_DEPTH,
        Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION
    ]
    # DBD5()
    complexes = [basename(name).replace("_l_b.pdb", "") for name in glob.glob(database_pdb_directory + "*_l_b.pdb")]
    complexes.remove("2B42")
    complexes.remove("3R9A")
    complexes.sort()
    # Print.print_info("Extracting features ...")
    for counter, complex_code in enumerate(complexes):
        Print.print_info("{0}/{1}... processing complex {2}".format(counter + 1, len(complexes), complex_code))
        complex_object_model = read_pdbs(complex_code)
        for protein in [complex_object_model.unbound_formation.ligand, complex_object_model.unbound_formation.receptor]:
            ProtrusionIndexExtractor(protein).extract()
            ResidueNeighbourhoodExtractor(protein).extract()
            ProteinLengthExtractor(protein).extract()
            ProfileExtractor(protein).extract()
            ResidueIDExtractor(protein).extract()
            ResidueDepthExtractor(protein).extract()
            HalfSphereAminoAcidCompositionExtractor(protein).extract()
            ProtrusionIndexExtractor(protein).extract()
            StrideSecondaryStructureExtractor(protein).extract()
            BValueExtractor(protein).extract()
            D1PlainShapeDistributionExtractor(protein).extract()
            # D2PlainShapeDistributionExtractor(protein, **kwargs).extract()
            # D2SurfaceShapeDistributionExtractor(protein, **kwargs).extract()
            D1SurfaceShapeDistributionExtractor(protein).extract()
            # D2CategoryShapeDistributionExtractor(protein, **kwargs).extract()
            D1CategoryShapeDistributionExtractor(protein).extract()
            D1SurfaceCategoryShapeDistributionExtractor(protein).extract()
            DSSPSecondaryStructureExtractor(protein).extract()
            # PatchExtractor(protein, **kwargs).extract()
            FixedNeighbourhoodExtractor(protein).extract()
            ResidueFullIDExtractor(protein).extract()
            # PredictedSSExtractor(protein, **kwargs).extract()
            D1SurfaceAtomsShapeDistributionExtractor(protein).extract()
            D1MultiLevelShapeDistributionExtractor(protein).extract()
            NearestAATypeResidues(protein, features).compute_representation()


if __name__ == '__main__':
    main()
