import numpy as np


class PairwiseKernel():
    def __init__(self, kernel_type='tensor'):
        if kernel_type != 'tensor':
            raise NotImplementedError()
        self.kernel_type = kernel_type

    def compute(self, x1, x2):
        """
        x1 and x2 must be vectors of the same size
        :param x1:
        :param x2:
        :return:
        """
        if not ((isinstance(x1, np.ndarray) and isinstance(x2, np.ndarray) and x1.shape == x2.shape) or
                    (isinstance(x1, list) and isinstance(x2, list) and len(x1) == len(x2))):
            raise ValueError("")
        n = len(x1)
        if self.kernel_type == 'tensor':
            x1 = np.array(x1).flatten()
            x2 = np.array(x2).flatten()

            # return np.multiply(x1,x2).reshape(1, -1)
            return np.concatenate(((x1+x2).reshape(1, -1), np.multiply(x1, x2).reshape(1, -1)))
            # x1_x2 = np.outer(x1, x2) + np.outer(x2, x1)
            # return np.concatenate((np.sum(
            # np.outer(x1, x2) + np.outer(x2, x1), axis=0)/n , np.sum(np.outer(x1, x2) + np.outer(x2, x1),
            # axis=1)/n)).reshape(1, -1)
            # return (np.outer(x1, x2) + np.outer(x2, x1)).reshape(1, -1)
