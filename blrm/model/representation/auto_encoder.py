from abc import ABCMeta, abstractmethod
import hashlib
import numpy as np


class AutoEncoder:
    __metaclass__ = ABCMeta
    name = ""

    def __init__(self, load_form=None):
        self.model_file = load_form
        # self._build_model()
        pass

    @abstractmethod
    def _get_model_file_name(self):
        pass

    @abstractmethod
    def train(self, x, z=None):
        """
        trains an autoencoder.
        :param x: given input.
        :param z: desired output if none, x is considered as the desired output
                  and it is up to the implementing class to produce the given input.
        """
        pass

    def _already_trained(self, x, z):
        m = hashlib.md5()
        m.update(np.array(range(100)))
        m.update(np.array(range(200)))

    @abstractmethod
    def _load(self, file_name):
        pass

    @abstractmethod
    def _build_model(self):
        pass

    @abstractmethod
    def encode(self, x, batch_size):
        pass


if __name__ == '__main__':


    m = hashlib.md5()
    m.update(np.array(range(100)))
    m.update(np.array(range(200)))

    m2 = hashlib.md5()
    m2.update(np.array(range(100)))
    m2.update(np.array(range(200)))

    print m.hexdigest()
    print m2.hexdigest()

    assert m.hexdigest() == m2.hexdigest()

    m3 = hashlib.md5()
    m3.update(np.array(range(100)))
    m3.update(np.array(range(199)))

    print m3.hexdigest()

    assert m.hexdigest() == m3.hexdigest()