import pickle
import timeit
import theano

import numpy as np
from theano.tensor.shared_randomstreams import RandomStreams

from blrm.configuration import categories_pickle_filename
from blrm.data.dbd.dbd5 import DBD5
from blrm.model.enums import Features
import theano.tensor as t

# noinspection PyCompatibility
from blrm.model.representation.auto_encoder import AutoEncoder
from blrm.model.representation.tutorial.SdA import SdA
from blrm.model.representation.tutorial.dA import dA
from blrm.model.representation.tutorial.logistic_sgd import shared_dataset
from blrm.model.representation.tutorial.mlp import HiddenLayer


class StackedDenoisingAutoEncoder(AutoEncoder):
    def __init__(self,
                 name,
                 load_form=None,
                 n_in=1000,
                 hidden_layers_sizes=None,
                 corruption_levels=None,
                 seed=32,
                 rng=None):
        super(StackedDenoisingAutoEncoder, self).__init__(load_form)

        if corruption_levels is None:
            corruption_levels = [0.1, 0.1, 0.1]
        if hidden_layers_sizes is None:
            hidden_layers_sizes = [1000, 1000, 1000]

        self.corruption_levels = corruption_levels
        self.n_in = n_in
        self.seed = seed
        self.hidden_layers_sizes = hidden_layers_sizes
        self.sigmoid_layers = []
        self.dA_layers = []
        self.params = []

        self.n_layers = len(hidden_layers_sizes)
        assert self.n_layers > 0
        if rng is None:
            self.numpy_rng = np.random.RandomState(seed)
        else:
            self.numpy_rng = rng
        self._build_model()

    def _build_model(self):
        self.x = t.matrix('x')
        self.numpy_rng = np.random.RandomState(self.seed)
        for i in range(self.n_layers):
            if i == 0:
                input_size = self.n_in
                layer_input = self.x

            else:
                input_size = self.hidden_layers_sizes[i - 1]
                layer_input = self.sigmoid_layers[-1].output

            sigmoid_layer = HiddenLayer(rng=self.numpy_rng,
                                        input=layer_input,
                                        n_in=input_size,
                                        n_out=self.hidden_layers_sizes[i],
                                        activation=t.nnet.sigmoid)
            # add the layer to our list of layers
            self.sigmoid_layers.append(sigmoid_layer)
            self.params.extend(sigmoid_layer.params)
            theano_rng = RandomStreams(self.numpy_rng.randint(2 ** 30))
            da_layer = dA(numpy_rng=self.numpy_rng,
                          theano_rng=theano_rng,
                          input=layer_input,
                          n_visible=input_size,
                          n_hidden=self.hidden_layers_sizes[i],
                          W=sigmoid_layer.W,
                          bhid=sigmoid_layer.b)
            self.dA_layers.append(da_layer)

    def pretraining_functions(self, train_set_x, batch_size):
        """ Generates a list of functions, each of them implementing one
        step in training the dA corresponding to the layer with same index.
        The function will require as input the minibatch index, and to train
        a dA you just need to iterate, calling the corresponding function on
        all minibatch indexes.

        :type train_set_x: theano.tensor.TensorType
        :param train_set_x: Shared variable that contains all data points used
                            for training the dA

        :type batch_size: int
        :param batch_size: size of a [mini]batch

        """

        # index to a [mini]batch
        index = t.lscalar('index')  # index to a minibatch
        corruption_level = t.scalar('corruption')  # % of corruption to use
        learning_rate = t.scalar('lr')  # learning rate to use
        # beginning of a batch, given `index`
        batch_begin = index * batch_size
        # ending of a batch given `index`
        batch_end = batch_begin + batch_size
        pretrain_fns = []
        for da in self.dA_layers:
            # get the cost and the updates list
            cost, updates = da.get_cost_updates(corruption_level, learning_rate)
            # compile the theano function
            fn = theano.function(
                inputs=[
                    index,
                    theano.In(corruption_level, value=0.2),
                    theano.In(learning_rate, value=0.1)
                ],
                outputs=cost,
                updates=updates,
                givens={
                    self.x: train_set_x[batch_begin: batch_end]
                }
            )
            # append `fn` to the list of functions
            pretrain_fns.append(fn)

        return pretrain_fns

    def build_encoding_function(self, train_set_x, batch_size):
        index = t.lscalar('index')  # index to a [mini]batch
        encoder = theano.function(
            inputs=[index],
            outputs=self.sigmoid_layers[-1].output,
            givens={
                self.x: train_set_x[index * batch_size: (index + 1) * batch_size]
            },
            name='encoder'
        )
        return encoder

    def _load(self, file_name):
        pass

    def _get_model_file_name(self):
        pass

    def encode(self, x, batch_size):
        self.x.set_value(x)
        n_train_batches = x.shape[0]/batch_size
        encoder = self.build_encoding_function(self.x, batch_size)
        coded = []
        for batch_i in range(n_train_batches):
            coded.append(encoder(index=batch_i))
        return np.array(coded)

    def train(self, x,
              batch_size=1,
              pretraining_epochs=15,
              pretrain_lr=0.001,
              training_epochs=1000,
              corruption_levels=None):

        if corruption_levels is None:
            corruption_levels = [.1, .2, .3]

        self.x.set_value(x)
        n_train_batches = x.shape[0]/batch_size
        print('... getting the pretraining functions')
        pretraining_fns = self.pretraining_functions(train_set_x=self.x, batch_size=batch_size)
        print('... pre-training the model')
        start_time = timeit.default_timer()
        # Pre-train layer-wise

        for i in range(self.n_layers):
            # go through pretraining epochs
            for epoch in range(pretraining_epochs):
                # go through the training set
                c = []
                for batch_i in range(n_train_batches):
                    c.append(pretraining_fns[i](index=batch_i, corruption=corruption_levels[i], lr=pretrain_lr))
                print('Pre-training layer {}, epoch {}, cost {}'.format(i, epoch, np.mean(c)))

        end_time = timeit.default_timer()
        seconds = (end_time - start_time) / 60.
        import os
        print('The pretraining code for file {} ran for %{:2f}'.format(os.path.split(__file__)[1], seconds))
        # end-snippet-4


def get_data():
    # pass
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])

    complexes = list(set(training + testing))
    dbd = DBD5(complexes)
    features = [
        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
        # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.HALF_SPHERE_EXPOSURE,
        Features.PROTRUSION_INDEX,
        Features.D1_PLAIN_SHAPE_DISTRIBUTION,
    ]
    v, ids = dbd.get_raw_vector(features, complexes, "Raw Residue vectors")
    v = v[~np.isnan(v).any(axis=1)]
    return v, ids


def main():
    import os
    file_name = "x.npy"
    size = 0
    if os.path.exists(file_name):
        x = np.load(file_name)
        size = x[0].size
        x = shared_dataset((x, x))[0]
    else:
        x, ids = get_data()
        size = x[0].size
        np.save(file_name, x.as_numpy_array())
    seed = 89677
    numpy_rng = np.random.RandomState(seed)
    finetune_lr = 0.1
    pretraining_epochs = 15
    pretrain_lr = 0.001
    training_epochs = 1000,
    batch_size = 1
    print('... building the model')
    # construct the stacked denoising autoencoder class
    n_train_batches = x.get_value(borrow=True).shape[0]
    n_train_batches //= batch_size
    sda = SdA(
        numpy_rng=numpy_rng,
        n_ins=size,
        hidden_layers_sizes=[1000, 1000, 1000],
        n_outs=10
    )

    # end-snippet-3 start-snippet-4
    #########################
    # PRETRAINING THE MODEL #
    #########################
    print('... getting the pretraining functions')
    pretraining_fns = sda.pretraining_functions(train_set_x=x, batch_size=batch_size)
    print('... pre-training the model')
    start_time = timeit.default_timer()
    # Pre-train layer-wise
    corruption_levels = [.1, .2, .3]
    for i in range(sda.n_layers):
        # go through pretraining epochs
        for epoch in range(pretraining_epochs):
            # go through the training set
            c = []
            for batch_i in range(n_train_batches):
                c.append(pretraining_fns[i](index=batch_i, corruption=corruption_levels[i], lr=pretrain_lr))
            print('Pre-training layer {}, epoch {}, cost {}'.format(i, epoch, np.mean(c)))

    end_time = timeit.default_timer()
    seconds = (end_time - start_time) / 60.
    print('The pretraining code for file {} ran for %{:2f}'.format(os.path.split(__file__)[1], seconds))
    # end-snippet-4
    encoder = sda.build_encoding_function(x, batch_size)
    coded = []
    for batch_i in range(n_train_batches):
        coded.append(encoder(index=batch_i))
    print np.array(coded)


if __name__ == '__main__':
    main()
