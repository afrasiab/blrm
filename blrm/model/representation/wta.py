import pickle

import wtaae.python_lib.nn_utils as nn
import wtaae.python_lib.convnet as convnet

from blrm.configuration import categories_pickle_filename, wta_saved_models_directory
from blrm.data.dbd.dbd5 import DBD5
from blrm.data.dbd.utils import feature_dict
from blrm.model.enums import Features
import numpy as np
import wtaae.gnumpy as gp
import os

# noinspection PyCompatibility
from blrm.model.representation.auto_encoder import AutoEncoder


class WTAAE(AutoEncoder):
    def __init__(self, x, sparsity):
        super(WTAAE, self).__init__(x)
        self.sparsity = sparsity
        self.auto_encoder = None
        model_file_name = self._get_model_file_name()
        if not os.path.exists(model_file_name):
            self.train()
        self._load()

    def encode(self, data):
        pad_size = 128-data.shape[0] % 128
        data = np.vstack((data, np.zeros((pad_size, data.shape[1]))))
        return self.auto_encoder.test(data, data)[1][0:-pad_size, :]

    def _load(self):
        self.auto_encoder = self.build_ae()
        self.auto_encoder.load(self._get_model_file_name())

    def train(self):
        auto_encoder = self.build_ae()
        # config.info()
        auto_encoder.train(self.x, self.x, self.x, None,
                           dataset_size=self.x[0],
                           batch_size=128,
                           initial_weights=.01 * nn.randn(auto_encoder.cfg.num_parameters),
                           momentum=.9,
                           learning_rate=.01,
                           visual=False,
                           report=False
                           , num_epochs=1000
                           )
        auto_encoder.save(self._get_model_file_name())

    def build_ae(self):
        reload(nn)
        reload(convnet)
        reload(convnet.cn)
        nn.set_backend("gnumpy")
        config = convnet.cn.NeuralNetCfg(want_dropout=False, want_k_sparsity=True, want_tied=True)
        d = self.x.shape[1]
        config.input_dense(d)
        config.dense(num_filters=int(d * 1.5), activation=nn.relu, dropout=None, k_sparsity=self.sparsity)
        config.output_dense(num_filters=d, activation=nn.linear)
        auto_encoder = convnet.NeuralNet(config)
        auto_encoder.set_cost(auto_encoder.compute_cost_euclidean)
        return auto_encoder

    def __get_model_file_name(self):
        key = self.database + ":"
        for feature in self.features:
            key += feature_dict[feature][2] + "-"
        key += ":"
        for c in self.complexes:
            key += c + "-"
        key += ":" + str(self.sparsity)
        return wta_saved_models_directory + "m-90.model"
        # return wta_saved_models_directory + hashlib.sha1(key).hexdigest() + ".model"


def get_data(backend='gnumpy', dtype='float32'):
    # pass
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])

    complexes = list(set(training + testing))
    dbd = DBD5(complexes)
    features = [
        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
        # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.HALF_SPHERE_EXPOSURE,
        Features.PROTRUSION_INDEX,
        Features.D1_PLAIN_SHAPE_DISTRIBUTION,
    ]
    v, ids = dbd.get_raw_vector(features, complexes, "Raw Residue vectors")
    v = v[~np.isnan(v).any(axis=1)]
    if backend == "numpy":
        x = np.array(v, dtype)
    else:
        x = gp.garray(v)
    return x, ids


def main():
    import os
    file_name = "x.npy"
    if os.path.exists(file_name):
        x = np.load(file_name)
        x = gp.as_garray(x)
    else:
        x, ids = get_data()
        np.save(file_name, x.as_numpy_array())
    reload(nn)
    reload(convnet)
    reload(convnet.cn)
    nn.set_backend("gnumpy")
    config = convnet.cn.NeuralNetCfg(want_dropout=False, want_k_sparsity=True, want_tied=True)
    (n, d) = x.shape
    print (n, d)
    config.input_dense(d)
    config.dense(num_filters=int(d * 1.5), activation=nn.relu, dropout=None, k_sparsity=90)
    # config.dense(num_filters=int(d * 2.5), activation=nn.relu, dropout=None, k_sparsity=80)
    # config.dense(num_filters=int(d * 3.5), activation=nn.relu, dropout=None, k_sparsity=95)
    config.output_dense(num_filters=d, activation=nn.linear)
    auto_encoder = convnet.NeuralNet(config)
    auto_encoder.set_cost(auto_encoder.compute_cost_euclidean)
    config.info()
    auto_encoder.train(x, x, x, None,
                       dataset_size=n,
                       batch_size=128,
                       initial_weights=.01 * nn.randn(auto_encoder.cfg.num_parameters),
                       momentum=.9,
                       learning_rate=.01,
                       visual=False,
                       report=False
                       , num_epochs=1000
                       )
    model_file = wta_saved_models_directory + "m-90.model"
    auto_encoder.save(model_file)
    # auto_encoder.load(model_file)

    # _, t_array = auto_encoder.test(x, x)
    # print t_array.shape
    # np.save("transformed-3layer.npy", t_array)


if __name__ == '__main__':
    main()
