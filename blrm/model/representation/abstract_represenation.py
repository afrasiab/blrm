from abc import abstractmethod


class AbstractRepresentation:
    def __init__(self, protein, chain_code=None, **kwargs):
        self._protein = protein
        if chain_code is not None:
            self._chain = protein.get_chain(chain_code)
        else:
            self._chain = None

    @abstractmethod
    def compute_representation(self):
        pass

    @abstractmethod
    def load_representation(self, name):
        pass
