import pickle
from random import shuffle

__author__ = 'basir shariat (basir@rams.colostate.edu)'

from pymol import cmd


def normalize(values):
    normalized = []
    mi = min(values)
    ma = max(values)
    for value in values:
        normalized.append(int(9 * (value - mi) / (ma - mi)))
    return normalized


def load_top_patches(the_file):
    patches = {}
    file_name = scores_folder + protein + "_KK.csv"
    f = open(file_name)
    for line in f:
        parts = line.split(", ")
        patches[(parts[0], parts[1])] = int(parts[2])
    return patches


def color_top_patches(top_patches_file=""):
    if top_patches_file == "":
        return
    obj_list = cmd.get_names('objects')
    print obj_list
    # colors = ['br0',
    #           'br1',
    #           'br2',
    #           'br3',
    #           'br4',
    #           'br5',
    #           'br6',
    #           'br7',
    #           'br8',
    #           'br9']
    colors = ['brightorange',
              'brown',
              'carbon',
              'chartreuse',
              'chocolate',
              'cyan',
              'darksalmon',
              'dash',
              'deepblue',
              'deepolive',
              'deeppurple',
              'deepsalmon',
              'deepsalmon',
              'deepteal',
              'density',
              'dirtyviolet',
              'firebrick',
              'forest',
              'gray',
              'green',
              'greencyan',
              'grey',
              'hotpink',
              'hydrogen',
              'lightblue',
              'lightmagenta',
              'lightorange',
              'lightpink',
              'lightteal',
              'lime',
              'limegreen',
              'limon',
              'magenta',
              'marine',
              'nitrogen',
              'olive',
              'orange',
              'oxygen',
              'palecyan',
              'palegreen',
              'paleyellow',
              'pink']

    n = len(obj_list)
    top_patch_dict = pickle.load(open(top_patches_file))
    print "color top patch is executing..."
    for j in range(n):
        print obj_list[j]
        # end_part = ((obj_list[j])[-4:]).lower().strip()
        # if end_part == "_l_b":
        #     cmd.color("yellow", "/" + obj_list[j] + "////")
        # if end_part == "_r_b":
        cmd.color("green", "/" + obj_list[j] + "////")

        # print scores_dic.keys()
        for key in top_patch_dict[obj_list[j]]:
            _, chain_id, residue_id  = key.split("-")
            selector = "/" + obj_list[j] + "//" + chain_id.strip() + "/" + str(residue_id)
            # print selector, scores_dic[key]
            cmd.color(colors[-1], selector)


cmd.extend("color_top_patches", color_top_patches())

