import pickle
from random import shuffle

__author__ = 'basir shariat (basir@rams.colostate.edu)'

from pymol import cmd


def normalize(values):
    normalized = []
    mi = min(values)
    ma = max(values)
    for value in values:
        normalized.append(int(9*(value-mi)/(ma-mi)))
    return normalized


def load_scores(scores_folder, complex_code):
    ligand_file_name = scores_folder+complex_code+"_ligand_scores.csv"
    f_l = open(ligand_file_name)
    scores = []
    residues = []
    for line in f_l:
        parts = line.split(", ")
        residues.append(parts[0])
        scores.append(parts[1])

    ligand_file_name = scores_folder+complex_code+"_receptor_scores.csv"
    f_l = open(ligand_file_name)
    scores = []
    residues = []
    for line in f_l:
        parts = line.split(", ")
        residues.append(parts[0])
        scores.append(parts[1])

    scores_dict = dict(zip(residues, normalize(scores)))
    scores_dict.update(dict(zip(residues, normalize(scores))))
    return scores_dict


def color_scores(scores_folder=""):
    if scores_folder == "":
        return
    obj_list = cmd.get_names('objects')
    print obj_list
    colors = ['br0',
              'br1',
              'br2',
              'br3',
              'br4',
              'br5',
              'br6',
              'br7',
              'br8',
              'br9']
    n = len(obj_list)
    print "color interface is executing..."
    for j in range(n):
        print obj_list[j]
        end_part = ((obj_list[j])[-4:]).lower().strip()
        # print obj_list[j]
        if end_part != "_l_u" and end_part != "_r_u":
            continue
        complex_code = obj_list[j][:4]
        scores_dic = load_scores(scores_folder, complex_code)
        no_match = True
        # print scores_dic.keys()
        for key in scores_dic:
            parts = key.split("-")
            p = parts[0]
            # p = parts[0].replace("_u", "_b")
            # print obj_list[j] , p
            if p == obj_list[j]:
                no_match = False
                chain_id, residue_id = parts[1], parts[2]
                selector = "/" + obj_list[j] + "//" + chain_id + "/" + str(residue_id)
                print selector, scores_dic[key]
                cmd.color(colors[scores_dic[key]], selector)
        if no_match:
            print "there was no match for object " + obj_list[j] + " in the list"


cmd.extend("color_scores", color_scores())
