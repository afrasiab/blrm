import pickle
from random import shuffle

__author__ = 'basir shariat (basir@rams.colostate.edu)'

from pymol import cmd



def color_patches(file_name=""):
    if file_name == "":
        return
    interfaces = pickle.load(open(file_name, "rb"))
    obj_list = cmd.get_names('objects')


    l_colors = ['red',
                'brown',
                'tv_red',
                'raspberry',
                'darksalmon',
                'salmon',
                'deepsalmon',
                'warmpink',
                'firebrick',
                'ruby',
                'chocolate']
    r_colors = ['blue',
                'lightblue',
                'density',
                'tv_blue',
                'marine',
                'slate',
                'skyblue',
                'purpleblue',
                'deepblue']
    kernel_keys = {'H': 0, 'B': 1, 'E': 2, 'G': 3, 'I': 4, 'T': 5, 'S': 6, '-': 7}

    cc = ['orange', 'cyan', 'blue', 'green', 'yellow', 'brown', 'red', 'magenta']
    shuffle(cc)
    n = len(obj_list)
    color_counter = 0
    print "color interface is executing..."
    for j in range(n):

        end_part = ((obj_list[j])[-4:]).lower().strip()
        if obj_list[j] not in interfaces:
            continue

        if end_part == "_l_b":
            cmd.color("yellow", "/" + obj_list[j] + "////")
        if end_part == "_r_b":
            cmd.color("green", "/" + obj_list[j] + "////")

        partition_dict = interfaces[obj_list[j]]
        print "There are " + str(len(partition_dict)) + " residues in interface of object " + obj_list[j]
        if end_part == "_l_b":
            colors = l_colors
        else:
            colors = r_colors
        print len(set(partition_dict.values()))
        for key in partition_dict.keys():
            chain_id, residue_id = key
            partition = partition_dict[key]
            selector = "/" + obj_list[j] + "//" + chain_id + "/" + str(residue_id)
            # cmd.color("r"+str(partition*20).zfill(3), selector)
            cmd.color(cc[partition%len(cc)], selector)
            cmd.label(str(partition), selector)


cmd.extend("color_patches", color_patches())
