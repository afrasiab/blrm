import pickle
from random import shuffle

__author__ = 'basir shariat (basir@rams.colostate.edu)'

from pymol import cmd


def normalize(values):
    normalized = []
    mi = min(values)
    ma = max(values)
    for value in values:
        normalized.append(int(9 * (value - mi) / (ma - mi)))
    return normalized


def load_scores(scores_folder, protein):
    patches = {}
    file_name = scores_folder + protein + "_KK.csv"
    f = open(file_name)
    for line in f:
        parts = line.split(", ")
        patches[(parts[0], parts[1])] = int(parts[2])
    return patches


def color_patches(patches_folder=""):
    if patches_folder == "":
        return
    obj_list = cmd.get_names('objects')
    print obj_list
    # colors = ['br0',
    #           'br1',
    #           'br2',
    #           'br3',
    #           'br4',
    #           'br5',
    #           'br6',
    #           'br7',
    #           'br8',
    #           'br9']
    colors = ['brightorange',
              'brown',
              'carbon',
              'chartreuse',
              'chocolate',
              'cyan',
              'darksalmon',
              'dash',
              'deepblue',
              'deepolive',
              'deeppurple',
              'deepsalmon',
              'deepsalmon',
              'deepteal',
              'density',
              'dirtyviolet',
              'firebrick',
              'forest',
              'gray',
              'green',
              'greencyan',
              'grey',
              'hotpink',
              'hydrogen',
              'lightblue',
              'lightmagenta',
              'lightorange',
              'lightpink',
              'lightteal',
              'lime',
              'limegreen',
              'limon',
              'magenta',
              'marine',
              'nitrogen',
              'olive',
              'orange',
              'oxygen',
              'palecyan',
              'palegreen',
              'paleyellow',
              'pink']

    n = len(obj_list)
    print "color interface is executing..."
    for j in range(n):
        print obj_list[j]
        scores_dic = load_scores(patches_folder, obj_list[j])
        # print scores_dic.keys()
        for key in scores_dic:
            chain_id, residue_id = key[0], key[1]
            selector = "/" + obj_list[j] + "//" + chain_id.strip() + "/" + str(residue_id)
            print selector, scores_dic[key]
            cmd.color(colors[scores_dic[key]], selector)


cmd.extend("color_patches", color_patches())

