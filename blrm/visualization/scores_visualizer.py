import pickle
from random import shuffle

__author__ = 'basir shariat (basir@rams.colostate.edu)'

from pymol import cmd


def normalize(values):
    normalized = []
    mi = min(values)
    ma = max(values)
    for value in values:
        normalized.append(int(9*(value-mi)/(ma-mi)))
    return normalized


def load_scores(scores_folder, complex_code):
    l_dict = {}
    r_dict = {}
    file_name = scores_folder+complex_code+"_residue_scores_bound.csv"
    f = open(file_name)
    for line in f:
        parts = line.split(", ")
        for i, part in enumerate([parts[0], parts[1]]):
            pairs_dict = [l_dict, r_dict][i]
            if part not in pairs_dict:
                pairs_dict[part] = (0., 0)
            current_score, current_count = pairs_dict[part]
            pair_score = float(parts[2])
            pairs_dict[part] = (current_score + pair_score, current_count + 1)
    l_res_dict = {}
    r_res_dict = {}
    for i, pairs_dict in enumerate([l_dict, r_dict]):
        res_dict = [l_res_dict, r_res_dict][i]
        for residue in pairs_dict:
            current_score, current_count = pairs_dict[residue]
            res_dict[residue] = current_score / float(current_count)
    for i, pairs_dict in enumerate([l_res_dict, r_res_dict]):
        keys = pairs_dict.keys()
        values = pairs_dict.values()
        n = normalize(values)
        if i == 0:
            l_res_dict = dict(zip(keys, n))
        else:
            r_res_dict = dict(zip(keys, n))
    residue_dict = l_res_dict.copy()
    residue_dict.update(r_res_dict)
    return residue_dict


def color_scores(scores_folder=""):
    if scores_folder == "":
        return
    obj_list = cmd.get_names('objects')
    print obj_list
    colors = ['black',
              'grey10',
              'grey20',
              'grey30',
              'grey40',
              'grey50',
              'grey60',
              'grey70',
              'grey80',
              'grey90']
    # colors = ['br0',
    #           'br1',
    #           'br2',
    #           'br3',
    #           'br4',
    #           'br5',
    #           'br6',
    #           'br7',
    #           'br8',
    #           'br9']
    n = len(obj_list)
    print "color interface is executing..."
    for j in range(n):
        print obj_list[j]
        end_part = ((obj_list[j])[-4:]).lower().strip()
        # print obj_list[j]
        # if end_part != "_l_b" and end_part != "_r_b":
        #     continue
        complex_code = obj_list[j][:4]
        scores_dic = load_scores(scores_folder, complex_code)
        no_match = True
        # print scores_dic.keys()
        for key in scores_dic:
            parts = key.split("-")
            p = parts[0]
            # p = parts[0].replace("_u", "_b")
            # print obj_list[j] , p
            if p == obj_list[j]:
                no_match = False
                chain_id, residue_id = parts[1], parts[2]
                selector = "/" + obj_list[j] + "//" + chain_id + "/" + str(residue_id)
                print selector, scores_dic[key]
                cmd.color(colors[scores_dic[key]], selector)
        if no_match:
            print "there was no match for object " + obj_list[j] + " in the list"


cmd.extend("color_scores", color_scores())
