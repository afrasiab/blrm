import itertools
import pickle
from datetime import datetime

import numpy as np
import os
from blrm.ml.rankers.GBLRM import GBLRM

from blrm.configuration import categories_pickle_filename, results_directory
from blrm.data.dbd.dbd5 import DBD5
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.ml.rankers.rfpp_online_ensemble import RFPPOnlineEnsemble
# from pairpred.ml.rankers.ssrfpp_tensorflow import TFSSRFPP
# from pairpred.ml.rankers.ssrfpp_tensorflow import BLRM
from blrm.model.enums import Features, EvaluationType
from blrm.utils.pretty_print import Print
from blrm.ml.rankers.rfpp_svm import SSGDRFPPSVM

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Experiment:
    def __init__(self, configurations):
        [self.datasets, self.classifiers, self.feature_sets] = configurations
        self.configurations = list(itertools.product(*configurations))
        self.pyml_result = {}
        self.description = None
        self.out_directory = None
        self.duration = None
        self.start = None

    @staticmethod
    def export_to_csv(bags, complex_scores, output_directory):
        csv_directory = output_directory + "csv/"

        if not os.path.exists(csv_directory):
            os.mkdir(csv_directory)
        for complex_code in complex_scores:
            f_r_s = open(csv_directory + complex_code + "_residue_scores.csv", "w")

            scores = np.array(complex_scores[complex_code])
            indices = (-scores).argsort()
            scores = scores[indices]

            ligand, receptor = complex_code + "_l_u", complex_code + "_r_u"
            l_ids, r_ids = ResidueFullIDExtractor(None).load(ligand), ResidueFullIDExtractor(None).load(receptor)
            examples = bags[complex_code].values()[0][2]
            examples = examples[indices]
            for i, score in enumerate(scores):
                f_r_s.write("{0}, {1}, {2}\n".format(l_ids[examples[i, 0]], r_ids[examples[i, 1]], score))
            f_r_s.close()

    def run(self, desc, evaluation=EvaluationType.CV, bottom_k=.1, **kwargs):
        self.description = desc
        self.start = datetime.now()
        self.out_directory = results_directory + self.description + "/"
        if not os.path.exists(self.out_directory):
            os.makedirs(self.out_directory)
        Print.print_info("Running experiment {0}".format(self.description))

        for configuration in self.configurations:
            [dataset, classifier, feature_set] = configuration
            features = ""
            for jj, f in enumerate(feature_set):
                features +=  "\t{}-".format(jj+1) + feature_dict[f][2] + "\n"
            print("Features: \n{}".format(features))
            Print.print_info("Ranker {0}".format(classifier.__class__.__name__))
            # features_key = "-".join([feature_dict[f][2] for f in feature_set])

            if evaluation == EvaluationType.TRAIN_TEST:
                training_complexes, validation_complexes, testing_complexes = kwargs['data']
                training_sets = []
                for i in range(len(classifier.estimators)):
                    tr_ration = kwargs['training_ratio'] if 'training_ratio' in kwargs else 10
                    training_sets.append(dataset.get_bags(feature_set, training_complexes, tr_ration, gap=0, neighbours=False))
                # training_sets = dataset.get_bags(feature_set, training_complexes, 10, gap=20)  #, neighbours=True
                validation = dataset.get_bags(feature_set, validation_complexes, -1)
                classifier.train(training_sets, validation=None, k=kwargs['k'])
                del training_sets
                print "Training time {}".format(datetime.now() - self.start)
                print "k = {}".format(kwargs['k'])
                rfpp, aucs, scores = {}, {}, {}
                bags = {}
                for ii, a_complex in enumerate(testing_complexes):
                    bag = dataset.get_bags(feature_set, [a_complex], -1, neighbours=False)
                    bags[a_complex] = bag  # , neighbours=True
                    r, a, s = classifier.get_rfpps(bag)
                    rfpp[a_complex], aucs[a_complex], scores[a_complex] = r.values()[0], a.values()[0], s.values()[0]
                    print "{}/{}\t{}\t{}\t{:3.3f}".format(ii + 1,
                                                          len(testing_complexes),
                                                          a_complex,
                                                          rfpp[a_complex],
                                                          aucs[a_complex])
                print np.median(rfpp.values())
                print np.median(aucs.values())

                sorted_keys = sorted(rfpp, key=rfpp.get)[-int(len(testing_complexes) * bottom_k):]
                print "\n\nBottom {}% percent performing complexes:".format(bottom_k * 100)
                for ii, a_complex in enumerate(sorted_keys):
                    print "{}/{}\t{}\t{}\t{:3.3f}".format(ii + 1,
                                                          len(sorted_keys),
                                                          a_complex,
                                                          rfpp[a_complex],
                                                          aucs[a_complex])
                # self.export_to_csv(bags, scores, self.out_directory)
            elif evaluation == EvaluationType.LOO:
                complexes = kwargs['data']
                import copy
                rfpps, aucs = {}, {}

                for ii, complex_code in enumerate(complexes):
                    training = copy.copy(complexes)
                    del training[ii]
                    tr_ration = kwargs['training_ratio'] if 'training_ratio' in kwargs else 10
                    bags = dataset.get_bags(feature_set, training, tr_ration, gap=0, neighbours=False)
                    classifier.train(bags)
                    del training
                    testing_complex = dataset.get_bags(feature_set, [complex_code], -1)
                    rfpp, auc, _ = classifier.get_rfpps(testing_complex)
                    rfpps.update(rfpp)
                    aucs.update(auc)
                    print complex_code, rfpps[complex_code], aucs[complex_code]
                counter = 0
                for key in rfpps:
                    counter += 1
                    print("{}/{}\t{}\t{}\t{:3.3f}".format(counter, len(aucs), key, rfpps[key], aucs[key]))
                # median = np.median(rfpp.values())
                print np.median(rfpps.values())
                print np.median(aucs.values())
                print self.start
                self.duration = datetime.now() - self.start
                print self.duration
            else:
                raise ValueError("Unknown evaluation type!")
        self.duration = datetime.now() - self.start
        print self.start
        print self.duration
        self.__save_results()

    @staticmethod
    def __get_folds(complexes, n_folds):
        folds = [[] for _ in range(n_folds)]
        for i, a_complex in enumerate(complexes):
            folds[i%n_folds].append(a_complex)
        return folds

    def __save_results(self):
        pickle.dump(self.pyml_result, open(self.out_directory + "results.pickle", "w"))
        pass


def train_on_dbd4_test_on_dbd5():
    # training, testing = ["1AHW"], ["1MLC"]
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    # training.append("2B42")
    # testing.append("3R9A")
    # training.extend(testing)
    # testing = ["2wpt", "3bx1", "3E8L", "3fm8", "3r2x", "3u43", "4eef", "4G9S"]
    # n_tr, n_ts = len(training), len(testing)
    # l = training + testing
    # np.random.shuffle(l)
    # training = l[:n_tr]
    # testing = l[n_tr:]
    # training = list(set(training) - set(bad_complexes))
    validation_ratio = 0.
    validation_index = int(len(training) * validation_ratio)
    # np.random.shuffle(testing)
    np.random.shuffle(training)
    # validation = None
    validation = training[:validation_index]
    training = training[validation_index:]
    # training = ['1EAW']
    # validation = ['1E6J']
    # testing = ['1E96']
    datasets = [DBD5(list(set(training + testing + validation)), check_features=False)]  #
    # classifiers = [SSGDRFPPSVM(the_lambda=1, n_epochs=40)]
    # classifiers = [RFPPOnlinePatchEnsemble()]
    the_lambda = .000005
    print "lamda = {}".format(the_lambda)
    classifiers = [SSGDRFPPSVM(the_lambda=the_lambda, n_epochs=80)]
    # classifiers = [GBLRM(the_lambda=0.5, n_epochs=20)]00000
    # classifiers = [SSGDRFPPSVM(the_lambda=0.05, n_epochs=100)]
    # classifiers = [SSGDRFPPSVM(the_lambda=1.01314207892e-06, n_epochs=84)]
    # classifiers = [BLRM(the_lambda=.1, learning_rate=0.05, n_epochs=235)]
    # classifiers = [BLRM(the_lambda=1, learning_rate=0.5, n_epochs=180)]
    # classifiers = [SVRBLRM(the_lambda=0.000001, learning_rate=0.5, n_epochs=80)]
    # classifiers = [SSGDRFPPSVM(the_lambda=0.001)]
    # classifiers = [SSRFPP()]
    # classifiers = [OfflineLinearTopK()]
    # classifiers = [OnlineLinearTopK(the_lambda=10, n_epochs=150)]
    # classifiers = [OnlineLinearTopK(the_lambda=2.0921043693e-3, n_epochs=650)]
    # classifiers = [SSGDRFPPSVM()]
    # classifiers = [OfflineKernelizedTopK()]
    # classifiers = [KernelizedLMRFPPOptimizer()]
    # classifiers = [ClusteredKernelizedTopK()]
    # classifiers = [KernelizedLMRFPPOptimizer(the_lambda=0.05, n_epochs=20)]
    # classifiers = [sum_proxy_rfpp_svm()]
    # classifiers = [SVM(optimizer='pegasos', C=10)]
    feature_sets = [
        [
            Features.AA_ONE_HOT,
            Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RESIDUE_DEPTH,
            Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            Features.D1_PLAIN_SHAPE_DISTRIBUTION,
            Features.D2_PLAIN_SHAPE_DISTRIBUTION,
            Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION,
            Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
            Features.D1_SURFACE_SHAPE_DISTRIBUTION,
            Features.D2_SURFACE_SHAPE_DISTRIBUTION,
            Features.D1_CATEGORY_SHAPE_DISTRIBUTION,
            Features.D2_CATEGORY_SHAPE_DISTRIBUTION,
            Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION,
            # # Features.D2_MULTI_LEVEL_SHAPE_DISTRIBUTION,
        ],
    ]
    # datasets[0].get_bags(feature_sets[0], training+testing, 10)
    configurations = [datasets, classifiers, feature_sets]
    e = Experiment(configurations)
    # e.run("DBD4-DBD5-RFPP", EvaluationType.MODEL_SELECTION, data=(training, validation, testing), folds=3)
    e.run("DBD4-DBD5-RFPP-HADDOCK",
          EvaluationType.LOO,
          data=training+testing,
          training_ratio=15,
          k=-1)


def main():
    train_on_dbd4_test_on_dbd5()


if __name__ == "__main__":
    main()
    # np.random.seed(1000)
