from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.configuration import latex_report_template

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ReportGenerator():
    analysis = None

    def __init__(self, experiment, analysis=None):
        if not isinstance(analysis, ExperimentAnalysis):
            raise ValueError("Second argument must be an instance of a ExperimentAnalysis class")
        if analysis is None:
            analysis = ExperimentAnalysis()
        self.analysis = analysis

    def generate_latex_report(self, pdf=True):
        template = open(latex_report_template, 'r').read()
        user = an
        residue_scores = \
            "\\begin{center} \n" \
            "\\begin{tabular}{||c || c||} \n" \
            "\\hline \n" \
            "Measure & Value \\\\ [0.5ex] \n" \
            "\\hline \n\\hline \n" \
            "TP & {} \\\\ \\hline \n" \
            "FP & {} \\\\ \\hline \n" \
            "TN & {} \\\\ \\hline \n" \
            "FN & {} \\\\ \\hline \n" \
            "Precision & {} \\\\ \\hline \n" \
            "Recall & {} \\\\ \\hline \n" \
            "AUC & {} \\\\ \\hline \n" \
            "AUPRC & {} \\\\ \\hline \n" \
            "MCC & {} \\\\ \\hline \n" \
            "F-1 & {} \\\\ \\hline \n" \
            "\\hline \n" \
            "\\end{tabular} \n" \
            "\\end{center} \n".format(analysis)
        print(residue_scores)
