import pickle

from PyML import SVM

from blrm.configuration import categories_pickle_filename
from blrm.data.dbd.dbd5 import DBD5
from blrm.experiment.types.experiment_mixin import ExperimentMixin
from blrm.experiment.types.k_fold_cross_validation import KFoldCrossValidation
from blrm.model.enums import Features, EvaluationType

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def cv_on_dbd4():
    # complexes = ["1AHW", "1BVK", "1MLC", "1DQJ", "1E6J"]#, "1DQJ", "1E6J", "1JPS", "1ZM4"
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    complexes = list(categories['DBD4'])[:5]
    datasets = [DBD5(complexes)]
    classifiers = [SVM(C=10)]
    feature_sets = [
        [
            Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RESIDUE_DEPTH,
            Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
        ],
    ]
    configurations = [datasets[0], classifiers[0], feature_sets[0]]
    gamma = 1.6
    e = KFoldCrossValidation("CV on DBD4")
    e.run([configurations],
          complexes = complexes,
          gamma=gamma,
          n_folds = 5,
          radius=25,
          rNH=15,
          patch_size=2)


def main():
    cv_on_dbd4()


if __name__ == "__main__":
    main()
