from PyML.classifiers import modelSelection
from PyML.classifiers import svm
from PyML.containers import ker
from PyML.evaluators.assess import cvFromFolds

from blrm.configuration import categories_pickle_filename
from blrm.data.dbd.dbd5 import DBD5
from blrm.model.enums import Features
import pickle
from random import shuffle, seed
import numpy as np

param = modelSelection.ParamGrid(svm.SVM(ker.Gaussian()), 'C', [0.4, 0.8], 'kernel.gamma', [1, 10, 100, 1000])
m = modelSelection.ModelSelector(param, measure='roc')
categories = pickle.load(open(categories_pickle_filename, 'rb'))
l = list(categories['DBD4'])
seed(15)
shuffle(l)
dbd4_complexes = l
feature_set = [
        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
        # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.HALF_SPHERE_EXPOSURE,
        Features.PROTRUSION_INDEX,
        Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,

        # Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT,
        # Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT,
        # Features.SURFACE_ATOMS_CURVATURE_COMBINED,

        # Features.D1_PLAIN_SHAPE_DISTRIBUTION,
        # Features.D1_SURFACE_SHAPE_DISTRIBUTION,
        #
        Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION
    ]
dataset = DBD5(dbd4_complexes)
pyml_dataset, training, testing = dataset.get_pyml_cv_from_folds(feature_set, dbd4_complexes, 5)
result = cvFromFolds(m, pyml_dataset, training, testing)
print result
experiment_name = "DBD4-CV-SINGLE-GAMMA=3.2, C=1000/"
directory = results_directory + experiment_name
for i in [2, 5, 10, 15]:
    rfpps, patch_rfpps, patch_res_rfpps, aucs = Analyzer(result, directory).collect_stats(smooth=False,
                                                                                          patch_size=i,
                                                                                          export=False)
    print "Medians\t{0}\t{1}\t{2}\t{3}".format(np.median(rfpps.values()),
                                               np.median(patch_rfpps.values()),
                                               np.median(patch_res_rfpps.values()),
                                               np.median(aucs.values()))