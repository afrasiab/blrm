import sys
from PyML import SVM

from blrm.model.enums import Features
from blrm import experiment

__author__ = 'alex fout (fout@colostate.edu) and basir shariat (basir@rams.colostate.edu)'
feature_dict = {
    "ppSeq":
        [
        # pairpred
        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
    ],
    "ppStruct":[
        Features.HALF_SPHERE_EXPOSURE,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.PROTRUSION_INDEX,
    ],
    "Rdp":[Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT],
    "Ndp":[Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT],
    "sc": [Features.SURFACE_ATOMS_CURVATURE_COMBINED],
    "d1sc":[Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION],
}
feature_dict["pp"] = feature_dict["ppSeq"] + feature_dict["ppStruct"]
feature_dict["curv"] = feature_dict["Rdp"] + feature_dict["Ndp"] + feature_dict["sc"]
feature_dict["pp_curv"] = feature_dict["pp"] + feature_dict["curv"]
feature_dict["pp_Rdp"] = feature_dict["pp"] + feature_dict["Rdp"]
feature_dict["pp_Ndp"] = feature_dict["pp"] + feature_dict["Ndp"]
feature_dict["pp_sc"] = feature_dict["pp"] + feature_dict["sc"]
feature_dict["ppSeq_curv"] = feature_dict["ppSeq"] + feature_dict["curv"]
feature_dict["ppSeq_sc"] = feature_dict["ppSeq"] + feature_dict["sc"]
feature_dict["ppSeq_Rdp"] = feature_dict["ppSeq"] + feature_dict["Rdp"]
feature_dict["ppSeq_Ndp"] = feature_dict["ppSeq"] + feature_dict["Ndp"]
feature_dict["pp_d1sc"] = feature_dict["pp"] + feature_dict["d1sc"]
feature_dict["ppSeq_d1sc"] = feature_dict["ppSeq"] + feature_dict["d1sc"]
feature_dict["pp_sc_d1sc"] = feature_dict["pp_sc"] + feature_dict["d1sc"]
feature_dict["ppSeq_sc_d1sc"] = feature_dict["ppSeq_sc"] + feature_dict["d1sc"]
feature_dict["pp_curv_d1sc"] = feature_dict["pp_curv"] + feature_dict["d1sc"]
feature_dict["ppSeq_curv_d1sc"] = feature_dict["ppSeq_curv"] + feature_dict["d1sc"]

features2scale = [Features.SURFACE_ATOMS_CURVATURE_COMBINED, Features.SURFACE_ATOMS_CURVATURE, Features.SURFACE_ATOMS_CURVATURE_RMSE]

def run_features_cv(feature_indices):
    for feature_index in feature_indices:
        if feature_index not in feature_index:
            raise ValueError("{} is not a recognized feature set".format(feature_index))
    feature_sets = [feature_dict[feature_index] for feature_index in feature_indices]
    classifiers = [SVM()]
    folds = 5
    desc = "-".join(feature_indices)
    experiment.cv_on_dbd4("curv_test_" + desc, feature_sets, classifiers, 5, scale_features=features2scale)
    #experiment.cv_testing("curv_test_" + desc, feature_sets, classifiers, 5, scale_features=features2scale)
    #experiment.train_test_testing("curv_test_" + desc, feature_sets, classifiers, scale_features=features2scale)
    #experiment.train_test_testing("curv_test_" + desc, feature_sets, classifiers)


def main():
    feature_indices = sys.argv[1:]
    run_features_cv(feature_indices)

if __name__ == "__main__":
    main()
