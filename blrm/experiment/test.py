import glob
import numpy as np
import os
import warnings
from Bio.PDB import NeighborSearch
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import basename
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from blrm.configuration import amino_acids, complete_graph_representations_directory, \
    database_pdb_directory, complete_graph_neighbor_initial_search_radius as search_baseline
from blrm.data.common.pdb import PDB
from blrm.data.dbd.utils import feature_dict
from blrm.model.enums import Features
from blrm.model.representation.abstract_represenation import AbstractRepresentation
from blrm.utils.pretty_print import Print



class ResidueCompleteGraph(AbstractRepresentation):
    def __init__(self, protein, vertex_features, chain_code=None, n_neighbors=4, **kwargs):
        AbstractRepresentation.__init__(self, protein, chain_code, **kwargs)
        self._vertex_features = vertex_features
        self.n_neighbors = n_neighbors

    def load_representation(self, name):
        edge_file, vertex_file = self._get_file_name(name)
        edge_matrix = np.load(edge_file)
        vertex_matrix = np.load(vertex_file)
        return edge_matrix, vertex_matrix

    def compute_representation(self):
        pdb_code = self._protein.name

        # generate file names
        if self._chain is None:
            edge_file, vertex_file = self._get_file_name(pdb_code)
        else:
            pdb_code = self._chain.parent.parent.id.split("/")[-1]
            chain_id = "{}.{}".format(pdb_code, self._chain.id)
            edge_file, vertex_file = self._get_file_name(chain_id)
        full_dir = os.path.join(complete_graph_representations_directory, repr(self.n_neighbors))
        if not os.path.exists(full_dir):
            os.makedirs(full_dir)
        if not os.path.exists(edge_file) or not os.path.exists(vertex_file):

            n = len(amino_acids) + 1

            # concatenate all features for each residue into a single matrix
            all_res_mat = None
            for feature in self._vertex_features:
                extractor = feature_dict[feature][0]
                feature_array = extractor(self._protein).load(pdb_code)
                all_res_mat = feature_array if all_res_mat is None else np.hstack((all_res_mat, feature_array))

            # construct graph representation
            edge_mat = np.zeros((self.n_neighbors+1, self.n_neighbors+1, len(self._protein.residues)))
            vertex_mat = np.zeros((self.n_neighbors+1, all_res_mat.shape[1], len(self._protein.residues)))

            #TODO: figure out how to exclude waters and other molecules?
            # there may be water or other molecules listed as "residues" in the pdb
            # we need this list of indices to remove non residue entries from the
            # final_graph_matrix array
            neighbor_search = NeighborSearch(_remove_hetero_atoms(self._protein.atoms))
            # construct a neighborhood graph for each residue
            for i, residue in enumerate(self._protein.biopython_residues):
                # find nearby residues
                residue_coord = get_main_atom_coord(residue)
                neighbor_residues = []
                search_iters = 10
                search_radius = search_baseline
                for j in range(search_iters):
                    neighbor_residues = neighbor_search.search(residue_coord, search_radius * self.n_neighbors**(1./3), level="R")
                    if len(neighbor_residues) >= self.n_neighbors:
                        # print("n_neighbors:{}, j:{}".format(len(neighbor_residues), j))
                        break
                    search_radius *= 2
                # calculate distance to each and find the n_neighbors closest ones
                neighbor_distances = np.zeros(len(neighbor_residues))
                neighbor_coords = np.zeros((len(neighbor_residues), 3))
                for j, neighbor in enumerate(neighbor_residues):
                    neighbor_coords[j] = get_main_atom_coord(neighbor)
                    neighbor_distances[j] = np.linalg.norm(neighbor_coords[j] - residue_coord)
                # select only the n_neighbours closest residues
                sort_indices = list(np.argsort(neighbor_distances))
                neighbor_residues = [neighbor_residues[idx] for idx in sort_indices][:self.n_neighbors]
                neighbor_distances = neighbor_distances[sort_indices][:self.n_neighbors]
                neighbor_coords = neighbor_coords[sort_indices][:self.n_neighbors]
                # construct adj_matrix
                res_idx = self._protein.biopython_residues.index(residue)
                vertex_mat[0, :, i] = all_res_mat[res_idx, :]
                for j in range(self.n_neighbors):
                    edge_mat[0, j, i] = edge_mat[j, 0, i] = neighbor_distances[j]
                    for k in range(j):
                        edge_mat[k, j, i] = edge_mat[j, k, i] = np.linalg.norm(neighbor_coords[k] - neighbor_coords[j])
                    # print "\n".join([repr(res) for res in self._protein.biopython_residues])
                    # print neighbor_residues[j]
                    res_idx = self._protein.biopython_residues.index(neighbor_residues[j])
                    vertex_mat[j, :, i] = all_res_mat[res_idx, :]

            np.save(edge_file, edge_mat)
            np.save(vertex_file, vertex_mat)
        return True

    def _get_file_name(self, name):
        return (complete_graph_representations_directory + "{}/{}_edge.npy".format(self.n_neighbors, name),
                complete_graph_representations_directory + "{}/{}_vert.npy".format(self.n_neighbors, name))



def get_main_atom_coord(residue):
    if 'CA' in residue:
        return residue['CA'].coord
    elif 'CB' in residue:
        return residue['CB'].coord
    elif 'N' in residue:
        return residue['N'].coord
    else:
        c = None
        for a in residue:
            c = a.coord if c is None else c+a.coord
        c /= len(residue)
        return c

def _remove_hetero_atoms(atoms):
    homo_atoms = []
    for atom in atoms:
        if atom.parent.id[0] == ' ':
            homo_atoms.append(atom)
    return homo_atoms


def read_pdbs(the_complex):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        ligand_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_b.pdb"))
        receptor_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_b.pdb"))
        ligand_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_u.pdb"))
        receptor_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_u.pdb"))
        bound_formation = ProteinPair(ligand_bound, receptor_bound)
        unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
        return ProteinComplex(the_complex, unbound_formation, bound_formation)


def main():
    features = [
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.PROTRUSION_INDEX,
    ]
    complexes = ["1FQJ", "1BVK"]
    for counter, complex_code in enumerate(complexes):
        Print.print_info("{0}/{1}... processing complex {2}".format(counter + 1, len(complexes), complex_code))
        complex_object_model = read_pdbs(complex_code)
        for protein in [complex_object_model.unbound_formation.ligand, complex_object_model.unbound_formation.receptor]:
            ResidueCompleteGraph(protein, features, None, 4).compute_representation()



if __name__ == '__main__':
    main()