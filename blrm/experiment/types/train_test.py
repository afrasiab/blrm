__author__ = 'basir shariat (basir@rams.colostate.edu)'

from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.experiment.types.experiment_mixin import ExperimentMixin
from blrm.utils.pretty_print import Print


class TrainTest(ExperimentMixin):

    def __init__(self, configurations):
        ExperimentMixin.__init__(self, configurations)

    def load(self, file_name=None):
        super(TrainTest, self).load(file_name)

    def save(self, file_name=None):
        super(TrainTest, self).save(file_name)

    def _run(self, configuration, **kwargs):
        current_run = self._runs_history[-1]
        [dataset, classifier, feature_set] = configuration

        if 'train_test' in kwargs:
            training_complexes, testing_complexes = kwargs['train_test']
        elif 'train_test_ratio' in kwargs:
            training_complexes, testing_complexes = dataset.get_pyml_train_test_ratio(kwargs['train_test_ratio'])
        else:
            raise ValueError(
                'Either train_test parameter should contain the list of complexes for train and test sets or'
                ' train_test_ratio parameter should be set in order to divide all the complex to train and '
                'test sets randomly')
        train, test = dataset.get_pyml_train_test(feature_set, training_complexes, testing_complexes, **kwargs)
        Print.print_info("Training the Classifier")
        classifier.train(train)
        result = classifier.test(test)
        print result
        key = self._get_configuration_key(configuration, **kwargs)
        current_run['result'][key] = result, ExperimentAnalysis(result, current_run['output directory']).collect_stats(patch_size=2)
        configuration[1].save("{}{}.model".format(current_run['output directory'], len(current_run['result'])))

