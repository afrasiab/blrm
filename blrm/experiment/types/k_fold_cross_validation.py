__author__ = 'basir shariat (basir@rams.colostate.edu)'

from PyML.evaluators.assess import cvFromFolds

from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.configuration import default_k_for_k_fold_cv
from blrm.experiment.types.cross_validation_mixin import CrossValidationMixin


class KFoldCrossValidation(CrossValidationMixin):
    def load(self, file_name=None):
        pass

    def save(self, file_name=None):
        super(KFoldCrossValidation, self).save(file_name)

    def _run(self, configuration, **kwargs):
        current_run = self._runs_history[-1]
        [dataset, classifier, feature_set] = configuration
        complexes = dataset.complexes if 'complexes' not in kwargs else kwargs['complexes']
        pyml_dataset, training, testing = dataset.get_pyml_cv_from_folds(feature_set, complexes,  **kwargs)
        result = cvFromFolds(classifier, pyml_dataset, training, testing)
        key = self._get_configuration_key(configuration, **kwargs)
        current_run['result'][key] = result
        print result
