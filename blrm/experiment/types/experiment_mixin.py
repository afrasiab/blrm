import getpass
from datetime import datetime

import socket

import psutil
from abc import ABCMeta, abstractmethod
from blrm.configuration import results_directory, verbose
from blrm.data.dbd.utils import feature_dict
from blrm.utils.email_notifier import EmailNotifier
from blrm.utils.pretty_print import Print
import os
import cPickle


class ExperimentMixin(object):
    __metaclass__ = ABCMeta
    _ran_by = ""
    _machine = ""
    _name = ""
    _description = ""
    _output_directory = ""
    _runs_history = list()
    _previous_configurations = set()
    _check_previous_runs = True
    _email_notifier = None

    def __init__(self, name, description="", emails=None, output_directory=None):
        self._name = name
        self._description = description
        results_folder_output = "{}{}/".format(results_directory, self._name)
        self._output_directory = results_folder_output if output_directory is None else output_directory
        if not os.path.exists(self._output_directory):
            os.makedirs(self._output_directory)
        self._ran_by = getpass.getuser()
        self._machine = ExperimentMixin.get_machine_info()
        if emails is not None:
            self._email_notifier = EmailNotifier(emails)

    @abstractmethod
    def _run(self, configuration, **kwargs):
        pass

    @abstractmethod
    def save(self, file_name=None):
        file_path = self._output_directory + "runs.cPickle"
        cPickle.dump((self._runs_history, self._previous_configurations, self._output_directory), open(file_path, "w"))

    def directory(self):
        return self._output_directory

    @abstractmethod
    def load(self, file_name=None):
        file_path = self._output_directory + "runs.cPickle"
        (self._runs_history, self._previous_configurations, self._output_directory) = cPickle.load(open(file_path))

    def _clean_up(self):
        os.rmdir(self._runs_history[-1]['output directory'])

    def run(self, configurations, **kwargs):
        self._pre_run()
        for i, configuration in enumerate(configurations):
            self.check_previous_runs(configuration, kwargs)
            self._run(configuration, **kwargs)
            # saving the trained model
        self._post_run()
        # self._clean_up()

    def check_previous_runs(self, configuration, kwargs):
        key = self._get_configuration_key(configuration, **kwargs)
        if 'check_previous_runs' in kwargs:
            self._check_previous_runs = kwargs['check_previous_runs']
        if key in self._previous_configurations and self._check_previous_runs:
            response = input("Another run with the same configuration and parameters exist "
                             "under this experiment, do you still want to continue?(yes/no)")
            if response != "yes":
                exit()
        self._previous_configurations.add(key)

    def _pre_run(self):
        current_run = {}
        self._runs_history.append(current_run)
        current_run['start'] = datetime.now()
        current_run['result'] = {}
        current_run['output directory'] = "{}{}/".format(self._output_directory, len(self._runs_history))
        start_time = unicode(current_run['start'].replace(microsecond=0))
        name = self._name[:min(len(self._name), 32)] + ("..." if len(self._name) > 32 else "")
        if not os.path.exists(current_run['output directory']):
            os.makedirs(current_run['output directory'])
        if verbose:
            Print.print_info("{}: Running \"{}\" ".format(start_time, name))
        if self._email_notifier is not None:
            self._email_notifier.send("PAIRPred 2.0: Experiment {} is Running".format(self._name),
                                      "Description: {}\n"
                                      "Running by: {}\n"
                                      "Running On :\n {}\n".format(self._description, self._ran_by, self._machine))

    def _post_run(self):
        current_run = self._runs_history[-1]
        current_run['finish'] = datetime.now()
        self.save()
        duration = current_run['finish'] - current_run['start']
        if verbose:
            finish_time = unicode(current_run['finish'].replace(microsecond=0))
            Print.print_info("{}: \"{}\" ran for {}".format(finish_time, self._name, duration)[:-7])
        if self._email_notifier is not None:
            self._email_notifier.send("PAIRPred 2.0: Experiment {} finished!".format(self._name[:30]),
                                      "Description: {}\n"
                                      "Ran On :\n {}\n"
                                      "Total Running time: {}\n"
                                      "Performance Summary :\n {}\n".format(self._description, self._machine, duration,
                                                                            "@TODO"))

    def _get_configuration_key(self, configuration, **kwargs):
        [dataset, classifier, feature_set] = configuration
        key = "Features:\n"
        for feature in feature_set:
            key += "\t{}\n".format(feature_dict[feature][2])
        classifier_key = ("{}".format(classifier)).replace("\n", "\n\t")
        key += "Classifier:\n\t{}\n".format(classifier_key)
        key += "Dataset:\n\t{}\n".format(dataset.name)
        key += "Parameters:\n"
        for k in kwargs:
            key += "\t{}: {}\n".format(k, kwargs[k])
        return key

    def _dump_container(self, container):
        representation = ""
        if type(container) == dict:
            for key, value in container.items():
                if hasattr(value, '__iter__'):
                    return "{}\n{}".format(key, self._dump_container(value))
                else:
                    representation += '%s : %s' % (key, value)
        elif type(container) == list:
            for value in container:
                if hasattr(value, '__iter__'):
                    self._dump_container(value)
                else:
                    representation += value
        else:
            return representation + container
        return representation

    @staticmethod
    def get_machine_info():
        gb = 1e9
        host_fqd_name = socket.getfqdn()
        ip = socket.gethostbyname(socket.gethostname())
        n_cpu = psutil.cpu_count()
        mem = psutil.virtual_memory().total / gb
        curr_cpu = psutil.cpu_percent()
        curr_mem = psutil.virtual_memory().used / gb
        used_mem_per = curr_mem * 100 / mem
        return "Host: {} (IP:{})\n" \
               "Number of cores: {}\n" \
               "Physical Memory Size: {:.0f} GB\n" \
               "Current CPU Usage: {}%\n" \
               "Used Memory: {:.0f} GB ({:.0f}%)".format(host_fqd_name, ip, n_cpu, mem, curr_cpu, curr_mem, used_mem_per)

if __name__ == '__main__':
    print ExperimentMixin.get_machine_info()