from PyML import SVM
import pickle
from blrm.data.dbd.dbd5 import DBD5
from blrm.experiment.types.train_test import TrainTest
from blrm.model.enums import Features
from blrm.configuration import categories_pickle_filename

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def train_on_dbd4_test_on_dbd5():
    # training, testing = ["1AHW"], ["1MLC"]
    # training, testing = ["1AHW", "1BVK", "1E6J", "1JPS", "1ZM4"], ["1MLC", "1DQJ"]#
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    training.append("2B42")
    testing.append("3R9A")
    datasets = [DBD5(list(set(training + testing)), )]
    classifiers = [SVM(C=100)]
    feature_sets = [
        [
            # Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            # Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
            # Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            # Features.RESIDUE_DEPTH,
            # Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            # Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION
            # Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
        ],
    ]
    configurations = [datasets[0], classifiers[0], feature_sets[0]]
    gamma = 1.6
    e = TrainTest("DBD4-DBD5")
    e.run([configurations],
          train_test=[training, testing],
          gamma=gamma,
          radius=25,
          rNH=15)


def main():
    train_on_dbd4_test_on_dbd5()


if __name__ == "__main__":
    main()
