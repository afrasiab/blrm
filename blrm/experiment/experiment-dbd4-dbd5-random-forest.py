import itertools
import pickle
from datetime import datetime

import numpy as np
import os
from PyML.evaluators.assess import cvFromFolds

from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.ml.random_forest import RandomForest
from blrm.configuration import categories_pickle_filename, results_directory
from blrm.data.dbd.dbd5 import DBD5
from blrm.data.dbd.interface_inclusion_examples_extractor import InterfaceInclusionExampleExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.model.enums import Features, EvaluationType
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Experiment:
    def __init__(self, configurations):
        [self.datasets, self.classifiers, self.feature_sets] = configurations
        self.configurations = list(itertools.product(*configurations))
        self.pyml_result = {}
        self.description = None
        self.out_directory = None
        self.duration = None
        self.start = None

    def run(self, desc, evaluation=EvaluationType.CV, **kwargs):
        self.description = desc
        self.start = datetime.now()
        self.out_directory = results_directory + self.description + "/"
        if not os.path.exists(self.out_directory):
            os.makedirs(self.out_directory)
        Print.print_info("Running experiment {0}".format(self.description))

        for configuration in self.configurations:
            [dataset, classifier, feature_set] = configuration
            features_key = ""
            for feature in feature_set:
                features_key += feature_dict[feature][2] + "-"

            if evaluation == EvaluationType.TRAIN_TEST:
                if 'train_test' in kwargs:
                    training_complexes, testing_complexes = kwargs['train_test']
                elif 'train_test_ratio' in kwargs:
                    training_complexes, testing_complexes = dataset.get_pyml_train_test_ratio(
                        kwargs['train_test_ratio'])
                else:
                    raise ValueError(
                        'Either train_test parameter should contain the list of complexes for train and test sets or'
                        ' train_test_ratio parameter should be set in order to divide all the complex to train and '
                        'test sets randomly')
                # np.random.shuffle(testing_complexes)
                training = dataset.get_bags(feature_set, training_complexes, 200, "training dataset")
                # validation = dataset.get_bags(feature_set, training_complexes, 200, "validation dataset")
                classifier.train(training, validation=None)
                rfpp = {}
                aucs = {}

                # for complex_code in training_complexes:
                #     bags = dataset.get_bags(feature_set, [complex_code], 20)
                #     r, n, a, s, auprc = classifier.get_rfpps(bags)
                #     del bags
                #     print "{}\t{}\t{:3.3f}".format(complex_code, r[complex_code], a[complex_code])

                for complex_code in testing_complexes:
                    # bags = dataset.get_bags(feature_set, [complex_code], -1)
                    bags = dataset.get_bags(feature_set, [complex_code], -1)
                    r, n, a, s, auprc = classifier.get_rfpps(bags)
                    rfpp[complex_code], aucs[complex_code] = r.values()[0], a.values()[0]
                    del bags
                    print "{}\t{}\t{:3.3f}".format(complex_code, rfpp[complex_code], aucs[complex_code])
                print np.median(rfpp.values())
                print np.median(aucs.values())
            elif evaluation == EvaluationType.CV:
                if 'complexes' not in kwargs:
                    kwargs['complexes'] = dataset.complexes
                n_folds = kwargs['folds']
                if 'folds' in kwargs:
                    folds = n_folds
                else:
                    raise ValueError('For CV you should provide number of folds')

                complexes = kwargs['complexes']
                pyml_dataset, training, testing = dataset.get_pyml_cv_from_folds(feature_set, complexes, n_folds)
                result = cvFromFolds(classifier, pyml_dataset, training, testing)
                complexes_key = "-".join(complexes)
                result_key = (features_key, dataset.name, desc, evaluation, complexes_key)
                # smoothed_analysis = Analyzer(result, self.out_directory).analyze(export=True)
                non_smoothed_analysis = ExperimentAnalysis(result, self.out_directory).collect_stats(smooth=False)
                self.pyml_result[result_key] = (result, non_smoothed_analysis)
            elif evaluation == EvaluationType.LOO:
                pass
            else:
                raise ValueError("Unknown evaluation type!")
        self.duration = datetime.now() - self.start
        print self.start
        print self.duration
        self.__save_results()

    def __save_results(self):
        pickle.dump(self.pyml_result, open(self.out_directory + "results.pickle", "w"))
        pass


def train_on_dbd4_test_on_dbd5():
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    datasets = [DBD5(list(set(training + testing)), check_features=False)]
    classifiers = [RandomForest()]
    # testing= training = ["1SBB"]
    training = testing + training
    feature_sets = [
        [
            Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RESIDUE_DEPTH,
            Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
            # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D2_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION
        ],
    ]
    configurations = [datasets, classifiers, feature_sets]

    e = Experiment(configurations)
    e.run("DBD4-DBD5-RFPP-LINEAR-KERNEL", EvaluationType.TRAIN_TEST, train_test=(training, testing))


def main():
    train_on_dbd4_test_on_dbd5()


if __name__ == "__main__":
    main()
