import itertools
import pickle
from datetime import datetime

import numpy as np
import os
from PyML.evaluators.assess import cvFromFolds

from blrm.analysis.experiment_analysis import ExperimentAnalysis
from blrm.ml.rankers.offline_linear_rfpp import OfflineLinearTopK
from blrm.ml.rankers.rfpp_svm import SGDRFPPSVM, SSGDRFPPSVM, KernelizedSSGDRFPPSVM, PSSGDRFPPSVM, \
    sum_proxy_rfpp_svm
from blrm.ml.rankers.rfpp_online_ensemble import RFPPOnlineEnsemble, RFPPOnlinePatchEnsemble
from blrm.ml.rankers.kernelized_RFPP_optimizer import KernelizedLMRFPPOptimizer
from blrm.ml.rankers.online_linear_rfpp import OnlineLinearTopK
from blrm.ml.rankers.offline_kernelized_rfpp import OfflineKernelizedTopK
from blrm.ml.rankers.clusterized_kernel_topk import ClusteredKernelizedTopK

from blrm.configuration import categories_pickle_filename, results_directory
from blrm.data.dbd.dbd5 import DBD5
from blrm.data.dbd.interface_inclusion_examples_extractor import InterfaceInclusionExampleExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.ml.rankers.ssrfpp_tensorflow import BLRM
# from pairpred.ml.rankers.ssrfpp_theano import SSRFPP
from blrm.model.enums import Features, EvaluationType
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Experiment:
    def __init__(self, configurations):
        [self.datasets, self.classifiers, self.feature_sets] = configurations
        self.configurations = list(itertools.product(*configurations))
        self.pyml_result = {}
        self.description = None
        self.out_directory = None
        self.duration = None
        self.start = None

    def run(self, desc, evaluation=EvaluationType.CV, **kwargs):
        self.description = desc
        self.start = datetime.now()
        self.out_directory = results_directory + self.description + "/"
        if not os.path.exists(self.out_directory):
            os.makedirs(self.out_directory)
        Print.print_info("Running experiment {0}".format(self.description))

        for configuration in self.configurations:

            [dataset, classifier, feature_set] = configuration
            Print.print_info("Ranker {0}".format(classifier.__class__.__name__))

            if evaluation == EvaluationType.TRAIN_TEST:
                training_complexes, validation_complexes, testing_complexes = kwargs['data']
                training = dataset.get_bags(feature_set, training_complexes, 10, "building training dataset", gap=30)
                # validation = dataset.get_bags(feature_set, validation_complexes, -1)
                classifier.train(training)
                del training
                print "Training time {}".format(datetime.now() - self.start)
                rfpp, aucs = {}, {}
                # seed = 1
                for ii, complex_code in enumerate(validation_complexes):
                    bags = dataset.get_bags(feature_set, [complex_code], -1)
                    r, a = classifier.get_rfpps(bags)
                    rfpp[complex_code], aucs[complex_code] = r.values()[0], a.values()[0]
                    del bags
                    print "{}/{}\t{}\t{}\t{:3.3f}".format(ii + 1,
                                                          len(validation_complexes),
                                                          complex_code,
                                                          rfpp[complex_code],
                                                          aucs[complex_code])
                bags = dataset.get_bags(feature_set, testing_complexes, -1,  "building testing dataset")
                rfpp, aucs = classifier.get_rfpps(bags)
                for key in rfpp:
                    print("{}\t{:4.4f}\t{}\t{:3.3f}".format(key, float(rfpp[key])/bags[key][0].shape[0], rfpp[key], aucs[key]))
                # median = np.median(rfpp.values())
                print self.start
                self.duration = datetime.now() - self.start
                print self.duration
                print "Max RFPP {}".format(max(rfpp.values()))
                return max(rfpp.values())

    def __save_results(self):
        pickle.dump(self.pyml_result, open(self.out_directory + "results.pickle", "w"))
        pass


# def train_on_dbd4_test_on_dbd5():
#     # training, testing = ["1AHW"], ["1MLC"]
#     categories = pickle.load(open(categories_pickle_filename, 'rb'))
#     training, testing = map(list, categories['DBD4_DBD5'])
#
#     validation_ratio = 0
#     validation_index = int(len(training) * validation_ratio)
#     np.random.shuffle(training)
#     validation = training[:validation_index]
#     training = training[validation_index:]
#
#     datasets = [DBD5(list(set(training + testing + validation)))]
#     # classifiers = [RFPPOnlineEnsemble()]
#     # classifiers = [RFPPOnlinePatchEnsemble()]
#     classifiers = [TFSSRFPP()]
#     # classifiers = [SSGDRFPPSVM()]
#     # classifiers = [SSRFPP()]
#     # classifiers = [OfflineLinearTopK()]
#     # classifiers = [OnlineLinearTopK()]
#     # classifiers = [SSGDRFPPSVM()]
#     # classifiers = [OfflineKernelizedTopK()]
#     # classifiers = [KernelizedLMRFPPOptimizer()]
#     # classifiers = [ClusteredKernelizedTopK()]
#     # classifiers = [KernelizedSSGDRFPPSVM()]
#     # classifiers = [sum_proxy_rfpp_svm()]
#     # classifiers = [SVM(optimizer='pegasos', C=10)]
#
#     feature_sets = [
#         [
#             Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
#             # Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
#             Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
#             Features.RESIDUE_DEPTH,
#             Features.HALF_SPHERE_EXPOSURE,
#             Features.PROTRUSION_INDEX,
#             Features.D1_PLAIN_SHAPE_DISTRIBUTION,
#             Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
#             Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION,
#             Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
#             Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
#             Features.D2_PLAIN_SHAPE_DISTRIBUTION,
#             Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION
#         ],
#     ]
#     configurations = [datasets, classifiers, feature_sets]
#
#     e = Experiment(configurations)
#     e.run("DBD4-DBD5-RFPP", EvaluationType.TRAIN_TEST, data=(training, validation, testing))


def bo_rfpp(the_lambda, learning_rate):
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])

    validation_ratio = 0.1
    validation_index = int(len(training) * validation_ratio)
    np.random.shuffle(training)
    validation = training[:validation_index]
    training = training[validation_index:]

    datasets = [DBD5(list(set(training + testing + validation)))]
    classifiers = [BLRM(the_lambda=the_lambda, learning_rate=learning_rate)]

    feature_sets = [
        [
            Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            # Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RESIDUE_DEPTH,
            # Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            # Features.D1_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
            # Features.D2_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION
        ],
    ]
    configurations = [datasets, classifiers, feature_sets]
    e = Experiment(configurations)
    return float(e.run("DBD4-DBD5-RFPP", EvaluationType.TRAIN_TEST, data=(training, validation, testing)))


def main(job_id, params):
    return bo_rfpp(params['the_lambda'], params['learning_rate'])


print main(0, {'the_lambda': 0.01, 'n_epochs': 500, 'learning_rate': 0.01})
