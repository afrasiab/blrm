import itertools
import pickle
from datetime import datetime

import numpy as np
import os

from blrm.configuration import categories_pickle_filename, results_directory, default_fixed_neighbours_number
from blrm.data.dbd.dbd5 import DBD5
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.ml.rankers.rfpp_online_ensemble import RFPPOnlineEnsemble
from blrm.model.enums import Features, EvaluationType
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Experiment:
    def __init__(self, configurations):
        [self.datasets, self.classifiers, self.feature_sets] = configurations
        self.configurations = list(itertools.product(*configurations))
        self.pyml_result = {}
        self.description = None
        self.out_directory = None
        self.duration = None
        self.start = None

    def export_to_csv(self, bags, complex_scores):
        csv_directory = self.out_directory + "csv/"

        if not os.path.exists(csv_directory):
            os.mkdir(csv_directory)
        for complex_code in complex_scores:
            f_r_s = open(csv_directory + complex_code + "_residue_scores.csv", "w")

            scores = np.array(complex_scores[complex_code])
            indices = (-scores).argsort()
            scores = scores[indices]

            ligand, receptor = complex_code + "_l_u", complex_code + "_r_u"
            l_ids, r_ids = ResidueFullIDExtractor(None).load(ligand), ResidueFullIDExtractor(None).load(receptor)
            examples = bags[complex_code].values()[0][2]
            examples = examples[indices]
            for i, score in enumerate(scores):
                f_r_s.write("{0}, {1}, {2}\n".format(l_ids[examples[i, 0]], r_ids[examples[i, 1]], score))
            f_r_s.close()


    def compute_individual_scores(self, bags, complex_scores):
        scores_directory = self.out_directory + "scores/"

        if not os.path.exists(scores_directory):
            os.mkdir(scores_directory)
        for complex_code in complex_scores:
            l_s_f = open(scores_directory + complex_code + "_ligand_scores.csv", "w")
            r_s_f = open(scores_directory + complex_code + "_receptor_scores.csv", "w")
            scores = np.array(complex_scores[complex_code])
            ligand, receptor = complex_code + "_l_u", complex_code + "_r_u"
            l_ids, r_ids = ResidueFullIDExtractor(None).load(ligand), ResidueFullIDExtractor(None).load(receptor)
            examples = bags[complex_code].values()[0][2]
            for i in np.unique(examples[:, 0]):
                l_s_f.write("{0}, {1}\n".format(l_ids[i], np.max(scores[np.where(examples[:, 0]==i)[0]])))
            l_s_f.close()
            for i in np.unique(examples[:, 1]):
                r_s_f.write("{0}, {1}\n".format(r_ids[i], np.max(scores[np.where(examples[:, 1]==i)[0]])))
            r_s_f.close()

    def run(self, desc, evaluation=EvaluationType.CV, bottom_k=.1, **kwargs):
        self.description = desc
        self.start = datetime.now()
        self.out_directory = results_directory + self.description + "/"
        if not os.path.exists(self.out_directory):
            os.makedirs(self.out_directory)
        Print.print_info("Running experiment {0}".format(self.description))
        # values = np.load(os.path.join(self.out_directory, "params.npz"))
        for configuration in self.configurations:
            [dataset, classifier, feature_set] = configuration
            features = ""
            for jj, f in enumerate(feature_set):
                features +=  "\t{}-".format(jj+1) + feature_dict[f][2] + "\n"
            print("Features: \n{}".format(features))
            print("Number of estimators: {}".format(len(classifier.estimators)))
            Print.print_info("Ranker {0}".format(classifier.__class__.__name__))
            # features_key = "-".join([feature_dict[f][2] for f in feature_set])

            if evaluation == EvaluationType.TRAIN_TEST:
                training_complexes, validation_complexes, testing_complexes = kwargs['data']
                training_sets = []
                for i in range(len(classifier.estimators)):
                    tr_ration = kwargs['training_ratio'] if 'training_ratio' in kwargs else 10
                    training_sets.append(dataset.get_bags(feature_set, training_complexes, tr_ration, gap=0, neighbours=False))
                # training_sets = dataset.get_bags(feature_set, training_complexes, 10, gap=20)  #, neighbours=True
                validation = dataset.get_bags(feature_set, validation_complexes, -1, neighbours=False)
                # ttttt = dataset.get_bags(feature_set, testing_complexes, -1)
                classifier.train(training_sets, validation=None, k=kwargs['k'])
                del training_sets
                print "Training time {}".format(datetime.now() - self.start)
                print "k = {}".format(kwargs['k'])
                rfpp, aucs, scores, rkpps, auprcs = {}, {}, {}, {}, {}
                bags = {}
                for ii, a_complex in enumerate(testing_complexes):
                    bag = dataset.get_bags(feature_set, [a_complex], -1, neighbours=False)
                    bags[a_complex] = bag  # , neighbours=True
                    r, n, a, s, auprc = classifier.get_rfpps(bag, k=kwargs['k'])
                    rfpp[a_complex], aucs[a_complex], scores[a_complex] = r.values()[0], a.values()[0], s.values()[0]
                    auprcs[a_complex] = auprc[a_complex]
                    # rkpps[a_complex] = k[a_complex]
                    print ("{}/{}\t{}\t{}\t" 
                          # \t{}\t{}
                          "{:3.3f}\t{:3.3f}".format(ii + 1,
                                                          len(testing_complexes),
                                                          a_complex,
                                                          rfpp[a_complex],
                                                          # n[a_complex],
                                                          # k[a_complex],
                                                          aucs[a_complex],
                                                          auprc[a_complex]
                                                                   ))
                params = classifier.get_model_params()
                np.savez(os.path.join(self.out_directory, "params.npz"), *params)
                print np.median(rfpp.values())
                print np.mean(aucs.values())
                print np.mean(auprcs.values())
                # print np.mean(rkpps.values())

                sorted_keys = sorted(rfpp, key=rfpp.get)[-int(len(testing_complexes) * bottom_k):]
                print "\n\nBottom {}% percent performing complexes:".format(bottom_k * 100)
                for ii, a_complex in enumerate(sorted_keys):
                    print "{}/{}\t{}\t{}\t{:3.3f}".format(ii + 1,
                                                          len(sorted_keys),
                                                          a_complex,
                                                          rfpp[a_complex],
                                                          aucs[a_complex])
                self.export_to_csv(bags, scores)
                self.compute_individual_scores(bags, scores)
            elif evaluation == EvaluationType.LOO:
                complexes = kwargs['data']
                import copy
                rfpps, aucs = {}, {}

                for ii, complex_code in enumerate(complexes):
                    training = copy.copy(complexes)
                    training.pop(training[ii])
                    bags = dataset.get_bags(feature_set, training, 10, "building training dataset", gap=0)
                    classifier.train(bags)
                    del training
                    testing_complex = dataset.get_bags(feature_set, [complex_code], -1, "building testing dataset")
                    rfpp, auc = classifier.get_rfpps(testing_complex)
                    rfpps.update(rfpp)
                    aucs.update(auc)
                counter = 0
                for key in rfpps:
                    counter += 1
                    print("{}/{}\t{}\t{}\t{:3.3f}".format(counter, len(aucs), key, rfpps[key], aucs[key]))
                # median = np.median(rfpp.values())
                print self.start
                self.duration = datetime.now() - self.start
                print self.duration
                print "Max RFPP {}".format(max(rfpp.values()))
                return max(rfpp.values())
            else:
                raise ValueError("Unknown evaluation type!")
        self.duration = datetime.now() - self.start
        print self.start
        print self.duration
        self.__save_results()

    @staticmethod
    def __get_folds(complexes, n_folds):
        folds = [[] for _ in range(n_folds)]
        for i, a_complex in enumerate(complexes):
            folds[i%n_folds].append(a_complex)
        return folds

    def __save_results(self):
        pickle.dump(self.pyml_result, open(self.out_directory + "results.pickle", "w"))
        pass


def train_on_dbd4_test_on_dbd5():
    # training, testing = ["1AHW"], ["1MLC"]

    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    training, testing = map(list, categories['DBD4_DBD5'])
    # training.append("2B42")
    # testing.append("3R9A")
    # training.extend(testing)
    # testing = ["2wpt", "3bx1", "3E8L", "3fm8", "3r2x", "3u43", "4eef", "4G9S"]
    # n_tr, n_ts = len(training), len(testing)
    # l = training + testing
    # np.random.shuffle(l)
    # training = l[:n_tr]
    # testing = l[n_tr:]
    # training = list(set(training) - set(bad_complexes))
    validation_ratio = 0.
    validation_index = int(len(training) * validation_ratio)
    # np.random.shuffle(testing)
    # validation = []
    training = training#training[validation_index:]+testing
    # training = training[validation_index:]
    validation =  training[:validation_index]
    # training = ["1ACB", "1AK4", "1AVX", "1AY7", "1B6C", "1BKD", "1BUH", "1BVN", "1CGI", "1CLV", "1D6R", "1DFJ", "1E6E", "1E96", "1EAW", "1EFN", "1F34", "1F6M", "1FFW", "1FLE", "1GCQ", "1GHQ", "1GL1", "1GLA", "1GPW", "1GXD", "1H1V", "1H9D", "1HE1", "1I2M", "1IRA", "1J2J", "1JIW", "1JK9", "1JTG", "1KAC", "1KTZ", "1KXQ", "1LFD", "1M10", "1MAH", "1MQ8", "1NW9", "1OC0", "1OPH", "1OYV", "1PPE", "1PVH", "1PXV", "1QA9", "1R0R", "1R6Q", "1S1Q", "1SBB", "1SYX", "1T6B", "1UDI", "1US7", "1XD3", "1YVB", "1Z5Y", "1ZHH", "1ZHI", "1ZLI", "1ZM4", "2A5T", "2A9K", "2ABZ", "2AJF", "2AYO", "2B42", "2C0L", "2CFH", "2FJU", "2G77", "2HLE", "2HQS", "2HRK", "2I25", "2I9B", "2IDO", "2J0T", "2J7P", "2NZ8", "2O3B", "2O8V", "2OOB", "2OT3", "2OUL", "2OZA", "2PCC", "2SIC", "2SNI", "2UUY", "2VDB", "2Z0E", "3CPH", "3D5S", "3SGQ", "4CPA", "7CEI", "BOYV"]
    # training = ["1ACB", "1AK4", "1AVX", "1AY7", "1B6C", "1BKD", "1BUH", "1BVN", "1CGI", "1CLV", "1D6R", "1DFJ", "1E6E", "1E96", "1EAW", "1EFN", "1F34", "1F6M", "1FFW", "1FLE", "1GCQ", "1GHQ", "1GL1", "1GLA", "1GPW", "1GXD", "1H1V", "1H9D", "1HE1", "1I2M", "1IRA", "1J2J", "1JIW", "1JK9", "1JTD", "1JTG", "1KAC", "1KTZ", "1KXQ", "1LFD", "1M10", "1MAH", "1MQ8", "1NW9", "1OC0", "1OPH", "1OYV", "1PPE", "1PVH", "1PXV", "1QA9", "1R0R", "1R6Q", "1RKE", "1S1Q", "1SBB", "1SYX", "1T6B", "1UDI", "1US7", "1XD3", "1YVB", "1Z5Y", "1ZHH", "1ZHI", "1ZLI", "1ZM4", "2A1A", "2A5T", "2A9K", "2ABZ", "2AJF", "2AYO", "2B42", "2C0L", "2CFH", "2FJU", "2G77", "2GAF", "2GTP", "2HLE", "2HQS", "2HRK", "2I25", "2I9B", "2IDO", "2J0T", "2J7P", "2NZ8", "2O3B", "2O8V", "2OOB", "2OT3", "2OUL", "2OZA", "2PCC", "2SIC", "2SNI", "2UUY", "2VDB", "2X9A", "2YVJ", "2Z0E", "3A4S", "3AAD", "3BIW", "3BX7", "3CPH", "3D5S", "3DAW", "3F1P", "3FN1", "3H2V", "3K75", "3PC8", "3S9D", "3SGQ", "3VLB", "4CPA", "4FZA", "4H03", "4IZ7", "4M76", "7CEI", "BAAD", "BOYV" ]
    # training = list(set(training).difference(set(testing)))
    # testing = list(set(testing).intersection(set(testing)))
    # np.random.shuffle(training)
    # testing = training[:20]
    # training = training[20:]
  #   training = ["1VFB", "1H9D", "1BGX", "2G77", "1WQ1", "4CPA", "1AHW", "1QFW", "1RLB", "1BJ1", "1F34", "1GHQ", "1D6R", "2A9K",
  # "1JZD", "1HE8", "1GLA", "2HMI", "2NZ8", "1OFU", "1GL1", "1ATN", "2OUL", "1OYV", "1K74", "1ZHI", "1AKJ", "1NW9",
  # "2B4J", "2Z0E", "1PVH", "1ZLI", "1R0R", "1TMQ", "1EFN", "1ZHH", "1B6C", "1F6M", "2J0T", "1EZU", "1KAC", "1E6J",
  # "1FQ1", "3D5S", "1MLC", "1YVB", "1F51", "1ML0", "1WDW", "1JIW", "2O3B", "1I9R", "1BVN", "1BKD", "1CLV", "1E6E",
  # "2CFH", "1XQS", "2MTA", "1Z5Y", "1EAW", "1CGI", "1FQJ", "1EWY", "2HRK", "9QFW", "1IB1", "1SBB", "2HLE", "1FC2",
  # "2J7P", "2AJF", "1OC0", "1E96", "1KKL", "1WEJ", "1RV6", "1S1Q", "1E4K", "2BTF", "1AY7", "3CPH", "1QA9", "2OOB",
  # "1IRA", "1FLE", "1H1V", "1US7", "1XU1", "1FCC", "1MAH", "2VIS", "1KXP", "1IBR", "1AK4", "1GCQ", "1EER", "1I4D",
  # "1J2J", "1NCA", "1I2M", "1IJK", "2O8V", "1Y64", "1JPS", "2FJU", "1DQJ", "1KLU", "2HQS", "1NSN", "2VDB", "2OT3",
  # "1SYX", "2I25", "1KXQ", "1FAK", "1XD3", "2JEL", "2SIC", "1PPE", "2IDO", "2OOR", "1DE4", "3SGQ", "1GRN", "1BUH",
  # "2H7V", "1JTG", "1OPH", "1HCF", "1JWH", "1Z0K", "1K5D", "1GP2", "1AZS", "1R6Q", "1JK9", "1JMO", "1PXV"]
  #   validation = ["1ZM4", "1HE1", "1ACB", "2SNI", "1GPW", "2ABZ", "2UUY", "1DFJ", "1A2K", "1FFW", "1MQ8", "1N2C", "1IQD", "2AYO",
  # "2OZA", "1KTZ", "2FD6", "1GXD", "1HIA", "1R8S", "2C0L", "1UDI", "3BP8", "1LFD", "1AVX", "1M10", "1FSK", "1T6B",
  # "1K4C", "1BVK", "2A5T", "2PCC", "BOYV", "7CEI", "2I9B"]
    datasets = [DBD5(list(set(training + testing + validation)), check_features=False)]
    the_lambda = 0.0000005
    print "lamda = {}".format(the_lambda)
    classifiers = [RFPPOnlineEnsemble(the_lambda=the_lambda, n_epochs=50, n_estimators=10)]
    # classifiers = [RFPPOnlineEnsemble(the_lambda=the_lambda, n_epochs=200, n_estimators=1)]
    print("Patch size : ".format(default_fixed_neighbours_number))
    feature_sets = [
        [
            # Features.AA_ONE_HOT,
            Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
            Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
            # Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
            Features.RESIDUE_DEPTH,
            Features.HALF_SPHERE_EXPOSURE,
            Features.PROTRUSION_INDEX,
            # Features.D1_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D2_PLAIN_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_SHAPE_DISTRIBUTION,
            # Features.D1_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D2_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
            # Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION,
            # # Features.D2_MULTI_LEVEL_SHAPE_DISTRIBUTION,
        ],
    ]
    configurations = [datasets, classifiers, feature_sets]
    e = Experiment(configurations)
    # e.run("DBD4-DBD5-RFPP", EvaluationType.MODEL_SELECTION, data=(training, validation, testing), folds=3)
    neg_ratio = 20
    k= 10
    print("K = {}".format(k))
    print("Negative exampple ratio  = {}".format(neg_ratio))
    e.run("DBD4-DBD5-RFPP-Ensemble-10",
          EvaluationType.TRAIN_TEST,
          data=(training, validation, testing),
          training_ratio=neg_ratio,
          k=k)

def main():
    train_on_dbd4_test_on_dbd5()


if __name__ == "__main__":
    main()
    # np.random.seed(1000)
