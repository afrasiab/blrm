import copy
import glob
import numpy as np
import pickle
import warnings
from Bio.PDB import PDBParser, Selection, PPBuilder, CaPPBuilder, DSSP
from Bio.PDB.PDBExceptions import PDBException, PDBConstructionWarning
from os.path import splitext, basename
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from scipy.spatial.distance import cdist
from sklearn.cluster.hierarchical import AgglomerativeClustering
from sklearn.cluster.spectral import SpectralClustering
from blrm.configuration import database_pdb_directory, interaction_thr, brief_ss_kernel_keys, \
    brief_ss_kernel, detailed_ss_kernel_keys, categories_pickle_filename
from blrm.data.common.pdb import PDB
from blrm.data.dbd.examples_extractor import ExampleExtractor
from blrm.data.dbd.feature_extractors.secondary_srtucture_extractor import DSSPSecondaryStructureExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor
from blrm.model.enums import ClusteringAlg
from blrm.patch.clustering.kernel_kmeans import KernelKMeans


__author__ = 'basir shariat (basir@rams.colostate.edu)'


def read_pdb_file(pdb):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        name = splitext(pdb)[0]
        structure = PDBParser().get_structure(name, pdb)
        atoms = Selection.unfold_entities(structure, 'A')
        polypeptides = PPBuilder().build_peptides(structure)
        if len(polypeptides) == 0:
            polypeptides = CaPPBuilder().build_peptides(structure)
        residues = [residue for polypeptide in polypeptides for residue in polypeptide]
        return structure, residues


def give_secondary_structures(pdb):
    structure, residues = read_pdb_file(pdb)
    dssp = DSSP(structure[0], pdb)
    dssp_array = np.ndarray((len(residues), 6))
    ss = []
    for (i, res) in enumerate(residues):
        (_, _, cid, rid) = res.get_full_id()
        key = (cid, rid)
        if key in dssp:
            ss.append(dssp[key][1])
        else:
            ss.append('-')
    return ss, residues


w_dist = 0.7
w_ss = 0.1
w_seq = 0.2


def give_kernel_matrix1(pdb, full=False):
    ss, residues = give_secondary_structures(pdb)
    n = len(residues)
    d = np.zeros((n, n))
    s_d = 10
    s_ss = 1
    for i in range(0, n):
        for j in range(0, n):
            a_i = np.array([atom.get_coord() for atom in residues[i].child_list])
            a_j = np.array([atom.get_coord() for atom in residues[j].child_list])

            i_c = np.mean(a_i, axis=0)
            j_c = np.mean(a_j, axis=0)
            k_dist = np.exp(-np.linalg.norm(i_c - j_c) / s_d)

            i_ss = brief_ss_kernel_keys[ss[i]]
            j_ss = brief_ss_kernel_keys[ss[j]]
            k_ss = np.exp(-np.linalg.norm(brief_ss_kernel[i_ss][j_ss]) / s_ss)

            k_seq = np.exp(-np.linalg.norm(sum(np.abs(i - j) <= 1)) / s_ss)
            d[i, j] = w_dist * k_dist + w_ss * k_ss + w_seq * k_seq
    return residues, d, ss


def give_kernel_matrix(pdb):
    protein_name = basename(pdb).replace(".pdb", "")
    protein = Protein(*PDB.read_pdb_file(pdb))
    DSSPSecondaryStructureExtractor(protein).extract()
    ss = DSSPSecondaryStructureExtractor(None).load(protein_name)[0]
    residues = protein.residues
    n = len(residues)
    d = np.zeros((n, n))
    k_keys = brief_ss_kernel_keys
    s_d = 10
    for i in range(0, n):
        for j in range(0, n):
            k_dist = np.exp(-np.linalg.norm(residues[i].center - residues[j].center) * s_d)
            k_ss = brief_ss_kernel[k_keys[ss[i]], k_keys[ss[j]]] if i != j else 1.0
            k_seq = int(np.abs(i - j) <= 1)
            d[i, j] = w_dist * k_dist + w_ss * k_ss + w_seq * k_seq
    return residues, d, ss


def create_patch_file(algorithm=ClusteringAlg.KK, complexes=None):
    patches = {}
    if complexes is None:
        proteins = [name for name in glob.glob(database_pdb_directory + "*_l_b.pdb")]
        proteins.extend([name for name in glob.glob(database_pdb_directory + "*_r_b.pdb")])
        proteins.extend([name for name in glob.glob(database_pdb_directory + "*_l_u.pdb")])
        proteins.extend([name for name in glob.glob(database_pdb_directory + "*_r_u.pdb")])
    else:
        proteins = []
        for the_complex in complexes:
            proteins.append(database_pdb_directory + the_complex + "_l_b.pdb")
            proteins.append(database_pdb_directory + the_complex + "_r_b.pdb")
            proteins.append(database_pdb_directory + the_complex + "_l_u.pdb")
            proteins.append(database_pdb_directory + the_complex + "_r_u.pdb")
    seed = 10
    failed = 0
    alg_name = 'KK'
    for counter, pdb in enumerate(proteins):
        print "processing {0} {1}/{2}".format(basename(pdb), counter + 1, len(proteins))
        try:
            residues, kernel_matrix, _ = give_kernel_matrix(pdb)
            n = len(residues)
            num_clusters = n / 30
            empty_found = False
            partition = []
            if algorithm == ClusteringAlg.KK:
                alg_name = 'KK'
                while True:
                    if empty_found:
                        num_clusters -= 1
                    try:
                        km = KernelKMeans(n_clusters=num_clusters, max_iter=100, random_state=100)
                        partition = km.fit_predict(np.zeros(n), k=kernel_matrix)
                        partition = km.predict(np.zeros(n), k=kernel_matrix)
                        print "Number of cluster {0}".format(num_clusters)
                        break
                    except ValueError:
                        empty_found = True
            elif algorithm == ClusteringAlg.SC:
                alg_name = 'SC'
                sc = SpectralClustering(n_clusters=num_clusters, random_state=seed, affinity='precomputed')
                partition = sc.fit_predict(kernel_matrix)
            elif algorithm == ClusteringAlg.AG:
                alg_name = 'AG'
                ag = AgglomerativeClustering(n_clusters=num_clusters, affinity='precomputed', linkage='complete')
                partition = ag.fit_predict(kernel_matrix)
            colors = {}
            for ii, residue in enumerate(residues):
                colors[(residue.residue.parent.id, residue.residue.id[1])] = partition[ii]
            patches[basename(pdb).replace(".pdb", "")] = copy.copy(colors)
        except PDBException:
            print ">>>Couldn't read the pdb file {0}!".format(pdb)
            failed += 1
            continue
    if complexes is None:
        filename = "/home/basir/patches-{1}-{0}-{2}-{3}-{4}.pickle".format(seed, alg_name, w_dist, w_ss,
                                                                           w_seq)
    else:
        filename = "/home/basir/patches-{1}-{0}-{2}-{3}-{4}-{5}.pickle".format(seed, alg_name, w_dist, w_ss,
                                                                               w_seq, complexes)
    pickle.dump(patches, open(filename, "w"))
    if failed > 0:
        print "Failed to read {0} files!".format(failed)


def create_secondary_structure_file():
    patches = {}
    proteins = [name for name in glob.glob(database_pdb_directory + "*_l_b.pdb")]
    proteins.extend([name for name in glob.glob(database_pdb_directory + "*_r_b.pdb")])
    for counter, pdb in enumerate(proteins):
        print "processing {0} {1}/{2}".format(basename(pdb), counter, len(proteins))
        try:
            residues, kernel_matrix, ss = give_kernel_matrix(pdb)
            index = 0
            colors = {}
            for ii, residue in enumerate(residues):
                colors[(residue.parent.id, residue.id[1])] = detailed_ss_kernel_keys[ss[ii]]
                index += 1
            patches[basename(pdb).replace(".pdb", "")] = copy.copy(colors)
        except PDBException:
            continue
    pickle.dump(patches, open("/home/basir/secondary_structure.pickle", "w"))


def create_interaction_interface_file():
    patches = {}
    complexes = [basename(name).replace("_l_b.pdb", "") for name in glob.glob(database_pdb_directory + "*_l_b.pdb")]
    for counter, the_complex in enumerate(complexes):
        print "Processing complex {0}  {1}/{2}...".format(the_complex, counter + 1, len(complexes))
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", PDBConstructionWarning)
            ligand_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_b.pdb"))
            receptor_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_b.pdb"))
            ligand_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_u.pdb"))
            receptor_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_u.pdb"))
            bound_formation = ProteinPair(ligand_bound, receptor_bound)
            unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
            complex_object_model = ProteinComplex(the_complex, unbound_formation, bound_formation)
            bound_ligand_bio_residues = complex_object_model.bound_formation.ligand.biopython_residues
            bound_receptor_bio_residues = complex_object_model.bound_formation.receptor.biopython_residues
            bound_ligand_residues = complex_object_model.bound_formation.ligand.residues
            bound_receptor_residues = complex_object_model.bound_formation.receptor.residues
            pos = []
            neg = []
            l_colors = {}
            r_colors = {}
            for res in bound_ligand_bio_residues:
                l_colors[(res.parent.id, res.id[1])] = 0
            for res in bound_receptor_bio_residues:
                r_colors[(res.parent.id, res.id[1])] = 0

            for i in range(len(bound_ligand_bio_residues)):
                for j in range(len(bound_receptor_bio_residues)):
                    bound_ligand_residue = bound_ligand_bio_residues[i]
                    bound_receptor_residue = bound_receptor_bio_residues[j]
                    l_atoms = [atom.get_coord() for atom in bound_ligand_residue.get_list()]
                    r_atoms = [atom.get_coord() for atom in bound_receptor_residue.get_list()]
                    dist_mat = cdist(l_atoms, r_atoms)
                    ligand_b2u = complex_object_model.ligand_bound_to_unbound
                    receptor_b2u = complex_object_model.receptor_bound_to_unbound
                    # if the residues have an unbound counterpart
                    # this is due to the fact that the unbound and bound formations may have slightly different residues
                    if i in ligand_b2u and j in receptor_b2u:
                        if dist_mat.min() < interaction_thr:
                            l_colors[(bound_ligand_residue.parent.id, bound_ligand_residue.id[1])] = 1
                            r_colors[(bound_receptor_residue.parent.id, bound_receptor_residue.id[1])] = 1

                patches[the_complex + "_l_b"] = copy.copy(l_colors)
                patches[the_complex + "_r_b"] = copy.copy(r_colors)

    pickle.dump(patches, open("/home/basir/interaction_interface.pickle", "w"))


def main():
    global w_dist
    global w_ss
    global w_seq
    w_dist = 0.7
    w_ss = 0.1
    w_seq = 0.2
    create_patch_file(algorithm=ClusteringAlg.AG, complexes=['1A2K', '1ACB'])
    create_patch_file(algorithm=ClusteringAlg.SC, complexes=['1A2K', '1ACB'])
    create_patch_file(algorithm=ClusteringAlg.KK, complexes=['1A2K', '1ACB'])


# create_interaction_interface_file()
def create_interface_file():
    patches = {}
    # complexes = [basename(name).replace("_l_b.pdb", "") for name in glob.glob(database_pdb_directory + "*_l_b.pdb")]
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    complexes = list(categories['DBD5'])
    for counter, the_complex in enumerate(complexes):
        print "Processing complex {0}  {1}/{2}...".format(the_complex, counter + 1, len(complexes))
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", PDBConstructionWarning)
            ligand_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_b.pdb"))
            receptor_bound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_b.pdb"))
            ligand_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_l_u.pdb"))
            receptor_unbound = Protein(*PDB.read_pdb_file(database_pdb_directory + the_complex + "_r_u.pdb"))
            protein_names = [(the_complex + "_l_b", ligand_bound),
                             (the_complex + "_r_b", receptor_bound),
                             (the_complex + "_l_u", ligand_unbound),
                             (the_complex + "_r_u", receptor_unbound)]
            for protein_name, protein in protein_names:
                colors = {}
                interface = TrueInterfaceExtractor(None).load(protein_name)
                for i in range(interface.shape[0]):
                    if interface[i] > 0:
                        r = protein.residues[i].residue
                        colors[(r.parent.id, r.id[1])] = 1
                patches[protein_name] = copy.copy(colors)
    pickle.dump(patches, open("/home/basir/interaction_interface.pickle", "w"))
#
# p = Protein(*PDB.read_pdb_file(database_pdb_directory + "3G6D_l_b.pdb"))
# ExampleExtractor(p).extract()
# TrueInterfaceExtractor.load(p.name)
create_interface_file()
# create_secondary_structure_file()
