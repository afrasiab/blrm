from abc import ABCMeta, abstractmethod

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractEstimator(object):
    __metaclass__ = ABCMeta

    def __init__(self, **kwargs):
        pass

    @abstractmethod
    def predict(self, object):
        pass

    @abstractmethod
    def train(self, objects):
        pass

