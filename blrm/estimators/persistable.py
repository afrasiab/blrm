

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class Persistable(object):
    __metaclass__ = ABCMeta
    _file_address = None

    def __init__(self, file_address=None, **kwargs):
        self._file_address = file_address

    @abstractmethod
    def persist(self):
        pass

    @abstractmethod
    def load(self):
        pass
