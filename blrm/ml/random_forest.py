from PyML.utils.misc import ProgressBar
from sklearn.metrics import roc_auc_score

from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin
from blrm.utils.pretty_print import Print
__author__ = 'basir shariat (basir@rams.colostate.edu)'
import multiprocessing as mp
import numpy as np
from sklearn.ensemble import RandomForestClassifier


class RandomForest(LargeMarginRFPPMixin):
    def get_model_params(self):
        pass

    def __init__(self, number_of_trees=1050):
        print("number of trees: {}".format(number_of_trees))
        self.n = number_of_trees
        n = max(1, mp.cpu_count() - 1)
        self.forest = RandomForestClassifier(n_estimators=number_of_trees,
                                             n_jobs=n,
                                             verbose=0,
                                             class_weight="balanced_subsample",
                                             max_depth=20,
                                             min_samples_leaf=400,
                                             max_features=3,
                                             # verbose=2
                                             # max_leaf_nodes=10000
                                             )

    def train(self, training, validation=None):
        Print.print_info("Training Random forest started")

        xx = {}
        total = 0
        d = 0
        complexes = training.keys()
        for ii, bag in enumerate(complexes):
            l, r, e = training[bag]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            xx[bag] = x, y
            total += x.shape[0]
            d = x.shape[1]
        x = np.zeros((total, d))
        y = np.zeros((total, ))
        index = 0
        weights = []
        for bag in xx:
            xxx, yyy = xx[bag]
            x[index:(index+xxx.shape[0])] = xxx
            y[index:(index+xxx.shape[0])] = yyy
            index += xxx.shape[0]
            # weights.extend([1 if tt==1 else 1./200  for tt in yyy ]), sample_weight=np.array(weights)
        self.forest.fit(x, y)
        yp = self.predict(x)
        a = roc_auc_score(y, yp)
        print ("AUC of forest after training: {}".format(a))

        print("Staring the training...")

    def train1(self, training, validation=None):
        Print.print_info("Training Random forest started")

        xx = {}
        import gc
        n = max(1, mp.cpu_count() - 1)
        # prog = ProgressBar("Preparing pairs of examples...")
        complexes = training.keys()
        for ii, bag in enumerate(complexes):
            l, r, e = training[bag]
            # prog.progress((ii + 1.) / len(training))
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            self.forests.append(RandomForestClassifier(n_estimators=self.n,
                                             n_jobs=n,
                                             verbose=0,
                                             class_weight="balanced_subsample",
                                             max_depth=4,
                                             min_samples_leaf=800,
                                             max_features=10

                                             ))
            self.forests[-1].fit(x, y)

            l, r, e = validation[complexes[ii]]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            yp = self.forests[-1].predict(x)

            a = roc_auc_score(y, yp)
            print ("AUC of forest {} after training: {}".format(ii, a))
            pass
        print("Staring the training...")

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                scores[bag] = self.forest.predict(x)
        else:
            mid = x.shape[1] / 2
            x1 = x[:, :mid]
            x2 = x[:, mid:]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            scores = self.forest.predict(x)
        return scores
    
    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass

    def set_verbosity(self, level):
        self.forest.verbose = level

    def get_model_params(self):
        pass