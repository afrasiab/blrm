import itertools

import pickle
from random import shuffle, seed
import numpy as np
from PyML.classifiers import SVM
from PyML.evaluators.assess import cvFromFolds

from blrm.configuration import categories_pickle_filename, dbd4_nested_cv_pyml_results, \
    dbd4_nested_cv_trained_models
from blrm.data.dbd.dbd5 import DBD5
from blrm.model.enums import Features


def get_folds(complexes_dict):
    n_folds = max(complexes_dict.values()) + 1
    folds_list = [[] for i in range(n_folds)]
    for a_complex in complexes_dict:
        folds_list[complexes_dict[a_complex]].append(a_complex)
    return folds_list


def get_rfpps(self, bags):
    rfpps = {}
    for i, bag in enumerate(bags.keys()):
        x, y, _ = bags[bag]
        scores = x.dot(self.w).flatten()
        rfpps[bag] = np.min(np.where(y[np.argsort(scores)] == 1)[0]) + 1
    return rfpps


def structured_nested_cv(dataset, features, complexes, n_folds=5):
    c = [1, 10, 100]
    gamma = [0.4, 0.8, 1.6]
    parameters = list(itertools.product(*[c, gamma]))
    folds_complexes = get_folds(dataset.get_complex_folds_dict(complexes, n_folds)[0])

    for fold in range(n_folds):
        training_complexes = []
        testing_complexes = []
        for i in range(n_folds):
            if i == fold:
                testing_complexes.extend(folds_complexes[i])
            else:
                training_complexes.extend(folds_complexes[i])
        best_param = None
        best_rfpp = np.Inf

        for parameter in parameters:
            s = SVM(C=parameter[0])
            pyml_dataset, training, testing = dataset.get_pyml_cv_from_folds(features, training_complexes, n_folds,
                                                                             gamma=parameter[1])
            result = cvFromFolds(s, pyml_dataset, training, testing)
            result.save("{}fold:{}-c:{}-gamma:{}".format(dbd4_nested_cv_pyml_results, fold, parameter[0], parameter[1]))
            rfpp = []
            for r in result:
                rfpp.append(np.min(np.where(np.array(r.Y)[np.argsort(r.decisionFunc)] == 1)[0]) + 1)
            folds_median_rfpp = np.median(rfpp)
            if folds_median_rfpp < best_rfpp:
                best_rfpp = folds_median_rfpp
                best_param = parameter
        best_model = SVM(C=best_param[0])
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>."
        print "best median rfpp for c={} and  gamma={}: {}".format(best_param[0], best_param[1], best_rfpp)
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>."
        train, test = dataset.get_pyml_train_test(features, training_complexes, testing_complexes,
                                                  gamma=best_param[1])
        best_model.train(train)
        best_result = best_model.test(test)
        best_model.save("{}best-model-fold:{}-c:{}-gamma:{}".format(dbd4_nested_cv_trained_models, fold, best_param[0],
                                                                    best_param[1]))
        best_result.save("{}best-model-fold:{}-c:{}-gamma:{}".format(dbd4_nested_cv_pyml_results, fold, best_param[0],
                                                                     best_param[1]))
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>."
        print np.min(np.where(np.array(best_result.Y)[np.argsort(best_result.decisionFunc)] == 1)[0]) + 1
        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>."


def nested_cv_on_dbd4():
    categories = pickle.load(open(categories_pickle_filename, 'rb'))
    l = list(categories['DBD4'])
    # seed(15)
    shuffle(l)
    dbd4_complexes = l
    dbd4 = DBD5(dbd4_complexes)
    folds = 5
    feature_set = [

        Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX,
        Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX,
        Features.RELATIVE_ACCESSIBLE_SURFACE_AREA,
        Features.RESIDUE_DEPTH,
        Features.HALF_SPHERE_EXPOSURE,
        Features.PROTRUSION_INDEX,
        Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA,

        # Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT2,
        # Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT2,
        # Features.SURFACE_ATOMS_CURVATURE_COMBINED2,
        # Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT,
        # Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT,
        # Features.SURFACE_ATOMS_CURVATURE_COMBINED,

        # Features.D1_PLAIN_SHAPE_DISTRIBUTION,
        # Features.D2_PLAIN_SHAPE_DISTRIBUTION,
        # Features.D1_SURFACE_SHAPE_DISTRIBUTION,
        # #
        # Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION,
        # Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION,
        Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION,
        # # Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION
        # Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION

    ]

    structured_nested_cv(dbd4, feature_set, dbd4_complexes)


if __name__ == '__main__':
    nested_cv_on_dbd4()
