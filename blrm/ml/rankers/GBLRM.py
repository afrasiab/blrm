from PyML.utils.misc import ProgressBar

from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin
import numpy as np
from sklearn.utils.extmath import cartesian

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class GBLRM(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=150, the_lambda=0.1):
        super(GBLRM, self).__init__(n_epochs, the_lambda)

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = 0.5 * self.the_lambda * self.w.T.dot(self.w)
        constraints_cost = 0
        for bag in bags:
            l, r, e, _, _ = bags[bag]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            # x = np.hstack((x1, x2))
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            scores = np.dot(x, self.w)
            p = np.where(y == +1)[0]
            n = np.where(y == -1)[0]
            constraints_cost += np.max((0, 1 - scores[p].max() - scores[n].max())) / float(len(bags))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def train(self, training, validation=None, k=1):
        d = training[training.keys()[0]][0].shape[1]
        n = float(len(training))
        # self.w = np.random.rand(d*2,)
        self.w = np.zeros((d * 2,))
        indices = range(len(training))
        bag_ids = training.keys()
        w_pocket = None
        best = np.Inf
        # expanding bags
        xx = {}
        p = ProgressBar("\r Creating Bags...\r")
        for ii, bag in enumerate(training):
            l, r, e, n_l, n_r = training[bag]
            e_dict = {}
            for i, example in enumerate(e):
                e_dict[(example[0], example[1])] = i
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            nn = []
            for example in e:
                neighbours = []
                for index in cartesian([n_l[example[0]], n_r[example[1]]]):
                    if tuple(index) in e_dict:
                        neighbours.append(e_dict[tuple(index)])
                nn.append(neighbours)
            xx[bag] = x, y, nn
            p.progress((ii + 1.) / len(training))
        for t in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            # p = ProgressBar("".format(t))
            gp = self.the_lambda * self.w  # / float(len(self.w))
            eta_t = 1. / (self.the_lambda * t)
            for iii, bag in enumerate(bags_shuffle):
                x, y, nn = xx[bag]
                scores = np.dot(x, self.w)
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                neg_ind = np.where(y == -1)[0]
                pos_ind = np.where(y == +1)[0]
                scores_p[neg_ind] = -np.inf
                scores_n[pos_ind] = -np.inf
                k_p = np.minimum(len(pos_ind), len(neg_ind))
                k = np.minimum(k, k_p) if k != -1 else k_p
                for ii in range(k):
                    if scores_p.max() < scores_n.max() + 1:
                        gp -= (x[scores_p.argmax()] - x[scores_n.argmax()]) / (n * (k+ii))
                        scores_p[scores_p.argmax()] = -np.Inf
                        scores_n[scores_n.argmax()] = -np.Inf
                # neighbour scores
                for ii, nn_i in enumerate(nn):
                    for nnn_j in nn_i:
                        gp += 0.005 * ((x[ii] - x[nnn_j]) * (scores[ii] - scores[nnn_j])) / (float(len(nn_i)) * float(len(nn)))

            self.w = self.w - eta_t * gp  # w - w/t -1[w x_p < w x_n] = w(1-1/t)
            self.w = min(1., 1. / (np.linalg.norm(self.w)) * np.sqrt(self.the_lambda)) * self.w
            # self.w += w0
            if validation is not None:
                rfpps, aucs, _ = self.get_rfpps(validation)
                cost = self._get_current_cost(validation)
            else:
                rfpps, aucs, _ = self.get_rfpps(training)
                cost = self._get_current_cost(training)

            if np.max(rfpps.values()) < best:
                w_pocket = np.copy(self.w)
                best = np.max(rfpps.values())
            print("Epoch= {}/{}\t\t"
                  "Cost= {:0.5f}\t{:0.5f}\t{:0.5f}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  "|w|={:5.5f}\t\t"
                  "Mean AUC={:5.5f}"
                  .format(t, self.n_epochs,
                          cost[0], cost[1], cost[2],
                          int(np.percentile(rfpps.values(), 50)),
                          int(np.percentile(rfpps.values(), 80)),
                          int(np.percentile(rfpps.values(), 100)),
                          np.linalg.norm(self.w),
                          np.mean(aucs.values())
                          )
                  )
        # if validation is not None:
        print("Saving the pocket weight vector")
        self.w = np.copy(w_pocket)

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                l, r, e, _, _ = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                # x = np.hstack((x1, x2))
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                scores[bag] = np.dot(x, self.w)
        else:
            scores = np.dot(x, self.w)
        return scores
