from blrm.configuration import verbose
from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin
import numpy as np
import multiprocessing as mp
from sklearn.metrics import roc_auc_score

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def _parallel_RFPP(params):
    w, bag = params
    x, y, _ = bag
    scores = x.dot(w)
    auc = roc_auc_score(y, scores)
    rfpp = np.min(np.where(y[np.argsort(-scores)] == 1)[0]) + 1
    return rfpp, auc


class OnlineBaggingTopK(LargeMarginRFPPMixin):
    """An SSGD large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=500, the_lambda=0.00001, k=1, subsample_ratio=-1):
        super(OnlineBaggingTopK, self).__init__(n_epochs, the_lambda)
        self.k = k

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        constraints_cost = 0
        for bag in bags:
            x_i, y_i, _ = bags[bag]
            scores = np.dot(x_i, self.w)
            scores_p, scores_n = np.copy(scores), np.copy(scores)
            scores_p[np.where(y_i == -1)[0]] = -np.inf
            scores_n[np.where(y_i == 1)[0]] = -np.inf
            constraints_cost += (1. / len(bags)) * np.max(
                (0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        # d is the dimension of the data / number elements in each object's vector rep
        d = bags[bags.keys()[0]][0].shape[1]
        self.w = np.zeros((d,))
        # for bag in bags:
        #     x, y, _=bags[bag]
        #
        return bags

    def train(self, data):
        bags = self.__check_input_and_initialize(data)
        indices = range(len(bags))
        bag_ids = bags.keys()
        w_pocket = None
        median_rfpp = np.inf
        for e in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            eta = 1.0 / (self.the_lambda * (e**2 + 1))
            decay = (1 - self.the_lambda * eta)
            for bag in bags_shuffle:
                self.w *= decay
                x, y, _ = bags[bag]
                scores = np.dot(x, self.w)
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf
                for i in range(self.k):
                    if scores_p.max() < scores_n.max() + 1:
                        self.w += eta * (x[scores_p.argmax()] - x[scores_n.argmax()])
                    scores_p[scores_p.argmax()] = -np.inf

            rfpps, aucs = self.get_rfpps(bags)
            if np.sum(rfpps.values()) < median_rfpp:
                w_pocket = np.copy(self.w)
                median_rfpp = np.sum(rfpps.values())
            if verbose:
                cost = self._get_current_cost(bags)
                print("{}/{}\t|"
                      "\tCost: {:5.5f}\t{:5.5f}\t{:5.5f}\t|"
                      "\tMean AUC: {:3.3f}\t|"
                       "\t|w|: {:3.3f}".
                      format(e, self.n_epochs,
                             cost[0], cost[1], cost[2],
                             np.mean(aucs.values()),
                             np.linalg.norm(self.w)))
                for pct in range(10,110, 10):
                    print("\t|{}|{: <5}|".format(pct, int(np.percentile(rfpps.values(), pct)),
))
        self.w = w_pocket

    def predict(self, x):
        return np.dot(x, self.w)

    def get_rfpps_p(self, bags):
        keys = bags.keys()
        keys.sort()
        ws = [self.w for i in range(len(keys))]
        xs = [(bags[key]) for key in keys]
        pool = mp.Pool(mp.cpu_count())
        results = list(pool.map(_parallel_RFPP, zip(ws, xs)))
        pool.close()
        pool.join()
        rfpps = {}
        aucs = {}
        for i, key in enumerate(keys):
            rfpps[key] = results[i][0]
            aucs[key] = results[i][1]
        return rfpps, aucs
