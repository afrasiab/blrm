from blrm.configuration import verbose
from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin
import numpy as np
import multiprocessing as mp
from sklearn.metrics import roc_auc_score

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def _parallel_RFPP(params):
    w, bag = params
    x, y, _ = bag
    scores = x.dot(w)
    auc = roc_auc_score(y, scores)
    rfpp = np.min(np.where(y[np.argsort(-scores)] == 1)[0]) + 1
    return rfpp, auc


class OnlineLinearTopK(LargeMarginRFPPMixin):
    """An SSGD large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=250, the_lambda=0.01, kappa=1, k=10):
        super(OnlineLinearTopK, self).__init__(n_epochs, the_lambda)
        self.k = k
        self.kappa = kappa

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        constraints_cost = 0
        for bag in bags:
            x_i, y_i, _ = bags[bag]
            scores = np.dot(x_i, self.w)
            scores_p, scores_n = np.copy(scores), np.copy(scores)
            scores_p[np.where(y_i == -1)[0]] = -np.inf
            scores_n[np.where(y_i == 1)[0]] = -np.inf
            constraints_cost += np.max((0, 1 - (scores_p.max()-scores_n.max())))/ len(bags)
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        # d is the dimension of the data / number elements in each object's vector rep
        d = bags[bags.keys()[0]][0].shape[1]
        # self.w = np.random.rand(d,)
        self.w = np.zeros((d,))
        return bags

    def train(self, data):
        bags = self.__check_input_and_initialize(data)
        n = float(len(bags))
        for e in range(1, self.n_epochs + 1):
            all_scores = self.predict(bags)
            if verbose:
                rfpps, aucs = self.get_rfpps(bags, bags_scores=all_scores)
                cost = self._get_current_cost(bags)
                p = self._get_rfpp_percentiles(rfpps)
                nw = np.sqrt(self.w.T.dot(self.w))/float(np.size(self.w))
                print("{}/{}\t|\tCost: {:3.3f}\t{:3.3f}\t{:3.3f}\t|\tMean AUC: {:3.3f}\t|\t|w|: {:3.3f}\t|\t{}".
                      format(e, self.n_epochs, cost[0], cost[1], cost[2], np.mean(aucs.values()), nw, p))

            eta = 1. / (self.the_lambda * (e+1)**2)#
            for bag in bags:
                gw = self.the_lambda * self.w
                x, y, _ = bags[bag]
                scores = self.predict(x)
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf
                for i in range(self.k):
                    if scores_p.max() < scores_n.max() + 1:
                        gw -= self.kappa * (x[scores_p.argmax()] - x[scores_n.argmax()])
                    scores_p[scores_p.argmax()] = -np.inf
                self.w -= (eta) * gw
        # self.w = self.w/np.linalg.norm(self.w)

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                x, _, _ = bags[bag]
                scores[bag] = np.dot(x, self.w)
        else:
            scores = np.dot(x, self.w)
        return scores


    def get_rfpps_p(self, bags):
        keys = bags.keys()
        keys.sort()
        ws = [self.w for i in range(len(keys))]
        xs = [(bags[key]) for key in keys]
        pool = mp.Pool(mp.cpu_count())
        results = list(pool.map(_parallel_RFPP, zip(ws, xs)))
        pool.close()
        pool.join()
        rfpps = {}
        aucs = {}
        for i, key in enumerate(keys):
            rfpps[key] = results[i][0]
            aucs[key] = results[i][1]
        return rfpps, aucs
