from __future__ import print_function

from bisect import bisect
from random import random

import numpy as np
from PyML.utils.misc import ProgressBar
from abc import abstractmethod, ABCMeta
from sklearn.metrics import roc_auc_score, precision_recall_curve, average_precision_score
from sklearn.preprocessing import normalize

from numba import jit

class LargeMarginRFPPMixin(object):
    __metaclass__ = ABCMeta

    def __init__(self, n_epochs=1000, the_lambda=0.1, id=None):
        self.n_epochs = int(n_epochs)
        self.the_lambda = float(the_lambda)
        self.w = None
        self.id = id

    @abstractmethod
    def get_model_params(self):
        pass

    def get_rfpps(self, bags, bags_scores=None, k=None):
        rfpps = {}
        rkpps = {}
        rfnps = {}
        aucs = {}
        auprcs = {}
        prs = {}
        all_scores = self.predict(bags) if bags_scores is None else bags_scores
        for i, bag in enumerate(bags.keys()):
            y = bags[bag][1] if len(bags[bag]) == 4 else bags[bag][2][:, 2]
            scores = all_scores[bag]
            aucs[bag] = roc_auc_score(y, scores)
            rfpps[bag] = np.min(np.where(y[np.argsort(scores)[::-1]] == 1)[0]) + 1
            rfnps[bag] = np.min(np.where(y[np.argsort(scores)[::-1]] == -1)[0]) + 1
            # p, r, _ = precision_recall_curve(y, scores)
            # prs[bags] = (p, r)
            auprcs[bag] = average_precision_score(y, scores)
        return rfpps, rfnps, aucs, all_scores, auprcs

    @abstractmethod
    def train(self, data, validation=None):
        pass

    @abstractmethod
    def _get_cost_gradient(self, bags, k=-1):
        pass

    @abstractmethod
    def _get_current_cost(self, bags):
        pass

    @staticmethod
    def _get_rfpp_percentiles(rfpps):
        rfpp_percentiles = "["
        for pct in range(10, 110, 10):
            if pct == 50:
                rfpp_percentiles += "|{:^3}|, ".format(int(np.percentile(rfpps.values(), pct)))
            elif pct == 100:
                rfpp_percentiles += "{:^3}]".format(int(np.percentile(rfpps.values(), pct)))
            else:
                rfpp_percentiles += "{:^3}, ".format(int(np.percentile(rfpps.values(), pct)))
        return rfpp_percentiles


class SSGDRFPPSVM(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=150, the_lambda=0.1):
        super(SSGDRFPPSVM, self).__init__(n_epochs, the_lambda)

    def _get_cost_gradient(self, bags, k=1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = 0.5 * self.the_lambda * self.w.T.dot(self.w)
        constraints_cost = 0
        for bag in bags:
            l, r, e = bags[bag]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            # x = np.hstack((x1, x2))
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            # x = normalize(np.hstack(((x1 + x2), np.multiply(x1, x2))), axis=1)
            # import sklearn
            #
            # x = sklearn.metrics.pairwise.pairwise_kernels(x1, x2, metric='rbf', n_jobs = 30)
            # n_e = x1.shape[0]
            # x = np.zeros((n_e, x1.shape[1] * x2.shape[1]))
            # for i in range(n_e):
            #     x[i, :] = np.outer(x1[i, :], x2[i, :]).flatten()
            scores = np.dot(x, self.w)
            # x = np.hstack((x1, x2))
            # scores = np.dot(x, self.w)
            p = np.where(y == +1)[0]
            n = np.where(y == -1)[0]
            constraints_cost += np.max((0, 1 - scores[p].max() - scores[n].max())) / float(len(bags))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost
    # @jit

    def get_model_params(self):
        return self.w

    def train(self, training, validation=None, k=1):
        d = training[training.keys()[0]][0].shape[1]
        n = float(len(training))
        self.w = np.zeros((d * 2))
        # self.w = np.random.rand(d * 2)
        indices = range(len(training))
        bag_ids = training.keys()
        w_pocket = None
        best = np.Inf
        # expanding bags
        xx = {}
        import gc
        prog = ProgressBar("Preparing pairs of examples...")
        for ii, bag in enumerate(training):
            l, r, e = training[bag]
            prog.progress((ii+1.)/len(training))
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            # n_e = x1.shape[0]
            # x = np.zeros((n_e, x1.shape[1] * x2.shape[1]))
            # for i in range(n_e):
            #     x[i, :] = np.outer(x1[i, :], x2[i, :]).flatten()
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            # x = normalize(np.hstack(((x1 + x2), np.multiply(x1, x2))), axis=1)
            # x = np.hstack((x1, x2))

            neg_ind = np.where(y == -1)[0]
            pos_ind = np.where(y == +1)[0]
            k_p = np.minimum(len(pos_ind), len(neg_ind))
            k = np.minimum(k, k_p) if k != -1 else k_p
            xx[bag] = x, y, pos_ind, neg_ind, k
            gc.collect()
        print("Staring the training...")

        for t in range(1, self.n_epochs + 1):
            # print("Epoch= {:02d}/{}\t\t".format(t, self.n_epochs))
            data = validation if validation is not None else training
            rfpps, rfnps, aucs, all_scores, auprcs = self.get_rfpps(data,k=k)
            # cost = self._get_current_cost(data)
            rfpps_values = rfpps.values()
            rfpps_values.sort()

            performance_measure = np.max(rfpps_values)
            if performance_measure <= best :
                w_pocket = np.copy(self.w)
                best = performance_measure
            print("Epoch {:02d} of {}\t\t"
                  # "Cost= {:0.4f}\t{:0.4f}\t{:0.4f}\t\t"
                  "RFPPs= {}\t{}\t{}\t\t"
                  "|w|={:5.5f}\t\t"
                  "Median AUC={:5.5f}\t"
                  "Mean AUC={:5.5f}\t"
                  "Mean AUPRC={:5.5f}"
                  .format(t, self.n_epochs,
                          # cost[0], cost[1], cost[2],
                          rfpps_values[int(len(rfpps_values)*0.5)],
                          rfpps_values[int(len(rfpps_values)*0.8)],
                          rfpps_values[-1],
                          np.linalg.norm(self.w),
                          np.median(aucs.values()),
                          np.mean(aucs.values()),
                        np.mean(auprcs.values()))
                  )
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]

            for iii, bag in enumerate(bags_shuffle):
                x, y, pos_ind, neg_ind, k = xx[bag]
                gp = self.the_lambda * self.w
                eta_t = 1. / (self.the_lambda * t * 2)
                scores = np.dot(x, self.w)
                scores_p = np.copy(scores)
                scores_p[neg_ind] = -np.inf
                scores_n = np.copy(scores)
                scores_n[pos_ind] = -np.inf
                for jj in range(len(pos_ind)):
                    pp = scores_p.argmax()
                    for ii in range(100):
                        if scores_p.max() < scores_n.max()+1:
                            nn = scores_n.argmax()
                            gp -= (x[pp] - x[nn]) / float(jj+1)
                            scores_n[nn] = -np.Inf
                    scores_p[pp] = -np.Inf
                self.w = self.w - eta_t * (gp / float(n))
        print("Saving the pocket weight vector...best AUC: {}".format(best))
        self.w = np.copy(w_pocket)
        self.w = self.w/np.linalg.norm(self.w)

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
                # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
                # scores[bag] = np.min(np.hstack((s1, s2)), axis=1)
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                # x = np.hstack((x1, x2))

                # x = normalize(np.hstack(((x1 + x2), np.multiply(x1, x2))), axis=1)

                # x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                # x = np.outer(x1, x2)
                # n_e = x1.shape[0]
                # x = np.zeros((n_e, x1.shape[1] * x2.shape[1]))
                # for i in range(n_e):
                #     x[i, :] = np.outer(x1[i, :], x2[i, :]).flatten()
                scores[bag] = np.dot(x, self.w)
        else:
            mid = x.shape[1] / 2
            x1 = x[:, :mid]
            x2 = x[:, mid:]
            # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
            # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
            # scores[bag] = np.dot(x, self.w)
            # scores = np.min(np.hstack((s1, s2)), axis=1)
            # n_e = x1.shape[0]
            # x = np.zeros((n_e, x1.shape[1] * x2.shape[1]))
            # for i in range(n_e):
            #     x[i, :] = np.outer(x1[i, :], x2[i, :]).flatten()
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            # x = np.hstack((x1, x2))

            # x = normalize(np.hstack(((x1 + x2), np.multiply(x1, x2))), axis=1)

            scores = np.dot(x, self.w)
        return scores



class KernelizedSSGDRFPPSVM(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=5, the_lambda=10):
        super(KernelizedSSGDRFPPSVM, self).__init__(n_epochs, the_lambda)
        self.alphas = {}
        self.support_vectors = None
        self.gamma = 0.5

    def _get_cost_gradient(self, bags, k=-1):
        pass

    # def _get_rfpps(self, bags):
    #     rfpps = {}
    #     for i, bag in enumerate(bags.keys()):
    #         x, y, _ = bags[bag]
    #         scores = self.get_scores(bags, bag).reshape(-1)
    #         rfpps[bag] = (np.where(y[np.argsort(-scores)] == 1)[0]).min() + 1
    #     return rfpps

    def _get_current_cost(self, bags):
        pass
        # regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        # constraints_cost = 0
        # for bag in bags:
        #     x_i, y_i, _ = bags[bag]
        #     if x_i.shape[0] == 0:
        #         continue
        #     scores = self.get_scores(bags, bag)
        #     scores_p, scores_n = np.copy(scores), np.copy(scores)
        #     scores_p[np.where(y_i == -1)[0]] = -np.inf
        #     scores_n[np.where(y_i == 1)[0]] = -np.inf
        #     constraints_cost += (1. / len(bags)) * np.max((0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        # return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        for bag in bags:
            l, r, e = bags[bag]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            # x = np.hstack((x1, x2))
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            pos_ind = np.where(y == +1)[0]
            neg_ind = np.where(y == -1)[0]
            bags[bag] = x, y, pos_ind, neg_ind
            self.alphas[bag] = np.zeros(y.shape)
        return bags

    def train(self, training, gamma=1.6, k=1):
        self.gamma = gamma
        bags = self.__check_input_and_initialize(training)
        indices = range(len(bags))
        bag_ids = bags.keys()
        for t in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            scores_dict = {}
            for bag in bags_shuffle:
                x, y, pos_ind, neg_ind = bags[bag]
                scores = self.get_scores(bags, bag)
                scores_dict[bag] = scores
                # scores_p, scores_n = np.copy(scores), np.copy(scores)
                # scores_p[neg_ind] = -np.inf
                # scores_n[np.where(y == 1)[0]] = -np.inf
                np.random.shuffle(neg_ind)
                for p in pos_ind:
                    for n in neg_ind[:15]:
                        if scores[p] < scores[n] + 1:
                            self.alphas[bag][n] -= 1
                            self.alphas[bag][p] += 1

                    # print(self.alphas[bag])
            self.save_support_vectors(bags)
            rfpps, aucs, _ = self.get_rfpps(bags, bags_scores=scores_dict)
            # cost = self._get_current_cost(bags)
            print("Epoch= {}/{}\t\t"
                  # "Cost= {:0.5f}\t{:0.5f}\t{:0.5f}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  # "|w|={:5.5f}\t\t"
                  "Mean AUC={}\t\t"
                  .format(t, self.n_epochs,
                          # cost[0], cost[1], cost[2],
                          int(np.percentile(rfpps.values(), 50)),
                          int(np.percentile(rfpps.values(), 80)),
                          int(np.percentile(rfpps.values(), 100)),
                          # np.linalg.norm(self.w)),
                        np.mean(aucs.values())
                          ))

        print(self.support_vectors)

    def save_support_vectors(self, bags):
        support_vectors = []
        alphas = []
        for bag in self.alphas:
            x, y, _, _ = bags[bag]
            non_zeros = np.nonzero(self.alphas[bag])[0]
            for index in non_zeros:
                support_vectors.append(x[index])
                alphas.append(self.alphas[bag][index])
        print("Number of support vectors : {}".format(len(support_vectors)))
        self.support_vectors = (support_vectors, np.array(alphas).reshape(-1, 1))

    def get_scores(self, bags, bag):
        # x, _, _ = bags[bag]
        x, y, pos_ind, neg_ind = bags[bag]
        scores = np.zeros((x.shape[0]))
        if self.support_vectors is not None:
            support_vectors, alphas = self.support_vectors
            for i, alpha in enumerate(alphas):
                diff = x - support_vectors[i]
                norms_sq = np.einsum('ij,ij->i', diff, diff)
                scores += (alpha * np.exp(-self.gamma * norms_sq))
        return scores

    def predict(self, x):
        if isinstance(x, dict):
            scores = {}
            bags = x
            for bag in bags:
                if len(bags[bag]) != 4:
                    l, r, e = bags[bag]
                    x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                    # x = np.hstack((x1, x2))
                    x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                    bags[bag] = x, y, None, None
                x, y, _, _ = bags[bag]
                s = np.zeros((x.shape[0],))
                if self.support_vectors is not None:
                    support_vectors, alphas = self.support_vectors
                    for i, alpha in enumerate(alphas):
                        diff = x - support_vectors[i]
                        norms_sq = np.einsum('ij,ij->i', diff, diff)
                        s += (alpha * np.exp(-self.gamma * norms_sq))
                scores[bag] = s
            return scores
        else:
            # mid = x.shape[1] / 2
            # x1 = x[:, :mid]
            # x2 = x[:, mid:]
            # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
            # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
            # scores[bag] = np.dot(x, self.w)
            # scores = np.min(np.hstack((s1, s2)), axis=1)
            # x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            s = np.zeros((x.shape[0],))
            if self.support_vectors is not None:
                support_vectors, alphas = self.support_vectors
                for i, alpha in enumerate(alphas):
                    diff = x - support_vectors[i]
                    norms_sq = np.einsum('ij,ij->i', diff, diff)
                    s += (alpha * np.exp(-self.gamma * norms_sq))
            return s



class PSSGDRFPPSVM(LargeMarginRFPPMixin):
    """A Probabilistic Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=50, the_lambda=0.001):
        super(PSSGDRFPPSVM, self).__init__(n_epochs, the_lambda)

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        constraints_cost = 0
        for bag in bags:
            x_i, y_i, _ = bags[bag]
            scores = np.dot(x_i, self.w)
            scores_p, scores_n = np.copy(scores), np.copy(scores)
            scores_p[np.where(y_i == -1)[0]] = -np.inf
            scores_n[np.where(y_i == 1)[0]] = -np.inf
            constraints_cost += (1. / len(bags)) * np.max(
                (0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        # d is the dimension of the data / number elements in each object's vector rep
        d = bags[bags.keys()[0]][0].shape[1]
        self.w = np.zeros((d,))
        return bags

    def get_index_with_score(self, scores):
        if not np.any(scores == 0):
            non_inf = np.where(scores != -np.Inf)[0]
            shifted_scores = scores[non_inf]
            shifted_scores += (-shifted_scores.min() + 1)
            probabilities = shifted_scores / np.sum(shifted_scores)
            cdf = [probabilities[0]]
            for i in xrange(1, len(probabilities)):
                cdf.append(cdf[-1] + probabilities[i])
            return non_inf[bisect(cdf, random())]
        else:
            return scores.argmax()

    def train(self, data):
        bags = self.__check_input_and_initialize(data)
        indices = range(len(bags))
        bag_ids = bags.keys()
        for t in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            gw = self.the_lambda * self.w

            for bag in bags_shuffle:
                x, y, _ = bags[bag]
                scores = np.dot(x, self.w)
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf
                p_ind = self.get_index_with_score(scores_p)
                n_ind = self.get_index_with_score(scores_n)
                if scores_p[p_ind] < scores_n[n_ind] + 1:
                    gw -= 1. / len(bags) * (x[p_ind] - x[n_ind])
            self.w -= (1. / np.log(t + 1)) * gw
            rfpps, aucs, _ = self.get_rfpps(bags)
            cost = self._get_current_cost(bags)
            print("Epoch= {}/{}\t\t"
                  "Cost= {:0.5f}\t{:0.5f}\t{:0.5f}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  "|w|={:5.5f}\t\t"
                  "AUC={:3.3f}".
                  format(t, self.n_epochs, cost[0], cost[1], cost[2],
                         int(np.percentile(rfpps.values(), 50)),
                         int(np.percentile(rfpps.values(), 80)),
                         int(np.percentile(rfpps.values(), 100)),
                         np.linalg.norm(self.w),
                         np.mean(aucs.values())))

    def predict(self, x):
        scores = np.dot(x, self.w)
        scores[scores < 0] = 0
        return scores


class SSGDRFPPSVMff(LargeMarginRFPPMixin):
    """An SSGD large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=50, the_lambda=0.001):
        super(SSGDRFPPSVM, self).__init__(n_epochs, the_lambda)

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        constraints_cost = 0
        for bag in bags:
            x_i, y_i, _ = bags[bag]
            scores = np.dot(x_i, self.w)
            scores_p, scores_n = np.copy(scores), np.copy(scores)
            scores_p[np.where(y_i == -1)[0]] = -np.inf
            scores_n[np.where(y_i == 1)[0]] = -np.inf
            constraints_cost += (1. / len(bags)) * np.max(
                (0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        # d is the dimension of the data / number elements in each object's vector rep
        d = bags[bags.keys()[0]][0].shape[1]
        self.w = np.zeros((d,))
        return bags

    def train(self, data):
        bags = self.__check_input_and_initialize(data)
        indices = range(len(bags))
        t = 1
        bag_ids = bags.keys()
        for e in range(self.n_epochs):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            eta = 1.0 / (self.the_lambda * (t))
            decay = (1 - 1. / np.sqrt(t))
            # decay = (1 - self.the_lambda * eta)
            self.w *= decay
            w0 = 0
            for bag in bags_shuffle:
                x, y, _ = bags[bag]
                scores = np.dot(x, self.w)
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf
                if scores_p.max() < scores_n.max() + 1:
                    w0 += eta * 1. / len(bags) * (x[scores_p.argmax()] - x[scores_n.argmax()])
            self.w += w0
            rfpps, _, _ = self.get_rfpps(bags)
            cost = self._get_current_cost(bags)
            t += 1
            print("Epoch= {}/{}\t\t"
                  "Cost= {:0.5f}\t{:0.5f}\t{:0.5f}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  "|w|={:5.5f}\t\t".
                  format(t, self.n_epochs, cost[0], cost[1], cost[2],
                         int(np.percentile(rfpps.values(), 50)),
                         int(np.percentile(rfpps.values(), 80)),
                         int(np.percentile(rfpps.values(), 100)),
                         np.linalg.norm(self.w)))
            # self.w = w_pocket

    def predict(self, x):
        return np.dot(x, self.w)


class sum_proxy_rfpp_svm(LargeMarginRFPPMixin):
    """A heuristic for large-margin rfpp optimizer
        This is a large-margin linear model that optimizes a proxy function for the RFPP
        model:
            .. math::
            s_{nx1} = x_{nxd}.w_{dx1}
            scores = x . w
            y_{nx1}

        cost function:
            .. math::
            \lambda / 2 w^2

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, the_lambda=0.1, n_epochs=1000):
        super(sum_proxy_rfpp_svm, self).__init__(n_epochs, the_lambda)
        self.the_lambda = the_lambda
        self.n_epochs = n_epochs
        self.w = None

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments:\n{}".format(sum_proxy_rfpp_svm.train.__doc__))
        # d is the dimension of the data / number elements in each object's vector rep
        d = bags[bags.keys()[0]][0].shape[1]
        self.w = np.zeros((d, 1))
        return bags

    def train(self, data):
        """
        :param data:
        :return:
        """
        bags = self.__check_input_and_initialize(data)
        epoch = 1.
        # not_converged = True
        rfpps = {}
        # p = ProgressBar("Training RFPP SVM (sum proxy) ")
        while epoch <= self.n_epochs:
            eta_t = 1. / np.sqrt(epoch)
            g_cost = self._get_cost_gradient(bags)
            self.w -= eta_t * g_cost
            rfpps, _, _ = self.get_rfpps(bags)
            cost = self._get_current_cost(bags)
            print("epoch: {}/{}\t"
                  "cost: {:0.5f}\t{:0.5f}\t{:0.5f}\t"
                  "median rfpp: {}\t\t"
                  "|w|={}".format(int(epoch), self.n_epochs, cost[0], cost[1], cost[2], np.median(rfpps.values()),
                                  np.linalg.norm(self.w)))
            epoch += 1
        print(rfpps)

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                x, _, _ = bags[bag]
                scores[bag] = np.dot(x, self.w)
        else:
            scores = np.dot(x, self.w)
        return scores

    def _get_cost_gradient(self, bags, k=10):
        bags_keys = bags.keys()
        indices = range(len(bags_keys))
        np.random.shuffle(indices)
        selected_indices = indices[:k] if k != -1 else indices
        selected_bags = [bags_keys[i] for i in selected_indices]
        g_cost = self.the_lambda * self.w
        for bag in selected_bags:
            x_i, y_i, total_n = bags[bag]
            n_i = float(total_n)
            if y_i.T.dot(x_i.dot(self.w))[0, 0] >= 0:
                g_cost += (1. / len(bags)) * x_i.T.dot(y_i)
        return g_cost

    def _get_current_cost(self, bags):
        cost_regularization_term = 0.5 * self.the_lambda * np.linalg.norm(self.w) ** 2
        cost_constraints_term = 0
        for i, bag in enumerate(bags.keys()):
            x_i, y_i, ids_i = bags[bag]
            n_i = float(x_i.shape[0])
            scores_sum = y_i.T.dot(-x_i.dot(self.w)).flatten()[0]
            cost_constraints_term += scores_sum / float(len(bags))
            # cost_constraints_term += max(0, 1 - (scores_sum)) / float(len(bags))
        cost = cost_regularization_term + cost_constraints_term
        return cost, cost_regularization_term, cost_constraints_term
