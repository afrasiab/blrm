import numpy as np
import tensorflow as tf
import os

from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin

__author__ = 'basir shariat (basir@rams.colostate.edu)'

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


class BLRM(LargeMarginRFPPMixin):
    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass

    def __init__(self, the_lambda=10e-2, n_epochs=500, learning_rate=0.01, id=None):
        super(BLRM, self).__init__(n_epochs, the_lambda, id)
        self.learning_rate = learning_rate
        self.sess = tf.Session()
        self.start = None
        self.end = None
        self.scores = None
        self.x_batch = None
        self.y_batch = None
        self.labels = None


    def train(self, data, validation=None):
        bags = data
        dimension = bags.values()[0][0].shape[1]
        self.x_batch = tf.placeholder(tf.float32, [None, dimension * 2])
        self.y_batch = tf.placeholder(tf.float32, [None, 1])
        # self.labels = tf.slice(yy, [self.start, 0], [self.end - self.start, 1])
        self.w = tf.get_variable('w'+self.id, shape=[2 * dimension, 1], dtype=tf.float32)
        # self.b = tf.get_variable('b', shape=[1], dtype=tf.float32)
        # self.w = tf.Variable(tf.random_normal([dimension, 1], stddev=0.001), dtype=tf.float32)
        self.scores = tf.matmul(self.x_batch, self.w)  # + self.b
        positive_indices = tf.where(tf.equal(self.y_batch, 1.))
        negative_indices = tf.where(tf.equal(self.y_batch, -1.))
        max_p = tf.reduce_max(tf.gather(self.scores, positive_indices))
        max_n = tf.reduce_max(tf.gather(self.scores, negative_indices))
        lamda = tf.constant(self.the_lambda, dtype=tf.float32)
        # loss = 0.5 * self.the_lambda * tf.matmul(tf.transpose(self.w), self.w) + tf.log(plus_one +
        #  tf.exp((self.scores - self.y_batch)))
        # loss = 0.5 * lamda * tf.matmul(tf.transpose(self.w), self.w) + tf.losses.hinge_loss(self.y_batch, self.scores)
        loss = 0.5 * lamda * tf.matmul(tf.transpose(self.w), self.w) + tf.maximum(0., 1. - (max_p - max_n))
        # optimizer = tf.train.AdagradDAOptimizer(self.learning_rate, tf.train.get_global_step())
        # optimizer = tf.train.MomentumOptimizer(self.learning_rate, 0.9)
        # optimizer = tf.train.AdagradOptimizer(self.learning_rate)
        optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)
        train = optimizer.minimize(loss=loss)
        # training loop
        self.sess.run(tf.global_variables_initializer())
        all_scores = {}
        keys = bags.keys()
        for i in range(self.n_epochs):
            np.random.shuffle(keys)
            p_loss = 0.
            for bag in keys:
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2:]
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                self.sess.run(train, {self.x_batch: x, self.y_batch: y[:, 0].reshape((-1, 1))})
                all_scores[bag], ll = self.sess.run([self.scores, loss],
                                                    {self.x_batch: x, self.y_batch: y[:, 0].reshape((-1, 1))})
                p_loss += ll[0, 0]
                all_scores[bag] = all_scores[bag].reshape(-1, )
            rfpps, aucs = self.get_rfpps(bags, bags_scores=all_scores)
            w = self.sess.run(self.w)
            w_n = np.sqrt(w.T.dot(w))[0][0]
            print("Epoch= {}/{}\t\t"
                  "|W|= {}\t\t"
                  "loss= {}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  "AUC= {:3.3f}".
                  format(i, self.n_epochs,
                         w_n,
                         p_loss,
                         int(np.percentile(rfpps.values(), 50)),
                         int(np.percentile(rfpps.values(), 80)),
                         int(np.percentile(rfpps.values(), 100)),
                         np.mean(aucs.values())
                         )
                  )

    # def train(self, data, validation=None):
    #     indices = {}
    #     bags = {}
    #
    #     if type(data) == type(dict()):
    #         print("Stacking Started")
    #         start = 0
    #         x_all = []
    #         y_all = []
    #         bags = data
    #         for bag in bags:
    #             # x, y, _ = bags[bag]
    #             l, r, e = bags[bag]
    #             x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2:]
    #             x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
    #             x_all.append(x)
    #             y_all.append(y)
    #             indices[bag] = start, start + x.shape[0]
    #             start += x.shape[0]
    #         print("Stacking Ended")
    #         xx = tf.constant(np.vstack(x_all), tf.float32)
    #         yy = tf.constant(np.vstack(y_all), tf.float32)
    #     elif type(data) == type(tuple()):
    #         bags["all"] = data
    #         xx = tf.constant(data[0], tf.float32)
    #         yy = tf.constant(data[1], tf.float32)
    #     else:
    #         raise ValueError("Please provide the correct arguments:\n{}".format(BLRM.train.__doc__))
    #     # with tf.device('/gpu:0'):
    #     self.start = tf.placeholder(tf.int32)
    #     self.end = tf.placeholder(tf.int32)
    #     self.x_batch = tf.slice(xx, [self.start, 0], [self.end - self.start, -1])
    #     self.y_batch = tf.slice(yy, [self.start, 0], [self.end - self.start, 1])
    #     # self.labels = tf.slice(yy, [self.start, 0], [self.end - self.start, 1])
    #     dimension = bags.values()[0][0].shape[1]
    #     self.w = tf.get_variable('w', shape=[2 * dimension, 1], dtype=tf.float32)
    #     # self.b = tf.get_variable('b', shape=[1], dtype=tf.float32)
    #     # self.w = tf.Variable(tf.random_normal([dimension, 1], stddev=0.001), dtype=tf.float32)
    #     self.scores = tf.matmul(self.x_batch, self.w) #+ self.b
    #     plus_one = tf.constant(+1, dtype=tf.float32)
    #     minus_one = tf.constant(-1, dtype=tf.float32)
    #     zero = tf.constant(0, dtype=tf.float32)
    #     positive_indices = tf.where(tf.equal(self.y_batch, plus_one))
    #     negative_indices = tf.where(tf.equal(self.y_batch, minus_one))
    #     max_p = tf.reduce_max(tf.gather(self.scores, positive_indices))
    #     max_n = tf.reduce_max(tf.gather(self.scores, negative_indices))
    #     lamda = tf.constant(self.the_lambda, dtype=tf.float32)
    #     # loss = 0.5 * self.the_lambda * tf.matmul(tf.transpose(self.w), self.w) + tf.log(plus_one +
    #     #  tf.exp((self.scores - self.y_batch)))
    #     # loss = 0.5 * lamda * tf.matmul(tf.transpose(self.w), self.w) + tf.losses.hinge_loss(self.y_batch,self.scores)
    #     loss = 0.5 * lamda * tf.matmul(tf.transpose(self.w), self.w) + tf.maximum(zero, plus_one - (max_p - max_n))
    #     # optimizer = tf.train.AdagradDAOptimizer(self.learning_rate, tf.train.get_global_step())
    #     # optimizer = tf.train.MomentumOptimizer(self.learning_rate, 0.9)
    #     # optimizer = tf.train.AdagradOptimizer(self.learning_rate)
    #     optimizer = tf.train.GradientDescentOptimizer(self.learning_rate)
    #     train = optimizer.minimize(loss=loss)
    #     # training loop
    #     self.sess.run(tf.global_variables_initializer())
    #     all_scores = {}
    #     keys = indices.keys()
    #     for i in range(self.n_epochs):
    #         np.random.shuffle(keys)
    #         l = 0
    #         for bag in keys:
    #             s, e = indices[bag]
    #             self.sess.run(train, {self.start: s, self.end: e})
    #             all_scores[bag], ll = self.sess.run([self.scores, loss], {self.start: s, self.end: e})
    #             l += ll[0][0]
    #             all_scores[bag] = all_scores[bag].reshape(-1, )
    #         rfpps, aucs = self.get_rfpps(bags, bags_scores=all_scores)
    #         w = self.sess.run(self.w)
    #         w_n = np.sqrt(w.T.dot(w))[0][0]
    #         print("Epoch= {}/{}\t\t"
    #               "|W|= {}\t\t"
    #               "loss= {}\t\t"
    #               "Median RFPP= {}\t{}\t{}\t\t"
    #               "AUC= {:3.3f}".
    #               format(i, self.n_epochs,
    #                      w_n,
    #                      l,
    #                      int(np.percentile(rfpps.values(), 50)),
    #                      int(np.percentile(rfpps.values(), 80)),
    #                      int(np.percentile(rfpps.values(), 100)),
    #                      np.mean(aucs.values())
    #                      )
    #               )

    def predict(self, x):
        scores = {}
        # w = self.sess.run([self.w])[0]
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                # x, _, _ = bags[bag]
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 3]
                # x = np.hstack((x1, x2))
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                # scores[bag] = x.dot(w).reshape(-1,)
                # with tf.device('/gpu:0'):
                scores[bag] = self.sess.run(self.scores, {self.x_batch: x}).reshape(-1, )

        else:
            # scores = x.dot(w).reshape(-1,)
            scores = self.sess.run(self.scores, {self.x_batch: x}).reshape(-1, )
        return scores

    def save(self, file_name=None):
        pass

    def load(self, file_name):
        pass
