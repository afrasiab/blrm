from __future__ import print_function

__author__ = 'basir shariat (basir@rams.colostate.edu)'

import multiprocessing as mp
import numpy as np
from abc import abstractmethod, ABCMeta
from bisect import bisect
from random import random
from numpy.core.umath_tests import inner1d
from rfpp_svm import LargeMarginRFPPMixin
from blrm.configuration import verbose

global_dict = {}

def compute_support_vector_score(params):
    i, x = params
    support_vector, alpha = global_dict['sv']
    diff = x - support_vector[i]
    norms_sq = np.einsum('ij,ij->i', diff, diff)
    return alpha[i] * np.exp(-global_dict['g'] * norms_sq)

def get_bag_alphas_updates(bag):
    x, y, _ = global_dict['bags'][bag]
    scores = np.zeros((y.shape[0],))
    if global_dict['sv'] is not None:
        support_vectors, alphas = global_dict['sv']
        pool = mp.Pool()
        results = list(pool.map(compute_support_vector_score, zip(range(len(alphas)), (x,)*len(alphas))))
        pool.join()
        for score in results:
            scores += score
    scores_p, scores_n = np.copy(scores), np.copy(scores)
    scores_p[np.where(y == -1)[0]] = -np.inf
    scores_n[np.where(y == 1)[0]] = -np.inf
    alpha_upddates = np.zeros(y.shape)
    if scores_p.max() < scores_n.max() + 1:
        alpha_upddates[scores_p.argmax()] += 1
        alpha_upddates[scores_n.argmax()] -= 1
    return alpha_upddates

class OfflineKernelizedTopK(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=20, the_lambda=10):
        super(OfflineKernelizedTopK, self).__init__(n_epochs, the_lambda)
        self.alphas = {}
        self.gamma = 0.5
        global_dict['g'] = self.gamma
        global_dict['sv'] = None

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_rfpps(self, bags):
        rfpps = {}
        for i, bag in enumerate(bags.keys()):
            x, y, _ = bags[bag]
            scores = self.get_scores(bags, bag).reshape(-1)
            rfpps[bag] = (np.where(y[np.argsort(-scores)] == 1)[0]).min()+1
        return rfpps

    def _get_current_cost(self, bags):
        pass
        # regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        # constraints_cost = 0
        # for bag in bags:
        #     x_i, y_i, _ = bags[bag]
        #     if x_i.shape[0] == 0:
        #         continue
        #     scores = self.get_scores(bags, bag)
        #     scores_p, scores_n = np.copy(scores), np.copy(scores)
        #     scores_p[np.where(y_i == -1)[0]] = -np.inf
        #     scores_n[np.where(y_i == 1)[0]] = -np.inf
        #     constraints_cost += (1. / len(bags)) * np.max((0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        # return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        for bag in bags:
            _, y, _ = bags[bag]
            self.alphas[bag] = np.zeros(y.shape)
        return bags

    def train(self, data, gamma=1):
        self.gamma = gamma
        bags = self.__check_input_and_initialize(data)
        global_dict['bags'] = bags
        bag_ids = bags.keys()
        bag_ids.sort()
        for t in range(1, self.n_epochs + 1):
            pool = mp.Pool()
            alpha_updates = pool.map(get_bag_alphas_updates, bag_ids)
            pool.close()
            pool.join()
            for i, bag in enumerate(bag_ids):
                self.alphas[bag] += alpha_updates[i]
            self.save_support_vectors(bags)
            # rfpps = self._get_rfpps(bags)
            # cost = self._get_current_cost(bags)
            # if verbose:
            #     cost = self._get_current_cost(bags)
            #     rfpp_percentiles = "["
            #     for pct in range(10,110, 10):
            #         if pct==50:
            #             rfpp_percentiles += "|{:^3}|, ".format(int(np.percentile(rfpps.values(), pct)))
            #         elif pct==100:
            #             rfpp_percentiles += "{:^3}]".format(int(np.percentile(rfpps.values(), pct)))
            #         else:
            #             rfpp_percentiles += "{:^3}, ".format(int(np.percentile(rfpps.values(), pct)))
            #     print("{}/{}\t|"
            #           # "\tCost: {:5.5f}\t{:5.5f}\t{:5.5f}\t|"
            #           "\t|w|: {:3.3f}\t|"
            #           "\t{}".
            #           format(e, self.n_epochs,
            #                  # cost[0], cost[1], cost[2],
            #                  np.linalg.norm(self.w),
            #                  rfpp_percentiles
            #                  )
            #           )
    def save_support_vectors(self, bags):
        support_vectors = []
        alphas = []
        for bag in self.alphas:
            x, _, _ = bags[bag]
            non_zeros = np.nonzero(self.alphas[bag])[0]
            for index in non_zeros:
                support_vectors.append(x[index])
                alphas.append(self.alphas[bag][index][0])
        # print("Number of support vectors : {}".format(len(support_vectors)))
        global_dict['sv'] = support_vectors, alphas

    def get_scores(self, bags, bag):
        x, _, _ = bags[bag]
        scores = np.zeros((x.shape[0], ))
        if global_dict['sv'] is not None:
            support_vectors, alphas = global_dict['sv']
            global_dict['x'] = x
            global_dict['g'] = self.gamma
            pool = mp.Pool(mp.cpu_count())
            results = list(pool.map(compute_support_vector_score, range(len(alphas))))
            pool.close()
            pool.join()
            for score in results:
                scores += score
        return scores

    def predict(self, x):
        scores = np.zeros((x.shape[0],))
        if global_dict['sv'] is not None:
            support_vectors, alphas = global_dict['sv']
            global_dict['g'] = self.gamma
            self.pool = mp.Pool()
            n = len(alphas)
            results = list(self.pool.map(compute_support_vector_score, zip(range(n), (x,)*n)))
            self.pool.close()
            self.pool.join()
            for score in results:
                scores += score
        return scores
