__author__ = 'basir shariat (basir@rams.colostate.edu)'
import multiprocessing as mp
import numpy as np
from datetime import datetime

def something_simple(n):
    a = 0
    for i in range(n):
        a+= np.exp(-i)




def main():
    start = datetime.now()
    n = 10000
    m = 1000
    for i in range(n):
        something_simple(m)
    print "sequential time {}".format(datetime.now() - start)
    start = datetime.now()
    pool = mp.Pool(mp.cpu_count())
    results = list(pool.imap_unordered(something_simple, (m,)*n))
    pool.close()
    pool.join()
    print "parallel time {}".format(datetime.now() - start)

if __name__ == '__main__':
    main()