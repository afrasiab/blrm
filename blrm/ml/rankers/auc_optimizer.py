from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin, SSGDRFPPSVM
from bisect import bisect
from random import random

import numpy as np
from PyML.utils.misc import ProgressBar
from abc import abstractmethod, ABCMeta
from sklearn.metrics import roc_auc_score
from numba import jit
#
# @jit
# def predict(self, x):
#     scores = {}
#     if isinstance(x, dict):
#         bags = x
#         for bag in bags:
#             l, r, e = bags[bag]
#             x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
#             # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
#             # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
#             # scores[bag] = np.min(np.hstack((s1, s2)), axis=1)
#             x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
#             scores[bag] = np.dot(x, self.w)
#     else:
#         mid = x.shape[1] / 2
#         x1 = x[:, :mid]
#         x2 = x[:, mid:]
#         # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
#         # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
#         # scores[bag] = np.dot(x, self.w)
#         # scores = np.min(np.hstack((s1, s2)), axis=1)
#         x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
#         scores = np.dot(x, self.w)
#     return scores
#
#
# @jit
# def get_rfpps(bags):
#     rfpps = {}
#     rfns = {}
#     aucs = {}
#     all_scores = self.predict(bags) if bags_scores is None else bags_scores
#     for i, bag in enumerate(bags.keys()):
#         # y = bags[bag][1]
#         y = bags[bag][2][:, 2]
#         scores = all_scores[bag]
#         aucs[bag] = roc_auc_score(y, scores)
#         rfpps[bag] = np.min(np.where(y[np.argsort(scores)[::-1]] == 1)[0]) + 1
#         rfns[bag] = np.min(np.where(y[np.argsort(scores)[::-1]] == -1)[0]) + 1
#     return rfpps, aucs, all_scores
#
#
# @jit
# def get_current_cost(bags, the_lambda, w):
#     regularization_cost = 0.5 * the_lambda * w.T.dot(w)
#     constraints_cost = 0
#     for bag in bags:
#         l, r, e = bags[bag]
#         x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
#         x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
#         scores = np.dot(x, w)
#         # x = np.hstack((x1, x2))
#         # scores = np.dot(np.hstack((x1, x2)), self.w)
#         p = np.where(y == +1)[0]
#         n = np.where(y == -1)[0]
#         constraints_cost += np.max((0, 1 - scores[p].max() - scores[n].max())) / float(len(bags))
#     return regularization_cost + constraints_cost, regularization_cost, constraints_cost
#
#
# @jit
# def train(training, w, n_epochs, the_lambda):
#     d = training[training.keys()[0]][0].shape[1]
#     n = float(len(training))
#     # self.w = np.zeros((d * 2,))
#     bb.w = np.random.rand(d * 2)
#     indices = range(len(training))
#     bag_ids = training.keys()
#     w_pocket = None
#     best = -np.Inf
#     # expanding bags
#     xx = {}
#     prog = ProgressBar("Preparing pairs of examples...")
#     for ii, bag in enumerate(training):
#         l, r, e = training[bag]
#         prog.progress((ii + 1.) / len(training))
#         x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
#         x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
#         neg_ind = np.where(y == -1)[0]
#         pos_ind = np.where(y == +1)[0]
#         k_p = np.minimum(len(pos_ind), len(neg_ind))
#         xx[bag] = x, y, pos_ind, neg_ind
#     print("Staring the training...")
#
#     for t in range(1, n_epochs + 1):
#         # print("Epoch= {:02d}/{}\t\t".format(t, self.n_epochs))
#         data = training
#         rfpps, aucs, _ = get_rfpps(data)
#         cost = get_current_cost(data)
#         rfpps_values = rfpps.values()
#         rfpps_values.sort()
#
#         if np.median(aucs.values()) >= best:
#             w_pocket = np.copy(bb.w)
#             best = np.median(aucs.values())
#         print("Epoch {:02d} of {}\t\t"
#               "Cost= {:0.5f}\t{:0.5f}\t{:0.8f}\t\t"
#               "Median RFPP= {}\t{}\t{}\t\t"
#               "|w|={:5.5f}\t\t"
#               "Median AUC={:5.5f}\t"
#               "Mean AUC={:5.5f}"
#               .format(t, bb.n_epochs, cost[0], cost[1], cost[2],
#                       rfpps_values[int(len(rfpps_values) * 0.5)],
#                       rfpps_values[int(len(rfpps_values) * 0.8)],
#                       rfpps_values[-1], np.linalg.norm(bb.w), np.median(aucs.values()), np.mean(aucs.values()))
#               )
#         np.random.shuffle(indices)
#         bags_shuffle = [bag_ids[i] for i in indices]
#
#         for iii, bag in enumerate(bags_shuffle):
#             x, y, pos_ind, neg_ind = xx[bag]
#             gp = bb.the_lambda * bb.w
#             eta_t = 1. / (bb.the_lambda * t)
#             scores = np.dot(x, bb.w)
#             for p in pos_ind:
#                 for n in neg_ind:
#                     if scores[p] < scores[n] + 1:
#                         gp -= (x[p] - x[n]) / (float(2. * d))
#             bb.w = bb.w - eta_t * (gp / float(n * len(neg_ind) * len(pos_ind)))
#     print("Saving the pocket weight vector...best AUC: {}".format(best))
#     bb.w = np.copy(w_pocket)

class LinearAUCOptimizer(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=150, the_lambda=0.1):
        super(LinearAUCOptimizer, self).__init__(n_epochs, the_lambda)

    def _get_cost_gradient(self, bags, k=1):
        pass

    def _get_current_cost(self, bags):
        regularization_cost = 0.5 * self.the_lambda * self.w.T.dot(self.w)
        constraints_cost = 0
        for bag in bags:
            l, r, e = bags[bag]
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            scores = np.dot(x, self.w)
            # x = np.hstack((x1, x2))
            # scores = np.dot(np.hstack((x1, x2)), self.w)
            p = np.where(y == +1)[0]
            n = np.where(y == -1)[0]
            constraints_cost += np.max((0, 1 - scores[p].max() - scores[n].max())) / float(len(bags))
        return regularization_cost + constraints_cost, regularization_cost, constraints_cost
    # @jit
    def train(self, training, validation=None, k=1):
        # self.w = train(training, self.w, self.n_epochs, self.the_lambda)
        d = training[training.keys()[0]][0].shape[1]
        n = float(len(training))
        # self.w = np.zeros((d * 2,))
        self.w = np.random.rand(d * 2)
        indices = range(len(training))
        bag_ids = training.keys()
        w_pocket = None
        best = -np.Inf
        # expanding bags
        xx = {}
        import gc
        prog = ProgressBar("Preparing pairs of examples...")
        for ii, bag in enumerate(training):
            l, r, e = training[bag]
            prog.progress((ii+1.)/len(training))
            x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            neg_ind = np.where(y == -1)[0]
            pos_ind = np.where(y == +1)[0]
            k_p = np.minimum(len(pos_ind), len(neg_ind))
            k = np.minimum(k, k_p) if k != -1 else k_p
            xx[bag] = x, y, pos_ind, neg_ind, k
            gc.collect()
        print("Staring the training...")

        for t in range(1, self.n_epochs + 1):
            # print("Epoch= {:02d}/{}\t\t".format(t, self.n_epochs))
            data = validation if validation is not None else training
            rfpps, aucs, _ = self.get_rfpps(data)
            cost = self._get_current_cost(data)
            rfpps_values = rfpps.values()
            rfpps_values.sort()

            if np.mean(aucs.values()) >= best:
                w_pocket = np.copy(self.w)
                best = np.mean(aucs.values())
            print("Epoch {:02d} of {}\t\t"
                  "Cost= {:0.5f}\t{:0.5f}\t{:0.8f}\t\t"
                  "Median RFPP= {}\t{}\t{}\t\t"
                  "|w|={:5.5f}\t\t"
                  "Median AUC={:5.5f}\t"
                  "Mean AUC={:5.5f}"
                  .format(t, self.n_epochs, cost[0], cost[1], cost[2],
                          rfpps_values[int(len(rfpps_values)*0.5)],
                          rfpps_values[int(len(rfpps_values)*0.8)],
                          rfpps_values[-1], np.linalg.norm(self.w), np.median(aucs.values()), np.mean(aucs.values()))
                  )
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]

            for iii, bag in enumerate(bags_shuffle):
                x, y, pos_ind, neg_ind, k = xx[bag]
                gp = self.the_lambda * self.w
                eta_t = 1. / (self.the_lambda * t)
                scores = np.dot(x, self.w)
                # counter = 1
                # np.random.shuffle(neg_ind)
                for p in pos_ind:
                    for n in neg_ind[:30]:
                        if scores[p] < scores[n]+1:
                            gp -= (x[p] - x[n]) / (float(2. * d))
                            # counter+=1
                self.w = self.w - eta_t * (gp / float(n))
        print("Saving the pocket weight vector...best AUC: {}".format(best))
        self.w = np.copy(w_pocket)

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
                # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
                # scores[bag] = np.min(np.hstack((s1, s2)), axis=1)
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                scores[bag] = np.dot(x, self.w)
        else:
            mid = x.shape[1] / 2
            x1 = x[:, :mid]
            x2 = x[:, mid:]
            # s1 = np.dot(np.hstack((x1, x2)), self.w).reshape((-1, 1))
            # s2 = np.dot(np.hstack((x2, x1)), self.w).reshape((-1, 1))
            # scores[bag] = np.dot(x, self.w)
            # scores = np.min(np.hstack((s1, s2)), axis=1)
            x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
            scores = np.dot(x, self.w)
        return scores


