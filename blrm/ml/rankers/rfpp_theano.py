from PyML.utils.misc import ProgressBar

__author__ = 'basir shariat (basir@rams.colostate.edu)'

import theano
import theano.tensor as T
import numpy as np


class RFPPSVMAverageHeuristic(object):
    """A heuristic for large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, c=10000, n_epochs=100, learning_rate=0.01):
        self.c = c
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

    def __create_expression_graph(self, d):
        self.x = T.matrix('x')  # data, presented as rasterized images
        self.y = T.ivector('y')  # labels, presented as 1D vector of [int] labels
        self.w = theano.shared(
            value=np.zeros(
                (d,),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        self.ones = np.ones((d,))
        self.scores = T.dot(self.x, self.w)
        self.y_pred = T.argmax(T.nnet.softmax(self.scores), axis=1)
        # self.cost = 0.5 * self.w.T.dot(self.w) + T.sum(self.x.dot(self.w)) * self.c / self.y.T.dot(self.y)
        # self.cost = 0.5 * self.w.T.dot(self.w) + (1 - self.ones.T.dot(self.x.dot(self.w))) * self.c / self.y.T.dot(self.y)
        self.cost = self.w.T.dot(self.w) - self.c * (self.y.T.dot(self.x.dot(self.w))) / self.y.T.dot(self.y)
        self.errors = T.mean(T.neq(self.y_pred, self.y))
        self.rfpp = T.argmin(T.eq(self.y[T.argsort(self.scores)], 1)) + 1
        self.get_scores = theano.function(
            inputs=[self.x],
            outputs=self.scores,
            allow_input_downcast=True
        )

    def __get_train_func(self):
        g_w = T.grad(cost=self.cost, wrt=self.w)
        updates = [(self.w, (self.w - self.learning_rate * g_w))]

        train_model = theano.function(
            inputs=[self.x, self.y],
            outputs=self.cost,
            updates=updates,
            allow_input_downcast=True
        )
        return train_model

    def __get_test_func(self, x):

        # compiling a Theano function `train_model` that returns the cost, but in
        # the same time updates the parameter of the model based on the rules
        # defined in `updates`
        test_model = theano.function(
            inputs=[self.x],
            outputs=self.scores,
            allow_input_downcast=True
            # givens={
            #     self.y: y
            # }
        )
        return test_model

    def train(self, data):
        """
        :param x: training vectors. a two dimensional numpy array of shape (n, d) where n is the number of objects and d is the dimension of the feature representation.
        :param y: labels of the x. it is a one dimensional numpy array of shape (n,) which are either -1 or 1
        :return: None
        """
        bags = {}
        if type(data) == type(dict()):
            bags = data
        elif type(data) == type(tuple()):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments:\n{}".format(RFPPSVMAverageHeuristic.train.__doc__))

        self.__create_expression_graph(bags[bags.keys()[0]][0].shape[1])
        train_function = self.__get_train_func()
        rfpps = {}
        for bag in bags:
            x, y, _ = bags[bag]
            scores = self.get_scores(x)
            rfpps[bag] = np.min(np.where(y[np.argsort(scores)] == 1)[0])
        print("{}".format(np.mean(rfpps.values())))
        x = None
        y = None
        keys = bags.keys()
        keys.sort()
        for bag in keys:
            xx, yy, _ = bags[bag]
            x = xx if x is None else np.vstack((xx, x))
            y = yy if y is None else np.concatenate((yy, y))
        for epoch in range(1, self.n_epochs + 1):
            cost = train_function(x, y)
            scores = self.get_scores(x)
            start = 0
            # for bag in keys:
            #     xx, yy, _ = bags[bag]
            #     end = start + len(yy)
            #     scores_b = scores[start:end]
            #     y_b = (yy[start:end])[np.argsort(scores_b)]
            #     rfpps[bag] = np.min(np.where(y_b == 1)[0])
            #     start = end
            print("epoch {}: cost={}".format(epoch, cost))

    def train1(self, data):
        """
        :param x: training vectors. a two dimensional numpy array of shape (n, d) where n is the number of objects and d is the dimension of the feature representation.
        :param y: labels of the x. it is a one dimensional numpy array of shape (n,) which are either -1 or 1
        :return: None
        """
        bags = {}
        if type(data) == type(dict()):
            bags = data
        elif type(data) == type(tuple()):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments:\n{}".format(RFPPSVMAverageHeuristic.train.__doc__))

        epoch = 0
        p = ProgressBar("Training Avg. Proxy RFPP SVM ")
        self.__create_expression_graph(bags[bags.keys()[0]][0].shape[1])
        train_function = self.__get_train_func()
        bags_keys = bags.keys()
        indices = range(len(bags_keys))
        rfpps = {}

        for bag in bags:
            x, y, _ = bags[bag]
            scores = self.get_scores(x)
            rfpps[bag] = np.min(np.where(y[np.argsort(scores)] == 1)[0])
        print("{}".format(np.mean(rfpps.values())))

        for epoch in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            for index in indices:
                bag = bags_keys[index]
                x, y, _ = bags[bag]
                cost = train_function(x, y)
                w = self.w.get_value()
                w_norm = w.T.dot(w)
                # m = " cost={} , w={} ".format(cost, w_norm)
                # p.progress(epoch / float(self.n_epochs), custom_message=m)

                scores = self.get_scores(x)
                rfpps[bag] = np.min(np.where(y[np.argsort(scores)] == 1)[0])
            print("epoch {}: RFPP={}, cost={}".format(epoch, np.mean(rfpps.values()), cost))
            # for v in zip(*(rfpps[k] for k in bags_keys)): print(*v, sep='\t')
            # print(epoch, np.mean(rfpps.values()),  self.w.get_value())

    def lazy_train(self, dataset, bags_keys, feature_set):
        """
        lazy loader, no caching
        :param x: training vectors. a two dimensional numpy array of shape (n, d) where n is the number of objects and d is the dimension of the feature representation.
        :param y: labels of the x. it is a one dimensional numpy array of shape (n,) which are either -1 or 1
        :return: None
        """
        epoch = 0
        p = ProgressBar("Average RFPP SVM ")
        a_bag = dataset.get_bags(feature_set, [bags_keys[0]], -1)
        self.__create_expression_graph(a_bag[bags_keys[0]][0].shape[1])
        train_function = self.__get_train_func()
        indices = range(len(bags_keys))
        for epoch in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            for index in indices:
                x, y, _ = dataset.get_bags(feature_set, [bags_keys[index]], -1)[bags_keys[index]]
                cost = train_function(x, y)
                w = self.w.get_value()
                w_norm = w.T.dot(w)
                m = " cost={} , w={} ".format(cost, w_norm)
                p.progress(epoch / float(self.n_epochs), custom_message=m)

    def predict(self, x):
        test_model = self.__get_test_func(x)
        return test_model(x)

    def save(self, file_name=None):
        pass

    def load(self, file_name):
        pass
