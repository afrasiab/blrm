from __future__ import print_function

import dispy
import numpy as np
from PyML.utils.misc import ProgressBar
import os

__author__ = 'basir shariat (basir@rams.colostate.edu)'

global_dict = {}

def task2(x):
    import numpy as np
    import multiprocessing as mp
    pool = mp.Pool()
    results = list(pool.map(dot_product, x))
    pool.close()
    pool.join()
    return np.array(results)


def task(i, svs, alphas, g):
    import sys
    import numpy as np
    import multiprocessing as mp
    # noinspection PyBroadException
    global_dict['x'] = i
    try:
        pool = mp.Pool()
        n = len(alphas)
        results = list(pool.map(dot_product, zip([i]*n, svs, alphas, [g]*n)))
        pool.close()
        pool.join()

        return np.array(results)
    except:
        e = sys.exc_info()
        m = e[1].message
        return m + os.path.dirname(os.path.realpath(__file__))


def dot_product(s):
    i, sv, alpha, g = s
    import numpy as np
    direct = "/s/jawar/c/nobackup/prot/basir/blrm/PPv3/blrm/ml/rankers/randoms-10000x100"
    file_name = "{}/x_{}.npy".format(direct, i)
    x = np.load(file_name)
    diff = x - sv
    norms_sq = np.einsum('ij,ij->i', diff, diff)
    return alpha*np.exp(-g*norms_sq)
    # return sv


def main():
    from datetime import datetime
    start = datetime.now()
    n = 10000
    d = 100
    folder = "randoms-{}x{}".format(n, d)
    if not os.path.exists(folder):
        os.mkdir(folder)
    import dispy
    cluster = dispy.JobCluster(task, depends=[dot_product])
    svs = np.random.rand(1000, d)
    alphas = np.random.rand(1000, )
    g = 1
    # cluster.shutdown()
    # return
    import dispy.httpd
    http_server = dispy.httpd.DispyHTTPServer(cluster)
    for i in range(2):
        print("============================================================")
        all_scores = {}
        jobs = []
        xs = []
        p = ProgressBar("submiting jobs".format(i))
        m = 100
        for j in range(m):
            p.progress((j + 1) / float(m))
            file_name = "{}/x_{}.npy".format(folder, j)
            if not os.path.exists(file_name):
                np.save(file_name,  np.random.rand(n, d))
            x = np.load(file_name)
            jobs.append(cluster.submit(j, svs, alphas, g))
        for j, job in enumerate(jobs):
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")

            # p.progress((j + 1.) / len(jobs))
            s = job()
            if isinstance(s, str):
                print (s)
            else:
                file_name = "{}/x_{}.npy".format(folder, j)
                x = np.load(file_name)
                print(s[0,0] / x[0,0])
            # print ("______")
            # print (s.reshape(-1,))
    cluster.wait() # wait for all jobs to finish
    cluster.close()
    print(datetime.now()-start)

if __name__ == '__main__':
    main()
