from __future__ import print_function

import dispy
import numpy as np
import multiprocessing as mp
from PyML.utils.misc import ProgressBar
from blrm.configuration import verbose
from rfpp_svm import LargeMarginRFPPMixin
import ctypes
import os

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def compute_support_vector_scores(params):
    import numpy as np
    import sharedmem
    g, sv, alpha = params
    xa = np.frombuffer(globals()['x']).reshape(globals()['shape'])
    diff = xa - sv
    return alpha * np.exp(-g * np.einsum('ij,ij->i', diff, diff))


def compute_scores(bag, x, support_vectors, alphas, gamma):
    import numpy as np
    import multiprocessing as mp
    import sharedmem
    import ctypes
    scores = np.zeros((x.shape[0],))
    n = len(alphas)
    if n != 0:
        xa = mp.Array(ctypes.c_double, x.size, lock=False)
        buffer = np.frombuffer(xa)
        buffer[:] = x.reshape(-1, )
        globals()['x'] = xa
        globals()['shape'] = x.shape
        pool = mp.Pool()
        params = zip((gamma,) * n, support_vectors, alphas)
        results = list(pool.map(compute_support_vector_scores, params))
        pool.close()
        pool.join()
        for score in results:
            scores += np.array(score)

    return bag, scores


class ClusteredKernelizedTopK(LargeMarginRFPPMixin):
    def __init__(self, n_epochs=20, the_lambda=1, gamma=0.005):
        super(ClusteredKernelizedTopK, self).__init__(n_epochs, the_lambda)
        self.alphas = {}
        self.servers = []
        self.support_vectors = None
        self.scores = None
        self.gamma = gamma
        self.__run_servers()
        import dispy
        self.cluster = dispy.JobCluster(compute_scores, depends=[compute_support_vector_scores], nodes = ["bacon"])

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass
        # os.system("")

    def __run_servers(self):
        from pprint import pprint
        # from pssh import ParallelSSHClient
        with open('{}/servers'.format(os.path.dirname(__file__))) as f:
            from blrm.utils.pretty_print import Print
            Print.print_info("Initializing Cluster")
            hosts = f.read().splitlines()
            # client = ParallelSSHClient(hosts)
            # output = client.run_command('source pp_activate && nohup python dispynode.py&')
            # for host in output:
            #     for line in output[host]['stdout']:
            #         pprint("Host %s: %s" % (host, line))
            self.servers = [host.split(".")[0] for host in hosts]
            Print.print_info("Cluster Initialized")

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        for bag in bags:
            _, y, _ = bags[bag]
            self.alphas[bag] = np.zeros(y.shape)
        return bags

    def train(self, data, k=1):
        bags = self.__check_input_and_initialize(data)
        bags_scores = {bag: np.zeros(((bags[bag][1]).shape[0],)) for bag in bags}
        for t in range(1, self.n_epochs + 1):
            rfpps, aucs = self.get_rfpps(bags, bags_scores)
            if verbose:
                rfpp_percentiles = self._get_rfpp_percentiles(rfpps)
                print("{}/{}\t|\tMean AUC: {:3.3f}\t|\t{}".format(t, self.n_epochs, np.mean(aucs.values()),
                                                                  rfpp_percentiles))
            bags_scores = self.predict(bags)
            for bag in bags_scores:
                y = bags[bag][1]
                scores = bags_scores[bag]
                scores_p, scores_n = np.copy(scores), np.copy(scores)
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf
                for m in range(k):
                    if scores_p.max() < scores_n.max() + 1:
                        self.alphas[bag][scores_p.argmax()] += 1
                        self.alphas[bag][scores_n.argmax()] -= 1
                        scores_p[scores_p.argmax()] = -np.inf
            if np.max(rfpps.values()) == 1:
                break
        self.support_vectors, self.alphas = self.get_support_vectors(bags)

    def get_support_vectors(self, bags):
        support_vectors = []
        alphas = []
        for bag in self.alphas:
            x, _, _ = bags[bag]
            non_zeros = np.nonzero(self.alphas[bag])[0]
            for index in non_zeros:
                support_vectors.append(x[index])
                alphas.append(self.alphas[bag][index][0])
        return support_vectors, alphas

    def predict(self, params):
        bags = {}
        if isinstance(params, dict):
            bags = params
        else:
            bags["$$$"] = params, None, None

        all_scores = {}
        support_vectors, alphas = self.get_support_vectors(bags) if self.support_vectors is None else (
            self.support_vectors, self.alphas)

        jobs = []
        for i, bag in enumerate(bags):
            x, _, _ = bags[bag]
            jobs.append(self.cluster.submit(bag, x, support_vectors, alphas, self.gamma))
        p = ProgressBar("Finished bags")

        for i, job in enumerate(jobs):
            p.progress((i + 1.) / len(jobs))
            r = job()
            bag, s = r
            all_scores[bag] = s

        if len(bags) == 1 and bags.keys()[0] == "$$$":
            return all_scores.values()[0]
        return all_scores
