from __future__ import print_function

import numpy as np
import os

__author__ = 'basir shariat (basir@rams.colostate.edu)'


def dot_product(i):
    return globals()['x'][i-1]/(i)


def task(x):
    import sys
    import multiprocessing as mp
    import numpy as np
    globals()['x'] = x
    # noinspection PyBroadException
    try:
        pool = mp.Pool()
        results = list(pool.map(dot_product, range(1, 1+x.shape[0])))
        pool.close()
        pool.join()
        return np.array(results)
    except:
        e = sys.exc_info()
        m = e[1].message
        return m + os.path.dirname(os.path.realpath(__file__))


def main():
    from datetime import datetime
    import dispy
    start = datetime.now()
    cluster = dispy.JobCluster(task, depends=[dot_product], nodes = ["bacon"])
    #
    # cluster.shutdown()
    jobs = []
    xs = []
    for j in range(10):
        xs.append(np.random.rand(5, ))
        jobs.append(cluster.submit(xs[-1]))
    for j, job in enumerate(jobs):
        s = job()
        if isinstance(s, str):
            print(s)
        else:
            print(xs[j]/s)
    # cluster.wait()
    # cluster.close()
    print(datetime.now() - start)


if __name__ == '__main__':
    main()
