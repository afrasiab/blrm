import numpy as np
import tensorflow as tf
import os
from sklearn.svm import SVR

from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin

__author__ = 'basir shariat (basir@rams.colostate.edu)'

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


class SVRBLRM(LargeMarginRFPPMixin):
    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass

    def __init__(self, the_lambda=10e-2, n_epochs=500, learning_rate=0.01):
        super(SVRBLRM, self).__init__(n_epochs, the_lambda)
        self.learning_rate = learning_rate
        self.sess = tf.Session()
        self.start = None
        self.end = None
        self.scores = None
        self.x_batch = None
        self.y_batch = None
        self.labels = None

    def train(self, data, validation=None):
        indices = {}
        bags = {}

        if type(data) == type(dict()):
            print("Stacking Started")
            start = 0
            x_all = []
            y_all = []
            bags = data
            for bag in bags:
                # x, y, _ = bags[bag]
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2:]
                # x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                x = np.hstack((x1, x2))

                x_all.append(x)
                y_all.append(y)
                indices[bag] = start, start + x.shape[0]
                start += x.shape[0]
            print("Stacking Ended")
            xx = np.vstack(x_all)
            yy = np.vstack(y_all)
        elif type(data) == type(tuple()):
            bags["all"] = data
            xx = data[0]
            yy = data[1]
        else:
            raise ValueError("Please provide the correct arguments:\n{}".format(SVRBLRM.train.__doc__))
        # svr_rbf = SVR(kernel='rbf', C=1000, gamma=0.5, verbose=True)
        svr_lin = SVR(kernel='linear', C=1e3)
        # svr_poly = SVR(kernel='poly', C=1e3, degree=2)
        print("training SVR started")
        self.model = svr_lin.fit(xx, yy[:, 1])
        print(self.get_rfpps(bags))
        # y_lin = svr_lin.fit(X, y).predict(X)
        # y_poly = svr_poly.fit(X, y).predict(X)


    def predict(self, x):
        scores = {}
        # w = self.sess.run([self.w])[0]
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                # x, _, _ = bags[bag]
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 3]
                x = np.hstack((x1, x2))
                # x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                # scores[bag] = x.dot(w).reshape(-1,)
                # with tf.device('/gpu:0'):
                scores[bag] = self.model.predict(x)

        else:
            # scores = x.dot(w).reshape(-1,)
            scores = self.sess.run(self.scores, {self.x_batch: x}).reshape(-1, )
        return scores

    def save(self, file_name=None):
        pass

    def load(self, file_name):
        pass
