from blrm.ml.rankers.GBLRM import GBLRM

from blrm.ml.rankers.rfpp_svm import LargeMarginRFPPMixin, SSGDRFPPSVM, KernelizedSSGDRFPPSVM
from blrm.ml.rankers.auc_optimizer import LinearAUCOptimizer
import multiprocessing as mp
import numpy as np
from PyML.utils.misc import ProgressBar

from blrm.ml.rankers.ssrfpp_tensorflow import BLRM

__author__ = 'basir shariat (basir@rams.colostate.edu)'
_params = {}


def _train_estimators_parallel_function(estimator, bags_index):
    estimator.train(_params['bags'][bags_index])


def _predict_estimators_parallel_function(p):
    estimator, x = p
    return estimator.predict(x)


class RFPPOnlineEnsemble(LargeMarginRFPPMixin):
    def get_model_params(self):
        ws = []
        for i, estimator in enumerate(self.estimators):
            ws.append(estimator.get_model_params())
        return tuple(ws)

    def __init__(self, n_epochs=50, the_lambda=0.01, n_estimators=10):
        super(RFPPOnlineEnsemble, self).__init__(n_epochs, the_lambda)
        self.estimators = []
        for i in range(n_estimators):
            # self.estimators.append(LinearAUCOptimizer(n_epochs, the_lambda))
            # self.estimators.append(KernelizedSSGDRFPPSVM(n_epochs, the_lambda))
            self.estimators.append(SSGDRFPPSVM(n_epochs, the_lambda))

    def train(self, bags_set, validation=None, k=1):
        # pool = mp.Pool(mp.cpu_count())
        # _params['bags'] = bags_set
        # print("Ensemble Training")
        # pool.imap_unordered(_train_estimators_parallel_function, self.estimators, range(len(bags_set)))
        # pool.close()
        # pool.join()
        p = ProgressBar("Training Ensemble")
        for i, estimator in enumerate(self.estimators):
            p.progress((i + 1.) / len(self.estimators))
            estimator.train(bags_set[i],  k=k, validation=validation)#
        print("Ensemble Training Finished")

    # def train(self, bags, k=1):
    #     # pool = mp.Pool(mp.cpu_count())
    #     # _params['bags'] = bags
    #     # print("Ensemble Training")
    #     # pool.imap_unordered(_train_estimators_parallel_function, self.estimators)
    #     # pool.close()
    #     # pool.join()
    #     p = ProgressBar("Training Ensemble")
    #     for i, estimator in enumerate(self.estimators):
    #         p.progress((i + 1.) / len(self.estimators))
    #         estimator.train(bags)
    #     print("Ensemble Training Finished")

    def predict(self, x):
        # print("predict is called")
        # pool = mp.Pool(mp.cpu_count())
        # xs = [x for i in range(len(self.estimators))]
        # results = list(pool.imap_unordered(_predict_estimators_parallel_function, zip(self.estimators, xs)))
        # pool.close()
        # pool.join()
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                results = []
                # x, _, _ = bags[bag]
                l, r, e = bags[bag]
                x1, x2, y = l[e[:, 0]], r[e[:, 1]], e[:, 2]
                # x = np.hstack((x1, x2))
                x = np.hstack(((x1 + x2), np.multiply(x1, x2)))
                for i, e in enumerate(self.estimators):
                    results.append(e.predict(x).reshape((-1, 1)))
                s = 0
                # s = results[0]
                for result in results:
                    s += result
                    # s = np.min(np.hstack((result, s)), axis=1).reshape((-1, 1))
                s /= float(len(self.estimators))
                scores[bag] = s.reshape((-1,))
        else:
            scores = np.dot(x, self.w)
        return scores

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass


class RFPPOnlinePatchEnsemble(LargeMarginRFPPMixin):
    def __init__(self, n_epochs=50, the_lambda=0.001, n_estimators=30):
        super(RFPPOnlinePatchEnsemble, self).__init__(n_epochs, the_lambda)
        self.estimators = []
        self.ds = []
        for i in range(n_estimators):
            self.estimators.append(SSGDRFPPSVM(n_epochs, the_lambda))

    def _sample_bags(self, bags, f_ratio=0.1, n_ratio=1):
        sampled_bags = {}
        dim = bags[(bags.keys()[0])][0].shape[1]
        feature_indices = np.random.choice(dim, int(dim * f_ratio), replace=False)
        for bag in bags:
            x, y, d = bags[bag]
            neg_indices = np.where(y == -1)[0]
            np.random.shuffle(neg_indices)
            indices = list(np.where(y == 1)[0])
            end_index = min(n_ratio * len(indices), len(neg_indices))
            indices.extend(list(neg_indices[:end_index]))
            sampled_bags[bag] = np.copy(x[np.ix_(indices, feature_indices)]), np.copy(y[indices]), np.copy(0)
        return sampled_bags, feature_indices

    def train(self, bags):
        p = ProgressBar("Training Ensemble")
        self.ds = []
        for i, estimator in enumerate(self.estimators):
            sampled_bags, d = self._sample_bags(bags)
            self.ds.append(d)
            p.progress((i + 1.) / len(self.estimators))
            estimator.train(sampled_bags)
        print("Ensemble Training Finished")

    def predict(self, x):
        scores = {}
        if isinstance(x, dict):
            bags = x
            for bag in bags:
                results = []
                x, _, _ = bags[bag]
                for i, e in enumerate(self.estimators):
                    results.append(e.predict(x[:, self.ds[i]]))
                s = 0
                for result in results:
                    s += result
                s /= float(len(self.estimators))
                scores[bag] = s
        else:
            scores = np.dot(x, self.w)
        return scores

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass
