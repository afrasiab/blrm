from __future__ import print_function

__author__ = 'basir shariat (basir@rams.colostate.edu)'
global_dict = {}
import multiprocessing as mp
import numpy as np
from abc import abstractmethod, ABCMeta
from bisect import bisect
from random import random
from numpy.core.umath_tests import inner1d
from rfpp_svm import LargeMarginRFPPMixin
from blrm.configuration import verbose
from PyML.utils.misc import ProgressBar


def compute_support_vector_score(i):
    alpha, support_vector = global_dict['sv']
    x = global_dict['x']
    diff = x - support_vector[i]
    norms_sq = np.einsum('ij,ij->i', diff, diff)
    # print("Norm {}, {}, {}".format(alpha[i].shape, norms_sq.shape, x.shape))
    return alpha[i] * np.exp(-global_dict['g'] * norms_sq)


class KernelizedLMRFPPOptimizer(LargeMarginRFPPMixin):
    """A Stochastic Sub-gradient decent large-margin rfpp optimizer

    The rfpp optimizer is fully described by a weight matrix :math:`W`
    and bias vector :math:`b`.
    """

    def __init__(self, n_epochs=50, the_lambda=1):
        super(KernelizedLMRFPPOptimizer, self).__init__(n_epochs, the_lambda)
        self.alphas = {}
        global_dict['sv'] = None
        self.gamma = 0.005
        global_dict['g'] = self.gamma

    def _get_cost_gradient(self, bags, k=-1):
        pass

    def _get_current_cost(self, bags):
        pass
        # regularization_cost = (0.5 * self.the_lambda * self.w.T.dot(self.w))
        # constraints_cost = 0
        # for bag in bags:
        #     x_i, y_i, _ = bags[bag]
        #     if x_i.shape[0] == 0:
        #         continue
        #     scores = self.get_scores(bags, bag)
        #     scores_p, scores_n = np.copy(scores), np.copy(scores)
        #     scores_p[np.where(y_i == -1)[0]] = -np.inf
        #     scores_n[np.where(y_i == 1)[0]] = -np.inf
        #     constraints_cost += (1. / len(bags)) * np.max((0, 1 - self.w.T.dot(x_i[scores_p.argmax()] - x_i[scores_n.argmax()])))
        # return regularization_cost + constraints_cost, regularization_cost, constraints_cost

    def __check_input_and_initialize(self, data):
        bags = {}
        if isinstance(data, dict):
            bags = data
        elif isinstance(data, tuple):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments\n")
        for bag in bags:
            _, y, _ = bags[bag]
            self.alphas[bag] = np.zeros(y.shape)
        return bags

    def train(self, data, validation=None, gamma=1, k=1):
        self.gamma = gamma
        bags = self.__check_input_and_initialize(data)
        indices = range(len(bags))
        bag_ids = bags.keys()
        for t in range(1, self.n_epochs + 1):
            np.random.shuffle(indices)
            bags_shuffle = [bag_ids[i] for i in indices]
            p = ProgressBar("Iteration {} ".format(t))
            scores = {}
            for ii, bag in enumerate(bags_shuffle):
                x, y, _ = bags[bag]
                p.progress(float(ii + 1) / len(bags))
                scores[bag] = self.get_scores(bags, bag)
                scores_p, scores_n = np.copy(scores[bag]), np.copy(scores[bag])
                scores_p[np.where(y == -1)[0]] = -np.inf
                scores_n[np.where(y == 1)[0]] = -np.inf

                for index in range(k):
                    if scores_p.max() < scores_n.max() + 1:
                        self.alphas[bag][scores_n.argmax()] -= 1
                    self.alphas[bag][scores_p.argmax()] += 1
                    scores_p[scores_p.argmax()] = -np.inf

            self.save_support_vectors(bags)
            rfpps, aucs = self.get_rfpps(bags, bags_scores=scores)
            if verbose:
                rfpp_percentiles = self._get_rfpp_percentiles(rfpps)
                print("{}/{}\t|\t{}\t|\t{}".format(t, self.n_epochs, rfpp_percentiles, np.mean(aucs.values())))

    def save_support_vectors(self, bags):
        support_vectors = []
        alphas = []
        for bag in self.alphas:
            x, _, _ = bags[bag]
            non_zeros = np.nonzero(self.alphas[bag])[0]
            for index in non_zeros:
                support_vectors.append(x[index])
                alphas.append(self.alphas[bag][index][0])
        print("Number of support vectors : {}".format(len(support_vectors)))
        global_dict['sv'] = (alphas, support_vectors)

    def get_scores(self, bags, bag):
        x, _, _ = bags[bag]
        scores = np.zeros((x.shape[0],))
        if global_dict['sv'] is not None:
            alphas, support_vectors = global_dict['sv']
            global_dict['x'] = x
            self.pool = mp.Pool(mp.cpu_count())
            results = list(self.pool.map(compute_support_vector_score, range(len(alphas))))
            self.pool.close()
            self.pool.join()
            for score in results:
                scores += score
        return scores

    def predict(self, x):
        if isinstance(x, dict):
            scores = {}
            bags = x
            for bag in bags:
                x, _, _ = bags[bag]
                s = np.zeros((x.shape[0],))
                if global_dict['sv'] is not None:
                    support_vectors, alphas = global_dict['sv']
                    global_dict['x'] = x
                    self.pool = mp.Pool()
                    results = list(self.pool.map(compute_support_vector_score, range(len(alphas))))
                    self.pool.close()
                    self.pool.join()
                    for score in results:
                        s += score
                scores[bag] = s
        else:
            scores = np.zeros((x.shape[0],))
            if global_dict['sv'] is not None:
                support_vectors, alphas = global_dict['sv']
                global_dict['x'] = x
                self.pool = mp.Pool()
                results = list(self.pool.map(compute_support_vector_score, range(len(alphas))))
                self.pool.close()
                self.pool.join()
                for score in results:
                    scores += score
        return scores

        scores = np.zeros((x.shape[0],))
        if global_dict['sv'] is not None:
            support_vectors, alphas = global_dict['sv']
            global_dict['x'] = x
            self.pool = mp.Pool()
            results = list(self.pool.map(compute_support_vector_score, range(len(alphas))))
            self.pool.close()
            self.pool.join()
            for score in results:
                scores += score
        return scores
