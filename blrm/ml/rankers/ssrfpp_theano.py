from PyML.utils.misc import ProgressBar

__author__ = 'basir shariat (basir@rams.colostate.edu)'

import theano
import theano.tensor as t
import numpy as np


class SSRFPP(object):

    def __init__(self, the_lambda=100, n_epochs=50, learning_rate=0.01):
        self.the_lambda = the_lambda
        self.n_epochs = n_epochs
        self.learning_rate = learning_rate

    def __create_expression_graph(self, d):
        self.x_p = t.matrix('x_p')
        self.y_p = t.ivector('y_p')
        self.x_n = t.matrix('x_n')
        self.y_n = t.ivector('y_n')
        self.x = t.matrix('x')
        self.y = t.ivector('y')
        self.w = theano.shared(
            value=np.zeros(
                (d,),
                dtype=theano.config.floatX
            ),
            name='W',
            borrow=True
        )
        self.ones = np.ones((d,))
        self.scores_p = t.dot(self.x_p, self.w)
        self.scores_n = t.dot(self.x_n, self.w)
        self.y_pred_p = t.argmax(t.nnet.softmax(self.scores_p), axis=1)
        self.y_pred_n = t.argmax(t.nnet.softmax(self.scores_n), axis=1)
        self.hinge = abs(t.max(self.scores_p) - t.max(self.scores_n) - 1) - (t.max(self.scores_p) - t.max(self.scores_n) - 1)
        self.cost = 0.5 * self.the_lambda * self.w.T.dot(self.w) + self.hinge
        self.errors = t.mean(t.neq(self.y_pred_p, self.y_p)) + t.mean(t.neq(self.y_pred_n, self.y_n))
        self.scores = t.dot(self.x, self.w)
        self.rfpp = t.argmin(t.eq(self.y[t.argsort(self.scores)], 1)) + 1
        self.get_scores = theano.function(
            inputs=[self.x],
            outputs=self.scores,
            allow_input_downcast=True
        )
        # result, updates = theano.scan(fn=lambda bag_cost, A: bag_cost * A,
        #                               outputs_info=t.ones_like(A),
        #                               non_sequences=A,
        #                               n_steps=k)
        #
        # # We only care about A**k, but scan has provided us with A**1 through A**k.
        # # Discard the values that we don't care about. Scan is smart enough to
        # # notice this and not waste memory saving them.
        # final_result = result[-1]
        #
        # # compiled function that returns A**k
        # power = theano.function(inputs=[A, k], outputs=final_result, updates=updates)

    def __get_train_func(self):
        g_w = t.grad(cost=self.cost, wrt=self.w)
        updates = [(self.w, self.w - self.learning_rate * g_w)]

        train_model = theano.function(
            inputs=[self.x_p, self.x_n],
            outputs=self.cost,
            updates=updates,
            allow_input_downcast=True
        )
        return train_model

    def __get_test_func(self, x):

        # compiling a Theano function `train_model` that returns the cost, but in
        # the same time updates the parameter of the model based on the rules
        # defined in `updates`
        test_model = theano.function(
            inputs=[self.x],
            outputs=self.scores,
            allow_input_downcast=True
            # givens={
            #     self.y: y
            # }
        )
        return test_model

    def train(self, data):
        bags = {}
        if type(data) == type(dict()):
            bags = data
        elif type(data) == type(tuple()):
            bags["all"] = data
        else:
            raise ValueError("Please provide the correct arguments:\n{}".format(SSRFPP.train.__doc__))

        self.__create_expression_graph(bags[bags.keys()[0]][0].shape[1])
        train_function = self.__get_train_func()
        rfpps = {}
        for epoch in range(1, self.n_epochs + 1):
            for bag in bags:
                x, y, _ = bags[bag]
                x_p, _, x_n, _ = x[(y == 1).reshape(-1,), :], y[y == 1], x[(y == -1).reshape(-1,), :], y[y == -1]
                cost = train_function(x_p, x_n)
                scores = self.get_scores(x)
                rfpps[bag] = np.min(np.where(y[np.argsort(-scores)] == 1)[0]) + 1
            print("epoch {}: RFPP={}".format(epoch, np.median(rfpps.values())))

    def predict(self, x):
        test_model = self.__get_test_func(x)
        return test_model(x)

    def save(self, file_name=None):
        pass

    def load(self, file_name):
        pass
