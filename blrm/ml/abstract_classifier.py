


class AbstractClaasifier(object):
    __metaclass__ = ABCMeta
    _protein = None

    def __init__(self, protein, **kwargs):
        self._protein = protein

    @abstractmethod
    def extract(self):
        pass

    def load(self, protein_name):
        return np.load(self._get_file_name(protein_name))

    @staticmethod
    def _normalize(v):
        if len(v.shape) == 1:
            v = v.reshape(1, -1)
        # print v.shape
        v = v.astype(float)
        return normalize(v)[0]

    @abstractmethod
    def _get_file_name(self, protein_name):
        pass
