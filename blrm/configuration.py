from os import path
import os
import blrm
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'

database = "DBD5/"
pairpred_home = path.dirname(blrm.__path__[0]) + "/"
data_directory = pairpred_home + "data/"

report_templates_directory = data_directory + "report_templates/"
latex_templates_directory = report_templates_directory + "latex/"
latex_report_template = latex_templates_directory + "sample.tex"

pymol_directory = data_directory + "pymol"
pymol_patches_directory = pymol_directory + "patches"
seed = 1
# np.random.seed(seed=seed)
similarity_threshold = 50
unsupervised_database = data_directory + "PDB-NR-{}/".format(similarity_threshold)
unsupervised_pdb_directory = unsupervised_database + "pdb/"
unsupervised_clusters_directory = unsupervised_database + "clusters/"
selected_chains_file = unsupervised_database + "selected-chains-{}.pickle".format(seed)
bad_chains_file = unsupervised_database + "bad-chains.pickle"
graph_representation_radii = [11, 15, 19]
complete_graph_neighbor_initial_search_radius = 8

mnist_directory = data_directory + "MNIST/"
docking_directory = data_directory + "Docking/"
zdock_directory = docking_directory + "zdock/"
pairpred_docking_directory = zdock_directory + "pairpred_output/"
zdock_output_directory = zdock_directory + "zdock_output/"
zdock_dbd5_contacts_directory = zdock_output_directory + "output/"
zdock_decoys_bm5_output_directory = zdock_output_directory + "decoys_bm5/"
fast_pl_script = zdock_output_directory + "fast.pl"
saved_models_directory = data_directory + "saved_models/"
wta_saved_models_directory = saved_models_directory + "WTA/"
results_directory = pairpred_home + "results/"
dbd4_nested_cv_pyml_results = results_directory + "nested_cv/dbd4/results/"
dbd4_nested_cv_trained_models = results_directory + "nested_cv/dbd4/trained_models/"
data_tmp_directory = data_directory + "temp/"
iris_dataset = data_directory + "misc/iris.data"
heart_dataset = data_directory + "misc/heart.data"
heart_sparse_dataset = data_directory + "misc/heartSparse.data"

database_directory = data_directory + database
database_pdb_directory = database_directory + "pdb/"
database_kernels_directory = database_directory + "kernels/"
database_pairs_directory = database_directory + "pairs/"
pairs_file_registry = database_pairs_directory + "registry.pickle"
dbd4_pdb_directory = data_directory + "DBD4/pdb/"
dbd5_pdb_directory = data_directory + "DBD5/pdb/"
examples_vectors_directory = database_directory + "example_vectors"
database_interface_directory = database_directory + "interface/"
database_features_directory = database_directory + "features/"
database_representations_directory = database_directory + "representations/"
graph_representations_directory = database_directory + "naa_graph/"
complete_graph_representations_directory = database_directory + "cnn_graph/"

database_features_sequence_directory = database_features_directory + "sequence/"
database_features_profile_directory = database_features_directory + "profile-uniref90/"
# database_features_profile_directory = database_features_directory + "profile/"
database_features_pred_ss_directory = database_features_directory + "pred_ss/"
database_features_residue_depth_directory = database_features_directory + "depth/"
database_features_aa_onehot_directory = database_features_directory + "aa_onehot/"
database_features_stride_directory = database_features_directory + "stride/"
database_features_dssp_directory = database_features_directory + "dssp/"
database_features_hse_directory = database_features_directory + "hse/"
database_features_protrusion_directory = database_features_directory + "protrusion/"
database_features_neighbourhood_directory = database_features_directory + "neighbourhood/"
database_features_patches_directory = database_features_directory + "patches/"
database_features_length_directory = database_features_directory + "length/"
database_features_residue_id_directory = database_features_directory + "residue_ids/"
database_features_residue_full_id_directory = database_features_directory + "residue_full_ids/"

database_features_d2_directory = database_features_directory + "shape_dist/d2/"
database_features_d1_directory = database_features_directory + "shape_dist/d1/"
database_features_d1_multi_directory = database_features_directory + "shape_dist/d1_multi/"
database_features_d2_surface_category_directory = database_features_directory + "shape_dist/d2_surface_category/"
database_features_d1_surface_category_directory = database_features_directory + "shape_dist/d1_surface_category/"
database_features_d2_category_directory = database_features_directory + "shape_dist/d2_category/"
database_features_d1_category_directory = database_features_directory + "shape_dist/d1_category/"
database_features_d2_surface_directory = database_features_directory + "shape_dist/d2_surface/"
database_features_d1_surface_directory = database_features_directory + "shape_dist/d1_surface/"
database_features_d2_surface_atoms_directory = database_features_directory + "shape_dist/d2_surface_atoms/"
database_features_d1_surface_atoms_directory = database_features_directory + "shape_dist/d1_surface_atoms/"
database_features_neighbourhood_residue_dot_product_directory = database_features_directory + "curvature/neighbourhood_residue_dot_product/"
database_features_neighbourhood_surface_dot_product_directory = database_features_directory + "curvature/neighbourhood_surface_dot_product/"
database_features_surface_atoms_curvature_directory = database_features_directory + "curvature/surface_atoms_curvature/"
# corrections / updates:
database_features_neighbourhood_residue_dot_product2_directory = database_features_directory + "curvature/neighbourhood_residue_dot_product2/"
database_features_neighbourhood_surface_dot_product2_directory = database_features_directory + "curvature/neighbourhood_surface_dot_product2/"
database_features_surface_atoms_curvature2_directory = database_features_directory + "curvature/surface_atoms_curvature2/"

database_features_b_value_directory = database_features_directory + "b_value/"
results_prediction_directory = "predictions/"
database_examples_directory = database_directory + "examples_full_id/"
database_examples_by_index_directory = database_directory + "examples_index/"
database_interface_inclusion_examples_directory = database_directory + "examples_interface_inclusion/"
database_interface_pair_examples_directory = database_directory + "examples_interface_pair/"
database_patch_based_examples_directory = database_directory + "patch_based_examples/"
database_examples_directory_id_orderless = database_directory + "examples_full_id_orderless/"
full_id_map_directory = database_directory + "full_id_maps/"
# database_examples_directory = database_directory + "examples/"
tools_directory = pairpred_home + 'tools/'
visualization_directory = results_directory + "/visualizations/"
kernel_vis_directory = visualization_directory + "kernel_vis/"
feature_vis_directory = visualization_directory + "feature_vis/"

dbd5_spreadsheet = data_directory + "original/Table_BM5.xlsx"
categories_pickle_filename = data_directory + "DBD5/categories.pkl"

""" External feature extraction programs folders """
psaia_home = tools_directory + 'psaia'
spinex_home = tools_directory + 'spinex/'
spinex_exe = spinex_home + 'spX.pl'
stride_home = tools_directory + 'stride/'
stride_exe = stride_home + 'stride'
msms_home = tools_directory + 'msms/'
msms_exe = msms_home + 'msms'
msms_pdb2xyz = msms_home + 'pdb_to_xyzr'
psiblast_home = "/s/jawar/j/nobackup/basir/ncbi-blast-2.2.31+/"
psiblast_db_folder = psiblast_home + 'db'
psiblast_executable = psiblast_home + 'bin/psiblast'
dssp_home = tools_directory + "dssp/"
dssp_exe = dssp_home + "dssp"

os.environ["PATH"] = os.environ.get("PATH") + ":" + msms_home + ":" + stride_home + ":" + psaia_home + ":" + dssp_home
os.environ["PYTHONPATH"] = os.environ.get("PYTHONPATH") + ":" + pairpred_home

""" Default parameters """
default_neighbourhood_sigma = 1.0
default_neighbourhood_threshold = 10
default_soft_margin_parameter = 10
patch_based_examples_default_patch_size = 5
default_smooth_soft_margin_sigma = 1.
default_true_positive_patch_threshold = 0
dbd4_ligand_interface_size_median = 24
dbd4_receptor_interface_size_median = 31
default_k_for_k_fold_cv = 5
default_negative_to_positive_ratio = 1
interaction_thr = 6.0
rNH = 25
number_of_layers = 5
neighbourhood_threshold_key = 'n_threshold'
neighbourhood_sigma_key = 'n_sigma'
ss_abbreviations = 'HGIEBTC'
amino_acids = 'ACDEFGHIKLMNPQRSTVWY'
three2one = {'CYS': 'C', 'ASP': 'D', 'SER': 'S', 'GLN': 'Q', 'LYS': 'K',
             'ILE': 'I', 'PRO': 'P', 'THR': 'T', 'PHE': 'F', 'ASN': 'N',
             'GLY': 'G', 'HIS': 'H', 'LEU': 'L', 'ARG': 'R', 'TRP': 'W',
             'ALA': 'A', 'VAL': 'V', 'GLU': 'E', 'TYR': 'Y', 'MET': 'M'}

""" Patch finding default parameters """
default_fixed_neighbours_number = 3
distance_kernel_weight = 0.7
secondary_structure_kernel_weight = 0.1
sequence_kernel_weight = 0.2

distance_kernel_gamma = 0.1
secondary_structure_kernel_gamma = 1.0
sequence_kernel_gamma = 1.0
mean_patch_size = 5

detailed_ss_kernel_keys = {'H': 0, 'B': 1, 'E': 2, 'G': 3, 'I': 4, 'T': 5, 'S': 6, '-': 7}
brief_ss_kernel_keys = {'H': 0, 'B': 2, 'E': 1, 'G': 0, 'I': 0, 'T': 2, 'S': 2, '-': 3, 'nan': 3}
example_id_separator = ":"
complex_classes = {
    "EI": "Enzyme-Inhibitor",
    "ES": "Enzyme-Substrate",
    "ER": "Enzyme-Regulatory/Accessory Chain",
    "A": "Antibody-Antigen",
    "AB": "Antigen-Bound Antibody",
    "OG": "Others, G-protein containing",
    "OR": "Others, Receptor containing",
    "OX": "Others, miscellaneous"
}
brief_ss_kernel = np.array([
    [1.0, 0.0, 0.5, 0.0],
    [0.0, 1.0, 0.5, 0.0],
    [0.5, 0.5, 1.0, 0.0],
    [0.0, 0.0, 0.0, 0.0],
])

# H	alpha-helix
# B	Isolated beta-bridge residue
# E	Strand
# G	3-10 helix
# I	pi-helix
# T	Turn
# S Bend

detailed_ss_kernel = np.array([
    # H    B    E    G    I    T    S    -
    [1.0, 0.0, 0.0, 0.8, 0.8, 0.5, 0.5, 0.0],
    [0.0, 1.0, 0.0, 0.0, 0.0, 0.5, 0.5, 0.0],
    [0.0, 0.0, 1.0, 0.0, 0.0, 0.5, 0.5, 0.0],
    [0.8, 0.0, 0.0, 1.0, 0.8, 0.5, 0.5, 0.0],
    [0.8, 0.0, 0.0, 0.8, 1.0, 0.5, 0.5, 0.0],
    [0.5, 0.5, 0.5, 0.5, 0.5, 1.0, 0.5, 0.0],
    [0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 1.0, 0.0],
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]])

""" gaussian kernels default gammas """
g_profile = 0.5
g_HSAAC = 0.5
g_CX = 0.75
g_exp = 0.5
g_RD = 0.5
g_SD = 0.5
g_p_exp = 3.0
# these values have been tuned using proportion=.05, thresh =0.5
g_surf_curv_comb = 0.44  # tuned on subset of complexes
g_surf_dp = 10.80
g_res_dp = 3.90
# has not been tuned:
g_surf_curv_rmse = 0.5
g_surf_curv = 0.5

### corrected/updated features:
g_surf_curv_comb2 = 7.43  # tuned on subset of complexes
g_surf_dp2 = 10.26  # tuned on subset of complexes
g_res_dp2 = 7.47  # tuned on subset of complexes
# has not been tuned:
g_surf_curv_rmse2 = 0.5
g_surf_curv2 = 0.5

""" shape distribution default parameters """
sd_def_number_of_bins = 30  # number of distribution bins
sd_def_radius = 25  # neighbourhood radius for considering the shape
sd_def_number_of_samples = -1  # -1 means to consider all the atoms in the neighbourhood ( no sampling )
sd_def_seed = seed  # random choices seed
sd_def_rASA_thresh = .5  # default rASA threshold for considering a residue as a surface residue
sd_def_levels = 4

""" curvature default parameters """
curv_def_number_of_bins = 9  # number of distribution bins
curv_def_seed = seed  # random choices seed (no randomness currently)

## Dot product of nearby residues ##
curv_def_dot_prod_neighbour_residue_radius = 10  # neighbourhood radius for computing dot product between adjacent residues (determines which residues are neighbours)
curv_def_dot_prod_neighbour_surface_radius = 5  # neighbourhood radius for computing dot product between adjacent residues (determines which surface points are in neighbourhood, which are used to generate normals for residues)

## Dot product of nearby surface normals ##
curv_def_dot_prod_neighbour_surface_radius_inner = 4  # neighbourhood radius for computing dot product between residue and nearby surface (determines which surface points define the normal for a residue)
curv_def_dot_prod_neighbour_surface_radius_outer = 8  # neighbourhood radius for computing dot product between residue and nearby surface (determines which surface points should be used for the dot product distribution)
curv_def_dot_prod_cone_angle = np.pi / 4  # angle of cone projecting out from residue which selects surface points for dot product comparison (determines which surface points should be used for the dot product distribution)

## Fit an analytic surface to the nearby msms surface and calculate curvature ##
# curv_def_surf_fit_radii = [2, 4, 6, 8, 10, 12, 14, 16] # neighbourhood radius for fitting a surface to get curvature (angstroms) (must be increasing list)
curv_def_surf_fit_radii = [4, 6, 8,
                           10]  # neighbourhood radius for fitting a surface to get curvature (angstroms) (must be increasing list)
curv_def_iters = 500  # number of fixed point iterations for fitting a surface to the surface atoms
curv_def_conv_thresh = 0.0001  # threshold for % change when iterating to fit a surface
curv_def_surface_type = 'sphere'  # type of surface to fit to the surface atoms which is used to compute a curvature
curv_def_min_num_neighbours = 20  # minimum number of surface points in the n'hood before attempting to fit a surface

""" creating folders in case they do not exist """
leaf_directories = [data_directory,
                    results_directory,
                    database_pdb_directory,
                    database_interface_directory,
                    database_features_directory,
                    database_features_sequence_directory,
                    database_features_profile_directory,
                    database_features_residue_depth_directory,
                    database_examples_directory,
                    database_patch_based_examples_directory,
                    database_features_stride_directory,
                    database_features_hse_directory,
                    database_features_neighbourhood_directory,
                    database_features_protrusion_directory,
                    database_features_d2_directory,
                    database_features_d1_directory,
                    database_features_d2_category_directory,
                    database_features_d1_category_directory,
                    database_features_d2_surface_directory,
                    database_features_d1_surface_directory,
                    database_features_d2_surface_atoms_directory,
                    database_features_d1_surface_atoms_directory,
                    database_features_b_value_directory,
                    database_pairs_directory,
                    data_tmp_directory,
                    database_features_dssp_directory,
                    database_features_patches_directory,
                    database_features_length_directory,
                    database_features_residue_id_directory,
                    database_features_pred_ss_directory,
                    database_features_d1_multi_directory,
                    database_features_residue_full_id_directory,
                    database_features_neighbourhood_residue_dot_product_directory,
                    database_features_neighbourhood_residue_dot_product2_directory,
                    database_features_neighbourhood_surface_dot_product_directory,
                    database_features_neighbourhood_surface_dot_product2_directory,
                    database_features_surface_atoms_curvature_directory,
                    database_features_surface_atoms_curvature2_directory,
                    wta_saved_models_directory,
                    database_representations_directory,
                    graph_representations_directory,
                    full_id_map_directory,
                    visualization_directory,
                    kernel_vis_directory,
                    feature_vis_directory,
                    database_examples_directory_id_orderless,
                    dbd4_nested_cv_pyml_results,
                    dbd4_nested_cv_trained_models,
                    database_interface_pair_examples_directory,
                    database_interface_inclusion_examples_directory,
                    database_features_aa_onehot_directory,
                    database_kernels_directory,
                    examples_vectors_directory,
                    database_examples_by_index_directory
                    ]

for directory in leaf_directories:
    if not path.exists(directory):
        os.makedirs(directory)

verbose = True
