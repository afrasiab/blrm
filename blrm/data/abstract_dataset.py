from abc import ABCMeta, abstractmethod

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AbstractDatabase(object):
    __metaclass__ = ABCMeta
    name = ""

    @abstractmethod
    def get_pyml_cv_from_folds(self, features, complexes, n_folds=5, **kwargs):
        pass
