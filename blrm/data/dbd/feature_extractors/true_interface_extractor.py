from blrm.configuration import database_interface_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


# noinspection PyMethodMayBeStatic
class TrueInterfaceExtractor(AbstractFeatureExtractor):
    def __init__(self, complex_object_model, **kwargs):
        super(TrueInterfaceExtractor, self).__init__(None)
        self.complex = complex_object_model

    def extract(self):
        # Example extractor also extracts the true interface
        pass

    def _get_file_name(self, protein_name):
        return database_interface_directory + protein_name + ".npy"

    def get_file_names(self, complex_name):
        base_name = database_interface_directory + complex_name
        return base_name + "_l_b.npy", base_name + "_r_b.npy", base_name + "_l_u.npy", base_name + "_r_u.npy"
