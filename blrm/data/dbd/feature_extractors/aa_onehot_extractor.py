import os
import numpy as np

from Bio.PDB import ResidueDepth
from blrm.configuration import three2one

from blrm.configuration import database_features_aa_onehot_directory, verbose, amino_acids
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class AAOneHotExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return database_features_aa_onehot_directory + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(AAOneHotExtractor, self).__init__(protein)

    def extract(self):
        aa_onehot_file = self._get_file_name(self._protein.name)

        if not os.path.exists(aa_onehot_file):
            aa_dict = dict(zip(amino_acids, range(len(amino_acids))))
            if verbose:
                Print.print_info("\t Computing Amino Acid one hot rep. for {0}".format(self._protein.name))
            aa_array = np.zeros((len(self._protein.residues), 21))
            for (i, res) in enumerate(self._protein.biopython_residues):
                if res.resname in three2one.keys():
                    aa_array[i, aa_dict[three2one[res.resname]]] = 1
                else:
                    aa_array[i, 20] = 1

            np.save(aa_onehot_file, aa_array)
