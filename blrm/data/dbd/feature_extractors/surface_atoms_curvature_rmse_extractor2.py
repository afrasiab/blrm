import numpy as np
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base2 import SurfaceAtomsCurvatureExtractorBase2

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'


class SurfaceAtomsCurvatureRMSEExtractor2(SurfaceAtomsCurvatureExtractorBase2):

    def load(self, protein_name):
        _, rmse_filen, _ = super(SurfaceAtomsCurvatureRMSEExtractor2, self)._get_file_name(protein_name)
        rmse_features = np.load(rmse_filen)
        return rmse_features
