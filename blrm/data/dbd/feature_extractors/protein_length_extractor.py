import os
from blrm.configuration import database_features_length_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print
import numpy as np

__author__ = 'basir'


class ProteinLengthExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return database_features_length_directory + protein_name + ".npy"

    def load(self, protein_name):
        return int(np.load(self._get_file_name(protein_name)))

    def extract(self):
        size_file_name = self._get_file_name(self._protein.name)
        if not os.path.exists(size_file_name):
            if verbose:
                Print.print_info("\t Computing protein length for {0}".format(self._protein.name))
            np.save(size_file_name, len(self._protein.residues))
