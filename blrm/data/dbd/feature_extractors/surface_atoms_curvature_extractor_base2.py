import os
from abc import abstractmethod
import pickle

import numpy as np

from numpy.random.mtrand import seed
from sklearn.preprocessing import normalize


from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.configuration import database_features_surface_atoms_curvature2_directory, verbose, \
    curv_def_seed, curv_def_surf_fit_radii, curv_def_iters, curv_def_surface_type, \
    curv_def_min_num_neighbours, curv_def_conv_thresh
from blrm.data.tools_interface.msms import get_surface_atoms
from blrm.utils.pretty_print import Print


__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'


class SurfaceAtomsCurvatureExtractorBase2(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        directory = database_features_surface_atoms_curvature2_directory + self.get_directory() + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        curv_filen = directory + protein_name + "_curv.npy"
        rmse_filen = directory + protein_name + "_rmse.npy"
        #fit_surf_filen = directory + protein_name + "_fit_surf.dump"
        #res_filen = directory + protein_name + "_residue_loc.npy"
        results_filen = directory + protein_name + "_results.pkl"
        return curv_filen, rmse_filen, results_filen

    def get_directory(self):
        radii = "-".join([repr(r) for r in self.neighbourhood_radii])
        return "{0}-{1}".format(radii, self.surface_type)

    def __init__(self, protein, **kwargs):
        if 'neighbourhood_radii' not in kwargs:
            kwargs['neighbourhood_radii'] = curv_def_surf_fit_radii
        if 'surface_type' not in kwargs:
            kwargs['surface_type'] = curv_def_surface_type
        if 'seed' not in kwargs:
            kwargs['seed'] = curv_def_seed
        if 'number_of_iters' not in kwargs:
            kwargs['number_of_iters'] = curv_def_iters
        if 'min_number_neighbours' not in kwargs:
            kwargs['min_number_neighbours'] = curv_def_min_num_neighbours
        if 'conv_thresh' not in kwargs:
            kwargs['conv_thresh'] = curv_def_conv_thresh
        self.neighbourhood_radii = kwargs['neighbourhood_radii']
        self.surface_type = kwargs['surface_type']
        self.iters = kwargs['number_of_iters']
        self.min_number_neighbours = kwargs['min_number_neighbours']
        self.conv_thresh = kwargs["conv_thresh"]
        self.protein = protein
        self.seed = kwargs['seed']

    def extract(self):
        curv_filen, rmse_filen, results_filen = self._get_file_name(self.protein.name)
        if not os.path.exists(curv_filen):
            if verbose:
                Print.print_info("\t Computing surface atoms curvature for {0}".format(self.protein.name))
            # get surface atoms of protein (use a mesh of the ses instead?)
            surface, _ = get_surface_atoms(self.protein.file_name)

            # for each residue...
            curv_features = np.zeros((len(self.protein.residues), len(self.neighbourhood_radii)))
            rmse_features = np.zeros((len(self.protein.residues), len(self.neighbourhood_radii)))
            results = [None] * len(self.protein.residues)
            for i, residue in enumerate(self.protein.residues):
                result ={}
                result["residue_loc"] = residue.center
                result["fit_surfaces"]=[]
                # isolate surface points around residue, do for each neighbourhood residue
                for nhood_radius in self.neighbourhood_radii:
                    distances = np.linalg.norm(surface - residue.center,None,1)
                    neighbourhood_indices = distances < nhood_radius
                    neighbourhood_surface = surface[neighbourhood_indices]
                    # fit smooth surface to the surface atoms
                    if self.surface_type == "sphere":
                        # need a minimum number of points in order to fit a surface
                        if len(neighbourhood_surface) > self.min_number_neighbours:
                            # fit a sphere to the data
                            radius, sph_center = self.sphere_fit(neighbourhood_surface.T)
                            
                            # calculate the local curvature
                            res_2_surf_centroid = np.mean(neighbourhood_surface, axis=0) - residue.center
                            surf_centroid_2_sph_center = sph_center - np.mean(neighbourhood_surface, axis=0)
                            # if the sphere surface is "behind" the surface, then concave in
                            if np.dot(res_2_surf_centroid, surf_centroid_2_sph_center) < 0:
                                # concave "down" (in)
                                dir_radius = - radius
                                curvature = - 1/radius
                            else:
                                # concave "up" (out)
                                dir_radius = radius
                                curvature = 1/radius
                            RMSE = self.rmse(sph_center, radius, neighbourhood_surface.T)
                        else:
                            dir_radius = 0
                            radius = 0
                            curvature = 0
                            sph_center = None
                            RMSE = 0 #should not hard code?
                        result["fit_surfaces"].append({"type":"sph","radius":radius,"center":sph_center,\
                                                       "dir_radius":dir_radius,"curvature":curvature,"rmse":RMSE })
                    else: 
                        print_error("\t Surface type {0} is not supported".format(self.surface_type))
                results[i] = result
                #curv_features[i,:] = np.array([fit_surface["curvature"] for fit_surface in result["fit_surfaces"]])
                curv_features[i,:] = np.array([fit_surface["dir_radius"]/nhood_radius
                                               for fit_surface, nhood_radius in zip(result["fit_surfaces"], self.neighbourhood_radii)])
                rmse_features[i,:] =   np.array([fit_surface["rmse"]      for fit_surface in result["fit_surfaces"]])
            pickle.dump((self.neighbourhood_radii, results), open(results_filen,'wb'))
            np.save(curv_filen, curv_features)
            np.save(rmse_filen, rmse_features)



    ''' Not used, potentially for fitting a paraboloid to the data '''
    def quadric_data_3d(self, data):
        quadric_data = np.zeros(len(data), 10)
        for i, (x, y, z) in enumerate(data):
            quadric_data[i,:] = np.array([x*x, y*y, z*z, x*y, y*z, x*z, x, y, z, 1])
        return quadric_data

    ''' Fits a sphere to the data '''
    ''' Assumes data is 3xn '''
    def sphere_fit(self, data):
        iters = self.iters
        center = np.mean(data, 1)
        for i in range(iters):
            L = self._L(center, data)
            L_bar_abc = self._L_bar_abc(center, data, L)
            center_new = np.mean(data,1) + np.mean(L) * L_bar_abc
            # print repr(i) + ":" + repr(np.linalg.norm(center_new - center)/np.linalg.norm(center))
            if np.linalg.norm(center_new-center)/np.linalg.norm(center) < self.conv_thresh:
                center = center_new
                break
            center = center_new
        r = np.mean(self._L(center, data))
        return r, center

    ''' Calculate L, used in sphere fitting '''
    ''' Assumes center is 3x1 np array and data is 3xn np array '''
    ''' A is [[1,0,0][0,1,0][0,0,1][-x0,-y0,-z0]] and L_i is computed as data_i^T * A^T * A * data_i '''
    ''' L is of shape 1xn? '''
    A = np.concatenate((np.identity(3), np.zeros((1,3))),0)
    def _L(self, center, data):
        self.A[3:] = -1 * center
        data_aug = np.concatenate((data, np.ones((1, data.shape[1]))),0) # add ones to end of data
        AAT = np.dot(self.A, self.A.T)
        LHS = np.dot(data_aug.T, AAT)
        return np.sqrt(np.einsum('ij,ij->i', LHS, data_aug.T))
        #return np.einsum('ij,ij->i', LHS, data_aug.T)/data.shape[1]
        #return np.sum(LHS * data_aug.T, axis=1)/data.shape[1] # equivalent but slower, shown to better illustrate the operation

    ''' Calculate L_abc, used in sphere fitting ''' 
    ''' Assumes center is 3x1 np array, data is 3xn np array, L is 1xn np array '''
    ''' L_bar_abc is 3x1? '''
    def _L_bar_abc(self, center, data, L):
        return np.mean(((center.reshape(3,1) - data)/L),1)

    def rmse(self, center, radius, data):
        return np.sqrt(np.mean((radius - self._L(center, data))**2))

    def load(self, protein_name, scale=False):
        curv_filen, rmse_filen, _ = self._get_file_name(protein_name)
        curv_features = np.load(curv_filen)
        rmse_features = np.load(rmse_filen)
        return np.hstack([curv_features, rmse_features])

def main():
    from matplotlib import pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D

    r = 20; x0 = 1; y0 = 3; z0 = -4
    n = 10
    u = np.linspace(0, 2 * np.pi, n)
    v = np.linspace(0, np.pi, n)
    
    x_noise = np.random.normal(size=n)
    y_noise = np.random.normal(size=n)
    z_noise = np.random.normal(size=n)


    x = np.array(r * np.sin(v) * np.cos(u) + x0) #+ x_noise
    y = np.array(r * np.sin(v) * np.sin(u) + y0) #+ y_noise
    z = np.array(r * np.cos(v) + z0) #+ z_noise

    
    test_data = np.vstack((x,y,z))
    ext = SurfaceAtomsCurvatureExtractorBase(None)
    radius, center = ext.sphere_fit(test_data)
    print "real radius:{}".format(r)
    print "fit radius:{}".format(radius)
    print "real center:{}".format((x0,y0,z0))
    print "fit center:{}".format(center)

    # plot 
    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    ax.scatter(x0, y0, z0, c='b', marker='o')
    ax.scatter(center[0], center[1], center[2], c='g', marker='o')
    
    ax.scatter(x, y, z, c='r', marker='o')

    
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)

    x_surf = radius * np.outer(np.cos(u), np.sin(v)) + center[0]
    y_surf = radius * np.outer(np.sin(u), np.sin(v)) + center[1]
    z_surf = radius * np.outer(np.ones(np.size(u)), np.cos(v)) + center[2]
    ax.plot_surface(x_surf, y_surf, z_surf, rstride = 4, cstride = 4, color = 'g', alpha=0.1)

    plt.show()

if __name__ == "__main__":

    main()

