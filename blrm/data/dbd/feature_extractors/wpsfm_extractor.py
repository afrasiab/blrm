import numpy as np

from sklearn.preprocessing import normalize

from blrm.data.dbd.feature_extractors.profile_extractor import ProfileExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class WPSFMExtractor(ProfileExtractor):

    def _get_file_name(self, protein_name):
        return super(WPSFMExtractor, self)._get_file_name(protein_name)

    def extract(self):
        return super(WPSFMExtractor, self).extract()

    def load(self, protein_name):
        _, _, wpsfm_file = self._get_file_name(protein_name)
        wpsfm = np.load(wpsfm_file)
        # return normalize(wpsfm[:, 110-60:110+60])
        return normalize(wpsfm)
        # return wpsfm[:, 110-10:110+10]
