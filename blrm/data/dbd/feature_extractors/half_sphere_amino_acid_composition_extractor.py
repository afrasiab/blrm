import os
import numpy as np
from Bio.PDB.Polypeptide import standard_aa_names
from Bio.PDB import is_aa, Vector

from blrm.configuration import database_features_hse_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.dbd.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class HalfSphereAminoAcidCompositionExtractor(AbstractFeatureExtractor):

    def _get_file_name(self, protein_name):
        return database_features_hse_directory + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(HalfSphereAminoAcidCompositionExtractor, self).__init__(protein)
        self._residue_index_table = {}
        for index, amino_acid in enumerate(standard_aa_names):
            self._residue_index_table[amino_acid] = index

    def extract(self):
        number_of_amino_acids = len(standard_aa_names)
        neighbourhood = ResidueNeighbourhoodExtractor(None).load(self._protein.name)
        hse_file = self._get_file_name(self._protein.name)
        if not os.path.exists(hse_file):
            if verbose:
                Print.print_info("\t Computing half sphere amino acid composition for {0}".format(self._protein.name))
            ResidueNeighbourhoodExtractor(self._protein).extract()
            number_of_residues = len(self._protein.biopython_residues)
            un = np.zeros(number_of_residues)
            dn = np.zeros(number_of_residues)
            uc = np.zeros((number_of_amino_acids, number_of_residues))
            dc = np.zeros((number_of_amino_acids, number_of_residues))
            for i, residue in enumerate(self._protein.biopython_residues):
                u = self.get_side_chain_vector(residue)
                if u is None:
                    un[i] = np.nan
                    dn[i] = np.nan
                    uc[:, i] = np.nan
                    dc[:, i] = np.nan
                else:
                    residue_index = self._residue_index_table[residue.get_resname()]
                    uc[residue_index, i] += 1
                    dc[residue_index, i] += 1
                    neighbours_indices = neighbourhood[i]
                    # print neighbours_indices
                    for neighbour_index in neighbours_indices:
                        if neighbour_index == -1:
                            break
                        neighbour_residue = self._protein.biopython_residues[int(neighbour_index)]
                        if is_aa(neighbour_residue) and neighbour_residue.has_id('CA'):
                            neighbour_vector = neighbour_residue['CA'].get_vector()
                            residue_index = self._residue_index_table[neighbour_residue.get_resname()]
                            if u[1].angle((neighbour_vector - u[0])) < np.pi / 2.0:
                                un[i] += 1
                                uc[residue_index, i] += 1
                            else:
                                dn[i] += 1
                                dc[residue_index, i] += 1
            uc = (uc / (1.0 + un)).T
            dc = (dc / (1.0 + dn)).T
            hse_array = np.hstack((uc, dc))
            np.save(hse_file, hse_array)

    @staticmethod
    def get_side_chain_vector(residue):
        """
        Find the average of the unit vectors to different atoms in the side chain
        from the c-alpha atom. For glycine the average of the N-Ca and C-Ca is
        used.
        Returns (C-alpha coordinate vector, side chain unit vector) for residue r
        """
        u = None
        gly = 0
        if is_aa(residue) and residue.has_id('CA'):
            ca = residue['CA'].get_coord()
            dv = np.array([ak.get_coord() for ak in residue.get_unpacked_list()[4:]])
            if len(dv) < 1:
                if residue.has_id('N') and residue.has_id('C'):
                    dv = [residue['C'].get_coord(), residue['N'].get_coord()]
                    dv = np.array(dv)
                    gly = 1
                else:
                    return None
            dv = dv - ca
            if gly:
                dv = -dv
            n = np.sum(abs(dv) ** 2, axis=-1) ** (1. / 2)
            v = dv / n[:, np.newaxis]
            u = (Vector(ca), Vector(v.mean(axis=0)))
        return u
