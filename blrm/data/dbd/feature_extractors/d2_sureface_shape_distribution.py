import os
import numpy as np
from Bio.PDB import NeighborSearch

from blrm.configuration import sd_def_rASA_thresh, database_features_d2_surface_directory, verbose
from blrm.data.dbd.feature_extractors.d2_base_shape_distribution import D2BaseShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D2SurfaceShapeDistributionExtractor(D2BaseShapeDistributionExtractor):
    def _get_file_name(self, protein_name):
        return database_features_d2_surface_directory + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(D2SurfaceShapeDistributionExtractor, self).__init__(protein, **kwargs)
        if 'rASA' not in kwargs:
            kwargs['rASA'] = sd_def_rASA_thresh
        self.rASA_threshold = kwargs['rASA']

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing D2 surface residue shape distribution for {0}".format(self._protein.name))
            StrideSecondaryStructureExtractor(self._protein).extract()
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            rsa = StrideSecondaryStructureExtractor(None).load(self._protein.name)
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                nearby_atoms = []
                temp_nearby_atoms = neighbour_search.search(residue.center, self.radius/2., "A")
                for atom in temp_nearby_atoms:
                    if atom.parent not in self._protein.biopython_residues:
                        continue
                    residues_index = self._protein.biopython_residues.index(atom.parent)
                    if rsa[residues_index] >= self.rASA_threshold:
                        nearby_atoms.append(atom)
                distributions[i, :] = D2BaseShapeDistributionExtractor._compute_shape_distribution(self, nearby_atoms)
            np.save(shape_dist_file, distributions)
