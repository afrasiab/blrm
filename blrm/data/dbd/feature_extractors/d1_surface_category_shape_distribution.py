import os
from random import seed
import numpy as np
from Bio.PDB import NeighborSearch

from blrm.configuration import verbose, sd_def_rASA_thresh, database_features_d1_surface_category_directory
from blrm.data.dbd.feature_extractors.d1_category_shape_distribution import D1CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D1SurfaceCategoryShapeDistributionExtractor(D1CategoryShapeDistributionExtractor):
    def __init__(self, protein, **kwargs):
        super(D1SurfaceCategoryShapeDistributionExtractor, self).__init__(protein, **kwargs)
        if 'rASA' not in kwargs:
            kwargs['rASA'] = sd_def_rASA_thresh
        self.rASA_threshold = kwargs['rASA']

    def _get_file_name(self, protein_name):
        directory = database_features_d1_surface_category_directory + self._get_directory() + "-{0}/".format(
            self.rASA_threshold)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing surface D1 category shape distribution for {0}".format(self._protein.name))
            neighbour_search = NeighborSearch(self._protein.atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins * 3))
            rsa = StrideSecondaryStructureExtractor(None).load(self._protein.name)
            for i in range(len(self._protein.residues)):
                nearby_atoms = []
                center = self._protein.residues[i].center
                atoms = neighbour_search.search(center, self.radius, "A")
                for atom in atoms:
                    if atom.parent not in self._protein.biopython_residues:
                        continue
                    residues_index = self._protein.biopython_residues.index(atom.parent)
                    if rsa[residues_index] >= self.rASA_threshold:
                        nearby_atoms.append(atom)
                distributions[i, :] = self._compute_category_distribution(nearby_atoms, center)
            np.save(shape_dist_file, distributions)
