import os
from random import seed
import numpy as np
from Bio.PDB import NeighborSearch

from blrm.configuration import verbose, database_features_d1_category_directory
from blrm.data.dbd.feature_extractors.base_shape_distribution import ResidueCategories, categories
from blrm.data.dbd.feature_extractors.d1_base_shape_distribution import D1BaseShapeDistributionExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D1CategoryShapeDistributionExtractor(D1BaseShapeDistributionExtractor):
    def __init__(self, protein, **kwargs):
        super(D1CategoryShapeDistributionExtractor, self).__init__(protein, **kwargs)

    def _get_file_name(self, protein_name):
        directory = database_features_d1_category_directory + self._get_directory() + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing plain D1 category shape distribution for {0}".format(self._protein.name))
            neighbour_search = NeighborSearch(self._protein.atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins * 3))
            for i in range(len(self._protein.residues)):
                center = self._protein.residues[i].center
                atoms = neighbour_search.search(center, self.radius, "A")
                distributions[i, :] = self._compute_category_distribution(atoms, center)
            np.save(shape_dist_file, distributions)

    def _compute_category_distribution(self, nearby_atoms, center):
        polar_charged = []
        hydrophobic_charged = []
        hydrophobic_polar = []
        all_cat = [polar_charged, hydrophobic_charged, hydrophobic_polar]
        for atom in nearby_atoms:
            res = atom.parent
            if res.resname not in categories:
                continue
            if categories[res.resname] == ResidueCategories.Polar:
                polar_charged.append(atom)
                hydrophobic_polar.append(atom)
            if categories[res.resname] == ResidueCategories.Hydrophobic:
                hydrophobic_charged.append(atom)
                hydrophobic_polar.append(atom)
            if categories[res.resname] == ResidueCategories.Charged:
                polar_charged.append(atom)
                hydrophobic_charged.append(atom)
        final_distribution = []
        for category in all_cat:
            dist = D1BaseShapeDistributionExtractor._compute_shape_distribution(self, category, center)
            final_distribution.extend(list(dist))
        return np.array(final_distribution) / float(len(all_cat))
