import os
from scipy.spatial.distance import cdist
import numpy as np

from blrm.configuration import neighbourhood_threshold_key, neighbourhood_sigma_key, \
    database_features_neighbourhood_directory, verbose, default_fixed_neighbours_number
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class FixedNeighbourhoodExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        if not os.path.exists(database_features_neighbourhood_directory + str(self.neighbours_number)):
            os.makedirs(database_features_neighbourhood_directory + str(self.neighbours_number))
        return database_features_neighbourhood_directory + str(self.neighbours_number) + "/" + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(FixedNeighbourhoodExtractor, self).__init__(protein)
        if 'neighbours_number' not in kwargs:
            kwargs['neighbours_number'] = default_fixed_neighbours_number
        self.neighbours_number = kwargs['neighbours_number']

    def extract(self):
        fixed_neighbourhood_file = self._get_file_name(self._protein.name)
        if not os.path.exists(fixed_neighbourhood_file):
            if verbose:
                Print.print_info("\t Computing fixed neighbourhood for {0}".format(self._protein.name))
            neighbourhood_array = np.zeros((len(self._protein.residues), self.neighbours_number))
            for i, query_residue in enumerate(self._protein.residues):
                distances = []
                for j, neighbour_residue in enumerate(self._protein.residues):
                    if i==j:
                        continue
                    distance = cdist(query_residue.get_coordinates(), neighbour_residue.get_coordinates()).min()
                    distances.append((j, distance))
                distances_array = np.array(distances)
                distances_array = distances_array[(distances_array[:, 1]).argsort()]
                neighbourhood_array[i, :] = distances_array[:self.neighbours_number, 0]
            np.save(fixed_neighbourhood_file, neighbourhood_array)

    def get_neighbourhood_number(self):
        return self.neighbours_number
