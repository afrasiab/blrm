import os
from scipy.spatial.distance import cdist
import numpy as np

from blrm.configuration import neighbourhood_threshold_key, neighbourhood_sigma_key, \
    database_features_neighbourhood_directory, verbose, default_neighbourhood_threshold, default_neighbourhood_sigma
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ResidueNeighbourhoodExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        dirs = "{}/{}/".format(database_features_neighbourhood_directory, self._threshold)
        if not os.path.exists(dirs):
            os.makedirs(dirs)
        return dirs + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(ResidueNeighbourhoodExtractor, self).__init__(protein)
        if neighbourhood_threshold_key not in kwargs:
            kwargs[neighbourhood_threshold_key] = default_neighbourhood_threshold
        if neighbourhood_sigma_key not in kwargs:
            kwargs[neighbourhood_sigma_key] = default_neighbourhood_sigma
        self._sigma = kwargs[neighbourhood_sigma_key]
        self._threshold = kwargs[neighbourhood_threshold_key]

    def extract(self):
        residue_neighbourhood_file = self._get_file_name(self._protein.name)
        if not os.path.exists(residue_neighbourhood_file):
            if verbose:
                Print.print_info("\t Computing residue neighbourhood for {0}".format(self._protein.name))
            neighbourhood = []
            for i, query_residue in enumerate(self._protein.residues):
                neighbourhood.append([])
                distances = []
                for j, neighbour_residue in enumerate(self._protein.residues):
                    if i == j:
                        continue
                    distance = cdist(query_residue.center.reshape(1, -1), neighbour_residue.center.reshape(1, -1))[0, 0]
                    # similarity = np.exp(-(distance ** 2) / self._sigma)
                    if distance <= self._threshold:
                        neighbourhood[-1].append(j)
                        distances.append(distance)
                neighbourhood[-1] = [x for x,_ in sorted(zip(neighbourhood[-1],distances))]
            np.save(residue_neighbourhood_file, np.array(neighbourhood))
