import os
import numpy as np
from Bio.PDB import DSSP

from blrm.configuration import verbose, database_features_dssp_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DSSPSecondaryStructureExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        base_name = database_features_dssp_directory + protein_name
        names = [base_name + extension for extension in ["_ss.npy", "_asa.npy", "_rasa.npy", "_phi.npy", "_psi.npy"]]
        return names

    def extract(self):
        names = self._get_file_name(self._protein.name)
        all_files_exist = True
        for name in names:
            if not os.path.exists(name):
                all_files_exist = False
                break
        if not all_files_exist:
            if verbose:
                Print.print_info("... running DSSP for protein " + self._protein.name)

            dssp = DSSP(self._protein.structure[0], self._protein.file_name)
            n = len(self._protein.residues)
            ss_l, asa_l, rasa_l, phi_l, psi_l = [], [], [], [], []
            for (i, res) in enumerate(self._protein.biopython_residues):
                (_, _, cid, rid) = res.get_full_id()
                key = (cid, rid)
                if key in dssp:
                    dssp_array = dssp[key]
                    ss_l.append(dssp_array[1])
                    asa_l.append(dssp_array[2])
                    rasa_l.append(dssp_array[3])
                    phi_l.append(dssp_array[4])
                    psi_l.append(dssp_array[5])
                else:
                    ss_l.append('-')
                    asa_l.append(np.nan)
                    rasa_l.append(np.nan)
                    phi_l.append(np.nan)
                    psi_l.append(np.nan)
            arrays = [np.array(ss_l), np.array(asa_l), np.array(rasa_l), np.array(phi_l), np.array(psi_l)]
            for i, array in enumerate(arrays):
                np.save(names[i], array)

    def load(self, protein_name):
        all_arrays = []
        for name in self._get_file_name(protein_name):
            all_arrays.append(np.load(name))
        return all_arrays
