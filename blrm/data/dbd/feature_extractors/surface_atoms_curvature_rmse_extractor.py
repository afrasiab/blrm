import numpy as np
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base import SurfaceAtomsCurvatureExtractorBase

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'


class SurfaceAtomsCurvatureRMSEExtractor(SurfaceAtomsCurvatureExtractorBase):

    def load(self, protein_name):
        _, rmse_filen, _ = super(SurfaceAtomsCurvatureRMSEExtractor, self)._get_file_name(protein_name)
        rmse_features = np.load(rmse_filen)
        return rmse_features
