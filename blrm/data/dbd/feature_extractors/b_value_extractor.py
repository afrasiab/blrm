import os
import numpy as np

from blrm.configuration import database_features_b_value_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class BValueExtractor(AbstractFeatureExtractor):

    def _get_file_name(self, protein_name):
        return database_features_b_value_directory + protein_name + ".npy"

    def extract(self):
        if not os.path.exists(self._get_file_name(self._protein.name)):
            if verbose:
                Print.print_info("\t Computing B-factor for {0}".format(self._protein.name))
            b_factor_array = np.zeros(len(self._protein.residues))
            for (index, residue) in enumerate(self._protein.biopython_residues):
                b_factor_array[index] = max([atom.get_bfactor() for atom in residue])
            np.save(self._get_file_name(self._protein.name), self._normalize(b_factor_array))
