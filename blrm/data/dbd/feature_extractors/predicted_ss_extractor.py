from multiprocessing import cpu_count
import os
import numpy as np

from blrm.configuration import psiblast_executable, database_features_sequence_directory, verbose, \
    database_features_pred_ss_directory, database_features_profile_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.tools_interface import spinex
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class PredictedSSExtractor(AbstractFeatureExtractor):
    def __init__(self, protein, **kwargs):
        super(PredictedSSExtractor, self).__init__(protein)
        self.pred_ss_file = None

    def _get_file_name(self, protein_name):
        ss_file = database_features_pred_ss_directory + protein_name + "_ss.npy"
        fasta_file = database_features_sequence_directory + protein_name + ".fasta"
        mat_file = database_features_profile_directory + protein_name + ".mat"
        return fasta_file, ss_file, mat_file

    def load(self, protein_name):
        _, ss_file, _ = self._get_file_name(protein_name)
        pred_ss = np.load(ss_file)
        return pred_ss

    def extract(self):
        self.__save_sequences_to_fasta()
        self.__compute_pred_ss()

    def __save_sequences_to_fasta(self):
        sequence = self._protein.sequence
        self.fasta_file = self._get_file_name(self._protein.name)[0]
        if os.path.exists(self.fasta_file):
            return
        f = open(self.fasta_file, "w+")
        f.write(">{0}\n".format(self._protein.name))
        f.write(sequence + "\n")
        f.close()

    def __compute_pred_ss(self, db='nr', niter=3):
        _, self.pred_ss_file, matrix_file = self._get_file_name(self._protein.name)
        if not os.path.exists(self.pred_ss_file):
            if verbose:
                Print.print_info("\t Computing predicted ss for {0}".format(self._protein.name))
            if not os.path.exists(matrix_file):
                Print.print_info_nn("\t... processing protein {0} ...    ".format(self._protein.name))
                command = "{4} " \
                          "-query {0} " \
                          "-db {1} " \
                          "-out {2}.psi.txt " \
                          "-num_iterations {3} " \
                          "-num_threads {5} " \
                          "-out_ascii_pssm {2}.mat" \
                    .format(self.fasta_file, db, matrix_file[:-4], niter, psiblast_executable, cpu_count())
                Print.print_info(command)
                error_code = os.system(command)
                if error_code == 0:
                    Print.print_info('Successful!')
                else:
                    Print.print_error('Failed with error code {0}'.format(error_code))
                    raise ValueError('Failed with error code {0}'.format(error_code))
            (asa, rasa, _, _, _) = spinex.pssm_to_spinex(matrix_file)
            np.save(self.pred_ss_file, rasa.reshape((rasa.shape[0], 1)))
