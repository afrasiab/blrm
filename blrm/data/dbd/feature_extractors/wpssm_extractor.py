import numpy as np

from sklearn.preprocessing import normalize

from blrm.data.dbd.feature_extractors.profile_extractor import ProfileExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class WPSSMExtractor(ProfileExtractor):

    def _get_file_name(self, protein_name):
        return super(WPSSMExtractor, self)._get_file_name(protein_name)

    def extract(self):
        return super(WPSSMExtractor, self).extract()

    def load(self, protein_name):
        _, wpssm_file, _ = self._get_file_name(protein_name)
        wpssm = np.load(wpssm_file)
        # return normalize(wpssm[:, 110-10:110+10])
        # return normalize(wpssm[:, 110-60:110+60])
        return normalize(wpssm)
        # return wpssmwp
