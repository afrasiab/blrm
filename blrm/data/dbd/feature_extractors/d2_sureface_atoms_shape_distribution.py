import os
import numpy as np
from blrm.data.dbd.feature_extractors.base_shape_distribution import BaseShapeDistributionExtractor
from scipy.spatial.distance import cdist

from numpy.random.mtrand import seed

from blrm.configuration import rNH, verbose, database_features_d2_surface_atoms_directory
from blrm.data.dbd.feature_extractors.d2_base_shape_distribution import D2BaseShapeDistributionExtractor
from blrm.data.tools_interface.msms import get_surface_atoms
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D2SurfaceAtomsShapeDistributionExtractor(BaseShapeDistributionExtractor):

    def __init__(self, protein, **kwargs):
        super(D2SurfaceAtomsShapeDistributionExtractor, self).__init__(protein)
        if 'rNH' not in kwargs:
            kwargs['rNH'] = rNH
        self.rNH = kwargs['rNH']

    def _get_file_name(self, protein_name):
        directory = database_features_d2_surface_atoms_directory + self._get_directory() + "-{0}/".format(self.rNH)
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing D2 surface atom shape distribution for {0}".format(self._protein.name))
            surface, normals = get_surface_atoms(self._protein.file_name)
            distributions = np.zeros((len(self._protein.residues), 2*self.number_of_bins))
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                distributions[i, :] = self.get_distributions(residue.center, surface, normals)
            np.save(shape_dist_file, distributions)

    def get_distributions(self, center, surface, normals):
        distances = cdist(surface, center.reshape((1, center.size))).flatten()
        indices = np.where(distances < self.rNH)[0]
        selected_normals = normals[indices]
        selected_coordinates = surface[indices]

        dist_distribution = self._compute_shape_distribution(selected_coordinates, selected_coordinates)
        norm_distribution = self._compute_normal_distribution(selected_normals, selected_normals)
        return np.hstack((dist_distribution, norm_distribution))
