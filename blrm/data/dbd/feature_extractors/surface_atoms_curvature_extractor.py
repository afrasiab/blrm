import numpy as np
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base import SurfaceAtomsCurvatureExtractorBase


__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'

class SurfaceAtomsCurvatureExtractor(SurfaceAtomsCurvatureExtractorBase):

    def load(self, protein_name):
        curv_filen, _, _ = super(SurfaceAtomsCurvatureExtractor, self)._get_file_name(protein_name)
        curv_features = np.load(curv_filen)
        return curv_features