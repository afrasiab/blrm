import os
import numpy as np
from Bio.PDB import NeighborSearch

from blrm.configuration import verbose, database_features_d2_directory
from blrm.data.dbd.feature_extractors.d2_base_shape_distribution import D2BaseShapeDistributionExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D2PlainShapeDistributionExtractor(D2BaseShapeDistributionExtractor):

    def _get_file_name(self, protein_name):
        directory = database_features_d2_directory + self._get_directory() + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing plain D2 shape distribution for {0}".format(self._protein.name))
            atoms = self._protein.atoms
            neighbour_search = NeighborSearch(atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            for i in range(len(self._protein.residues)):
                nearby_atoms = neighbour_search.search(self._protein.residues[i].center, self.radius/2., "A")
                distributions[i, :] = D2BaseShapeDistributionExtractor._compute_shape_distribution(self, nearby_atoms)
            np.save(shape_dist_file, distributions)
