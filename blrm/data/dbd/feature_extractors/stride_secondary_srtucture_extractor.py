import os
import numpy as np

from blrm.configuration import ss_abbreviations, database_features_stride_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.dbd.tools.stride import stride_dict_from_pdb_file
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class StrideSecondaryStructureExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return database_features_stride_directory + protein_name + "_rasa.npy"

    def load(self, protein_name):
        rsa_array = np.load(self._get_file_name(protein_name))
        rsa_array[:, 7] /= 360.
        rsa_array[:, 8] /= 360.
        return rsa_array[:,[0,1,2,3,4,5,6,7,8,10]]
        # return rsa_array[:,10].reshape(-1, 1)

    def extract(self):
        secondary_structure_dict = dict(zip(ss_abbreviations, range(len(ss_abbreviations))))
        stride_x_file = self._get_file_name(self._protein.name)
        if not os.path.exists(stride_x_file):
            if verbose:
                Print.print_info("\t Computing RSA for {0}".format(self._protein.name))
            n = len(self._protein.residues)
            stride_dictionary = stride_dict_from_pdb_file(self._protein.file_name)
            stride_x_array = np.zeros((n, 11))
            for i, residue in enumerate(self._protein.biopython_residues):
                (_, _, cid, (_, residue_index, ri_num)) = residue.get_full_id()
                key = cid, str(residue_index) + ri_num.strip()
                if key in stride_dictionary:
                    (_, s, phi, psi, asa, r_asa) = stride_dictionary[key]
                    if s not in secondary_structure_dict:
                        raise ValueError("unknown secondary structure! Add to dictionary!")
                    ss = np.zeros(len(secondary_structure_dict))
                    ss[secondary_structure_dict[s]] = 1
                    stride_x_array[i, :7] = ss
                    stride_x_array[i, 7] = phi
                    stride_x_array[i, 8] = psi
                    stride_x_array[i, 9] = asa
                    stride_x_array[i, 10] = r_asa
            np.save(stride_x_file, stride_x_array)
