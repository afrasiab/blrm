from abc import abstractmethod
from scipy.spatial.distance import cdist

from blrm.configuration import sd_def_number_of_bins, sd_def_radius, sd_def_seed, sd_def_number_of_samples
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.model.enums import ResidueCategories
import numpy as np

__author__ = 'basir shariat (basir@rams.colostate.edu)'

categories = dict({
    'CYS': ResidueCategories.Polar,
    'GLN': ResidueCategories.Polar,
    'ASP': ResidueCategories.Charged,
    'SER': ResidueCategories.Polar,
    'VAL': ResidueCategories.Hydrophobic,
    'LYS': ResidueCategories.Charged,
    'PRO': ResidueCategories.Hydrophobic,
    'THR': ResidueCategories.Polar,
    'TRP': ResidueCategories.Polar,
    'PHE': ResidueCategories.Hydrophobic,
    'GLU': ResidueCategories.Charged,
    'HIS': ResidueCategories.Polar,
    'GLY': ResidueCategories.Hydrophobic,
    'ILE': ResidueCategories.Hydrophobic,
    'LEU': ResidueCategories.Hydrophobic,
    'ARG': ResidueCategories.Charged,
    'ALA': ResidueCategories.Hydrophobic,
    'ASN': ResidueCategories.Polar,
    'TYR': ResidueCategories.Polar,
    'MET': ResidueCategories.Polar,
})


class BaseShapeDistributionExtractor(AbstractFeatureExtractor):
    def __init__(self, protein, **kwargs):
        super(BaseShapeDistributionExtractor, self).__init__(protein)
        if 'number_of_bins' not in kwargs:
            kwargs['number_of_bins'] = sd_def_number_of_bins
        if 'radius' not in kwargs:
            kwargs['radius'] = sd_def_radius
        if 'seed' not in kwargs:
            kwargs['seed'] = sd_def_seed
        if 'number_of_samples' not in kwargs:
            kwargs['number_of_samples'] = sd_def_number_of_samples

        self.radius = kwargs['radius']
        self.number_of_bins = kwargs['number_of_bins']
        self.number_of_samples = kwargs['number_of_samples']
        self.seed = kwargs['seed']

    @abstractmethod
    def extract(self):
        pass

    def _get_directory(self):
        if self.number_of_samples == -1:
            return "{0}-{1}-{2}".format(self.radius, self.number_of_bins, self.number_of_samples)
        else:
            return "{0}-{1}-{2}-{3}".format(self.radius, self.number_of_bins, self.number_of_samples, self.seed)

    def _compute_shape_distribution(self, first_set, second_set):
        if first_set.size == 0 or second_set.size == 0:
            return 0
        dist = cdist(first_set, second_set).flatten()
        bins = np.linspace(0, self.radius, self.number_of_bins)
        indices = np.digitize(dist, bins)
        distribution = np.bincount(indices, minlength=self.number_of_bins+1)
        if np.sum(distribution) > 0:
            distribution = distribution/np.sum(distribution, dtype=np.float32)
        return distribution[:self.number_of_bins]

    def _compute_normal_distribution(self, first_set, second_set):
        dots = first_set.dot(second_set.T).flatten()
        bins = np.linspace(-1, 1, self.number_of_bins)
        indices = np.digitize(dots, bins)
        norm_distribution = np.bincount(indices, minlength=self.number_of_bins+1)
        if np.sum(norm_distribution) > 0:
            norm_distribution = norm_distribution / np.sum(norm_distribution, dtype=np.float32)
        return norm_distribution[:self.number_of_bins]
