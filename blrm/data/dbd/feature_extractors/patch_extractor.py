import os
import numpy as np

from sklearn.cluster import SpectralClustering, AgglomerativeClustering, KMeans

from blrm.configuration import database_features_patches_directory, verbose, distance_kernel_weight, \
    secondary_structure_kernel_weight, sequence_kernel_weight, distance_kernel_gamma, secondary_structure_kernel_gamma, \
    sequence_kernel_gamma, brief_ss_kernel_keys, brief_ss_kernel, mean_patch_size
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.dbd.feature_extractors.secondary_srtucture_extractor import DSSPSecondaryStructureExtractor
from blrm.patch.clustering.kernel_kmeans import KernelKMeans
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class PatchExtractor(AbstractFeatureExtractor):
    def __init__(self, protein, **kwargs):
        super(PatchExtractor, self).__init__(protein)
        if 'dist_weight' not in kwargs:
            kwargs['dist_weight'] = distance_kernel_weight
        if 'ss_weight' not in kwargs:
            kwargs['ss_weight'] = secondary_structure_kernel_weight
        if 'seq_weight' not in kwargs:
            kwargs['seq_weight'] = sequence_kernel_weight

        if 'dist_gamma' not in kwargs:
            kwargs['dist_gamma'] = distance_kernel_gamma
        if 'ss_gamma' not in kwargs:
            kwargs['ss_gamma'] = secondary_structure_kernel_gamma
        if 'seq_gamma' not in kwargs:
            kwargs['seq_gamma'] = sequence_kernel_gamma
        if 'mean_patch_size' not in kwargs:
            kwargs['mean_patch_size'] = mean_patch_size
        if 'seed' not in kwargs:
            kwargs['seed'] = 1
        if 'verbose' not in kwargs:
            kwargs['verbose'] = True
        if 'other_kernels' not in kwargs:
            kwargs['other_kernels'] = None
        if 'pymol' not in kwargs:
            kwargs['pymol'] = False
        if 'print_stats' not in kwargs:
            kwargs['print_stats'] = True
        self.dist_weight = kwargs['dist_weight']
        self.ss_weight = kwargs['ss_weight']
        self.seq_weight = kwargs['seq_weight']
        self.dist_gamma = kwargs['dist_gamma']
        self.ss_gamma = kwargs['ss_gamma']
        self.seq_gamma = kwargs['seq_gamma']
        self.seed = kwargs['seed']
        self.mean_patch_size = kwargs['mean_patch_size']
        self.verbose = kwargs['verbose']
        self.other_kernels = kwargs['other_kernels']
        self.pymol = kwargs['pymol']
        self.print_stats = kwargs['print_stats']

    def load(self, protein_name):
        kk_file, sc_file, ag_file = self._get_file_name(protein_name)
        return np.load(sc_file)  #, np.load(ag_file)np.load(kk_file) ,

    def extract(self):
        kk_file, sc_file, ag_file = self._get_file_name(self._protein.name)
        if not os.path.exists(sc_file):  # or not os.path.exists(sc_file) or not os.path.exists(ag_file):
            residues = self._protein.residues
            number_of_clusters = max(len(residues) / self.mean_patch_size, 5)
            kernel = self.give_kernel_matrix(residues)
            # if verbose:
            #     Print.print_info("\t Finding {1} kk_patches for {0}".format(self._protein.name, number_of_clusters))
            # kk_patches = self.get_kk_patches(kernel, number_of_clusters)
            # if self.pymol:
            #     self.save_pymol_format(kk_patches)
            # if kk_patches is None:
            #     raise ValueError("Did n\'t find any kk_patches for {}".format(self._protein.name))
            # np.save(kk_file, kk_patches)
            sc_patches = self.get_sc_patches(kernel, number_of_clusters)
            if self.print_stats:
                patche_sizes = np.bincount(sc_patches)
                Print.print_info(u"\t{}: Average patch size: {:0.1f}{}{:0.1f} (min: {}, max: {})".
                                 format(self._protein.name, patche_sizes.mean(),u"\u00B1", patche_sizes.std(),
                                        patche_sizes.min(), patche_sizes.max()))
            np.save(sc_file, sc_patches)
            # np.save(ag_file, self.get_ac_patches(kernel, number_of_clusters))

    def give_kernel_matrix(self, residues):
        ss = DSSPSecondaryStructureExtractor(None).load(self._protein.name)[1]
        n = len(residues)
        d = np.zeros((n, n))
        k_keys = brief_ss_kernel_keys
        for i in range(0, n):
            for j in range(0, n):
                k_dist = np.exp(-np.linalg.norm(residues[i].center - residues[j].center) * self.dist_gamma)
                k_ss = brief_ss_kernel[k_keys[ss[i]], k_keys[ss[j]]] if i != j else 1.0
                k_seq = int(np.abs(i - j) <= 1)
                d[i, j] = self.dist_weight * k_dist + self.ss_weight * k_ss + self.seq_weight * k_seq
        if self.other_kernels is not None:
            d += 0.1 * self.other_kernels
        return d

    def _get_file_name(self, protein_name):
        dir_name = database_features_patches_directory + "{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}/".format(self.seed,
                                                                                                   self.dist_gamma,
                                                                                                   self.ss_gamma,
                                                                                                   self.seq_gamma,
                                                                                                   self.dist_weight,
                                                                                                   self.ss_weight,
                                                                                                   self.seq_weight,
                                                                                                   self.mean_patch_size)
        if self.other_kernels is not None:
            dir_name = dir_name[:-1] + "-*/"
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        base_name = dir_name + protein_name
        return base_name + "_KK.npy", base_name + "_SC.npy", base_name + "_AC.npy"

    def get_kk_patches(self, kernel, n):
        empty_found = False
        while n > 1:
            if empty_found:
                n -= 1
            try:
                km = KernelKMeans(n_clusters=n, max_iter=300, random_state=self.seed)
                m = kernel.shape[0]
                partition = km.fit_predict(np.zeros(m), k=kernel)
                # if self.verbose:
                #     Print.print_info("\t\t Number of clusters found using kernel-Kmeans {0}".format(n))
                return partition
            except ValueError:
                empty_found = True
        return None

    def get_sc_patches(self, kernel, n):
        sc = SpectralClustering(n_clusters=n, random_state=self.seed, affinity='precomputed')
        return sc.fit_predict(kernel)

    # noinspection PyMethodMayBeStatic
    def get_ac_patches(self, kernel, n):
        ag = AgglomerativeClustering(n_clusters=n, affinity='precomputed', linkage='complete')
        return ag.fit_predict(kernel)

    def save_pymol_format(self, patches):
        with open(self._get_file_name(self._protein.name)[0].replace(".npy", ".csv"), "w") as pymol_file:
            for i, residue in enumerate(self._protein.residues):
                pymol_file.write("{}, {}, {}\n".format(residue.residue.parent.id, residue.residue.id[1], patches[i]))
