from abc import abstractmethod
import numpy as np

from numpy.random.mtrand import choice
from blrm.data.dbd.feature_extractors.base_shape_distribution import BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D2BaseShapeDistributionExtractor(BaseShapeDistributionExtractor):
    @abstractmethod
    def extract(self):
        pass

    def _compute_shape_distribution(self, nearby_atoms):
        coordinates = np.array([atom.get_coord() for atom in nearby_atoms])
        n = coordinates.shape[0]
        first, second = range(n), range(n)
        if self.number_of_samples != -1:
            first = np.ix_(choice(n, self.number_of_samples))
            second = np.ix_(choice(n, self.number_of_samples))
        return BaseShapeDistributionExtractor._compute_shape_distribution(self, coordinates[first], coordinates[second])
