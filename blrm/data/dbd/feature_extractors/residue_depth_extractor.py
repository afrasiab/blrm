import os
import numpy as np

from Bio.PDB import ResidueDepth

from blrm.configuration import database_features_residue_depth_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ResidueDepthExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return database_features_residue_depth_directory + protein_name + ".npy"

    def __init__(self, protein, **kwargs):
        super(ResidueDepthExtractor, self).__init__(protein)
        pass
    def extract(self):
        residue_depth_file = self._get_file_name(self._protein.name)
        if not os.path.exists(residue_depth_file):
            if verbose:
                Print.print_info("\t Computing residue depth for {0}".format(self._protein.name))
            pdb_file = self._protein.file_name
            rd = ResidueDepth(self._protein.structure[0], pdb_file)
            rd_array = np.zeros((len(self._protein.residues), 2))
            for (i, res) in enumerate(self._protein.biopython_residues):
                (_, _, c, (h, rn, ic)) = res.get_full_id()
                key = (c, (h, rn, ic))
                if key in rd:
                    rd_array[i, :] = rd[key]
                else:
                    rd_array[i, :] = np.nan
                    message = 'Residue depth for residue {0} can not be computed'.format(key)
                    Print.print_error(message)
                    raise ValueError(message)
            rd_array /= 8.0
            np.save(residue_depth_file, rd_array)
