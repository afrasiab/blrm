import os
import numpy as np

from blrm.configuration import database_features_protrusion_directory, verbose
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.dbd.tools.psaia import run_psaia
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ProtrusionIndexExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        return database_features_protrusion_directory + protein_name + ".npy"

    def __init__(self, protein):
        super(ProtrusionIndexExtractor, self).__init__(protein)

    @staticmethod
    def _normalize_features(casa, rasa, rrasa, rdpx, rcx, rhph):
        """
        Normalizes all PSAIA features to 0-1
        """
        rdpx_max = np.array([7.5131, 2.4013, 7.658, 1.8651, 8.0278, 7.0175])
        rdpx_min = -1.0
        rcx_max = np.array([11.153, 4.8229, 12.212, 4.4148, 19.84, 9.4199])
        rcx_min = -1.0
        rrasa_max = np.array([167.0, 368.04, 124.66, 316.96, 253.85])
        rrasa_min = 0.0
        casa_max = np.array([36661.0, 7584.9, 29756.0, 15550.0, 21489.0])
        casa_min = np.array([1412.0, 235.32, 1174.2, 362.0, 940.64])
        rasa_min = 0.0
        rasa_max = np.array([273.22, 134.51, 216.4, 173.26, 185.47])
        rhph_min = -4.5
        rhph_max = +4.5
        n_rdpx = (rdpx - rdpx_min) / ((rdpx_max - rdpx_min)[:, np.newaxis]).T

        n_rcx = (rcx - rcx_min) / ((rcx_max - rcx_min)[:, np.newaxis]).T
        n_rrasa = (rrasa - rrasa_min) / ((rrasa_max - rrasa_min)[:, np.newaxis]).T
        n_casa = ((casa - casa_min)[:, np.newaxis]).T / ((casa_max - casa_min)[:, np.newaxis]).T
        n_rasa = (rasa - rasa_min) / ((rasa_max - rasa_min)[:, np.newaxis]).T
        n_rhph = (rhph - rhph_min) / (rhph_max - rhph_min)
        protrusion_array = np.zeros(28)
        protrusion_array[:5] = n_casa
        protrusion_array[5:10] = n_rasa
        protrusion_array[10:15] = n_rrasa
        protrusion_array[15:21] = n_rdpx
        protrusion_array[21:27] = n_rcx
        protrusion_array[27] = n_rhph
        return protrusion_array

    def extract(self):
        protrusion_file = self._get_file_name(self._protein.name)
        if not os.path.exists(protrusion_file):
            if verbose:
                Print.print_info("\t Computing protrusion index for {0}".format(self._protein.name))
            pdb_file = self._protein.file_name
            try:
                result_dict = run_psaia(pdb_file, impute=True)
                protrusion_array = np.zeros((len(self._protein.residues), 5 + 5 + 5 + 6 + 6 + 1))
                if result_dict is not None:
                    for index, residue in enumerate(self._protein.biopython_residues):
                        (_, _, cid, (_, residue_index, ri_num)) = residue.get_full_id()
                        key = cid, str(residue_index) + ri_num.strip()
                        if key in result_dict:
                            protrusion_array[index, :] = self._normalize_features(*result_dict[key])
                        else:
                            message = 'Key not found in PSAIA processing!'
                            Print.print_error(message)
                            raise ValueError(message)
                np.save(protrusion_file, protrusion_array[:, 21:])
            except ValueError:
                np.save(protrusion_file, np.repeat(np.array([0.233250423331, 0.349551564503, 0.219667827829,
                                                             0.351003433568,
                                                             0.167735898439, 0.225990056479, 0.631395563863]).reshape(1, -1), len(self._protein.biopython_residues), axis=0))
