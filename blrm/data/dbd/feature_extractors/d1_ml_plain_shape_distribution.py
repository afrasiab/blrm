import os
import numpy as np
from Bio.PDB import NeighborSearch
from blrm.configuration import database_features_d1_directory, verbose, sd_def_levels, number_of_layers
from blrm.data.dbd.feature_extractors.d1_base_shape_distribution import D1BaseShapeDistributionExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D1MultiLevelShapeDistributionExtractor(D1BaseShapeDistributionExtractor):
    def __init__(self, protein, **kwargs):
        super(D1MultiLevelShapeDistributionExtractor, self).__init__(protein, **kwargs)
        if 'number_of_levels' not in kwargs:
            kwargs['number_of_levels'] = sd_def_levels
        self.number_of_levels = kwargs['number_of_levels']

    def _get_file_name(self, protein_name):
        return database_features_d1_directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing Multi-level D1 shape distribution for {0}".format(self._protein.name))
            neighbour_search = NeighborSearch(self._protein.atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_levels * self.number_of_bins))
            k = self.number_of_bins
            previous_layers = set()
            for i in range(len(self._protein.residues)):
                center = self._protein.residues[i].center
                for j in range(self.number_of_levels):
                    nearby_atoms = neighbour_search.search(center, float(self.radius) / (number_of_layers - j), "A")
                    new_layer_atoms = list(set(nearby_atoms) - set(previous_layers))
                    dist = D1BaseShapeDistributionExtractor._compute_shape_distribution(self, new_layer_atoms, center)
                    distributions[i, j * k:(j + 1) * k] = dist
                    previous_layers.update(new_layer_atoms)
            np.save(shape_dist_file, distributions)
