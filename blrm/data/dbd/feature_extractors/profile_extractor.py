import glob
from multiprocessing import cpu_count
import os
from shutil import copyfile
import numpy as np

from matplotlib.cbook import mkdirs
from sklearn.preprocessing import normalize

from blrm.configuration import psiblast_executable, database_features_sequence_directory, verbose, \
    database_features_profile_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ProfileExtractor(AbstractFeatureExtractor):
    def __init__(self, protein, **kwargs):
        super(ProfileExtractor, self).__init__(protein)
        self.fasta_file, self.wpssm_file, self.wpsfm_file = None, None, None

    def _get_file_name(self, protein_name):
        base_name = database_features_profile_directory + protein_name
        fasta_file = database_features_sequence_directory + protein_name + ".fasta"
        return fasta_file, base_name + "_wpssm.npy", base_name + "_wpsfm.npy"

    def load(self, protein_name):
        _, wpssm_file, wpsfm_file = self._get_file_name(protein_name)
        wpssm = np.load(wpssm_file)
        wpsfm = np.load(wpsfm_file)
        return normalize(np.hstack((wpssm, wpsfm)))

    def extract(self):
        self.__save_sequences_to_fasta()
        self.__compute_profiles()

    def __save_sequences_to_fasta(self):
        sequence = self._protein.sequence
        self.fasta_file = self._get_file_name(self._protein.name)[0]
        if os.path.exists(self.fasta_file):
            return
        f = open(self.fasta_file, "w+")
        f.write(">{0}\n".format(self._protein.name))
        f.write(sequence + "\n")
        f.close()

    def __compute_profiles(self, db='nr', niter=3):
        _, self.wpssm_file, self.wpsfm_file = self._get_file_name(self._protein.name)
        if not (os.path.exists(self.wpssm_file) and os.path.exists(self.wpsfm_file)):
            if verbose:
                Print.print_info("\t Computing profile features for {0}".format(self._protein.name))
            matrix_file = self.wpsfm_file[:-10]
            if not os.path.exists(matrix_file + ".mat"):
                Print.print_info_nn("\t... processing protein {0} ...    ".format(self._protein.name))
                command = "{4} " \
                          "-query {0} " \
                          "-db {1} " \
                          "-out {2}.psi.txt " \
                          "-num_iterations {3} " \
                          "-num_threads {5} " \
                          "-out_ascii_pssm {2}.mat" \
                    .format(self.fasta_file, db, matrix_file, niter, psiblast_executable, cpu_count())
                Print.print_info(command)
                error_code = os.system(command)
                if error_code == 0:
                    Print.print_info('Successful!')
                else:
                    Print.print_error('Failed with error code {0}'.format(error_code))
                    raise ValueError('Failed with error code {0}'.format(error_code))
            pssm, psfm, info = ProfileExtractor.__parse_pssm_file(matrix_file + ".mat")
            wpssm = ProfileExtractor.__get_wpsm(pssm)
            wpsfm = ProfileExtractor.__get_wpsm(psfm)
            np.save(self.wpssm_file, wpssm)
            np.save(self.wpsfm_file, wpsfm)

    @staticmethod
    def __get_wpsm(psm, window_size=5):
        window_size = int(window_size)
        (sequence_length, dimension) = psm.shape
        padded_psm = np.vstack((np.zeros((window_size, dimension)), psm, np.zeros((window_size, dimension))))
        ws = 2 * window_size + 1
        wpsm = np.zeros((sequence_length, ws * dimension))
        for i in range(sequence_length):
            wpsm[i, :] = padded_psm[i:i + ws, :].flatten()
        return wpsm

    @staticmethod
    def __parse_pssm_file(file_name):
        pssm = []
        psfm = []
        info = []
        try:
            for line in open(file_name, 'r'):
                record = line.split()
                if len(record) and record[0].isdigit():  # the first character must be a position
                    pssm.append([float(i) for i in record[2:22]])
                    psfm.append([float(i)/100 for i in record[22:42]])
                    info.append(float(record[42]))
            profiles = (np.array(pssm), np.array(psfm), np.array(info))
        except IOError as e:
            print e
            profiles = None
        return profiles


def find_proteins_without_profile():
    complexes_without_profile = []
    mkdirs("/home/basir/no-profile")
    fasta_files = [name for name in glob.glob(database_features_sequence_directory + "*.fasta")]
    for fasta_file in fasta_files:
        if not os.path.exists(database_features_profile_directory + os.path.basename(fasta_file)[:-6] + ".mat"):
            copyfile(fasta_file, "/home/basir/no-profile/" + os.path.basename(fasta_file))


if __name__ == "__main__":
    find_proteins_without_profile()
