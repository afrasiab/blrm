import os
import numpy as np
from Bio.PDB import NeighborSearch

from numpy.random.mtrand import seed

from blrm.configuration import database_features_d1_directory, verbose
from blrm.data.dbd.feature_extractors.d1_base_shape_distribution import D1BaseShapeDistributionExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D1PlainShapeDistributionExtractor(D1BaseShapeDistributionExtractor):

    def _get_file_name(self, protein_name):
        directory = database_features_d1_directory + self._get_directory() + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        return directory + protein_name + ".npy"

    def extract(self):
        shape_dist_file = self._get_file_name(self._protein.name)
        if not os.path.exists(shape_dist_file):
            if verbose:
                Print.print_info("\t Computing plain D1 shape distribution for {0}".format(self._protein.name))
            neighbour_search = NeighborSearch(self._protein.atoms)
            distributions = np.zeros((len(self._protein.residues), self.number_of_bins))
            for i in range(len(self._protein.residues)):
                residue = self._protein.residues[i]
                center = residue.center
                nearby_atoms = neighbour_search.search(center, self.radius, "A")
                dist = D1BaseShapeDistributionExtractor._compute_shape_distribution(self, nearby_atoms, center)
                distributions[i, :] = dist
            np.save(shape_dist_file, distributions)
