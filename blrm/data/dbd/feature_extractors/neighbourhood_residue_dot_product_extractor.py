import os
import pickle

import numpy as np

from numpy.random.mtrand import seed

from sklearn.preprocessing import normalize
from scipy.spatial.distance import cdist

from blrm.configuration import database_features_neighbourhood_residue_dot_product_directory, verbose, \
    curv_def_number_of_bins, curv_def_seed, curv_def_dot_prod_neighbour_residue_radius, \
    curv_def_dot_prod_neighbour_surface_radius
# from pairpred.configuration import default_fixed_neighbours_number # used for fixed number of neighbours
from blrm.data.tools_interface.msms import get_surface_atoms
from blrm.utils.pretty_print import Print

__author__ = 'Alex Fout (fout@colostate.edu), modified from Basir Shariat (basir@rams.colostate.edu)'


class NeighbourhoodResidueDotProductExtractor():
    def _get_file_name(self, protein_name):
        directory = database_features_neighbourhood_residue_dot_product_directory + self.get_directory() + "/"
        if not os.path.exists(directory):
            os.makedirs(directory)
        dot_prod_filen = directory + protein_name + ".npy"
        results_filen = directory + protein_name + "_results.pkl"
        return dot_prod_filen, results_filen

    def get_directory(self):
        return "{0}-{1}-{2}".format(self.surface_neighbourhood_radius, self.residue_neighbourhood_radius,
                                    self.number_of_bins)

    def load(self, protein_name):
        filename, _ = self._get_file_name(protein_name)
        feature = np.load(filename)
        return normalize(feature)

    def __init__(self, protein, **kwargs):
        if 'residue_neighbourhood_radius' not in kwargs:
            kwargs['residue_neighbourhood_radius'] = curv_def_dot_prod_neighbour_residue_radius
        if 'surface_neighbourhood_radius' not in kwargs:
            kwargs['surface_neighbourhood_radius'] = curv_def_dot_prod_neighbour_surface_radius
        if 'number_of_bins' not in kwargs:
            kwargs['number_of_bins'] = curv_def_number_of_bins
        if 'seed' not in kwargs:
            kwargs['seed'] = curv_def_seed
        self.residue_neighbourhood_radius = kwargs['residue_neighbourhood_radius']
        self.surface_neighbourhood_radius = kwargs['surface_neighbourhood_radius']
        self.number_of_bins = kwargs['number_of_bins']
        self.protein = protein
        self.seed = kwargs['seed']

    def extract(self):
        neighbourhood_res_dot_product_file, results_filen = self._get_file_name(self.protein.name)
        if not os.path.exists(neighbourhood_res_dot_product_file):
            if verbose:
                Print.print_info("\t Computing neighborhood residue dot products for {0}".format(self.protein.name))
            # get surface mesh of protein (yes, the name is misleading)
            surface, normals = get_surface_atoms(self.protein.file_name)

            # compute an orientation vector for each residue
            orientation_vectors = []
            for i in range(len(self.protein.residues)):
                residue = self.protein.residues[i]
                # orientation_vectors.append(self.get_surface_vector(residue.center, surface))
                orientation_vectors.append(
                    self.get_ave_normal_vector(residue.center, surface, normals, self.surface_neighbourhood_radius))

            # for each residue...
            results = [None] * len(self.protein.residues)
            dp_distributions = np.zeros((len(self.protein.residues), self.number_of_bins))
            neighbourhood_array = np.zeros((len(self.protein.residues)))
            for i, query_residue in enumerate(self.protein.residues):
                result = {}
                result["residue"] = {"loc": query_residue.center, "vector": orientation_vectors[i]}
                result["neighbours"] = []
                # if this residue has no orientation vector then skip it. 
                if np.linalg.norm(orientation_vectors[i]) != 0:
                    query_res_ov = orientation_vectors[i]
                    neighbour_indices = []
                    distances = []
                    # get residues in the neighbourhood of each residue
                    for j, neighbour_residue in enumerate(self.protein.residues):
                        # don't consider self as neighbour
                        if j == i:
                            continue
                        # compute distance as the min distance between atoms in the residue
                        distance = cdist(query_residue.get_coordinates(), neighbour_residue.get_coordinates()).min()
                        if distance < self.residue_neighbourhood_radius and (j != i):
                            neighbour_indices.append(j)
                            distances.append(distance)
                    # for each neighbour...
                    res_dot_products = np.zeros(len(neighbour_indices))
                    k = 0
                    for j, (neighbour_residue, neighbour_res_ov) in enumerate(
                            zip(self.protein.residues, orientation_vectors)):
                        if (j not in neighbour_indices):
                            continue
                        if np.linalg.norm(neighbour_res_ov) == 0:
                            continue
                        # get the ('sensed') cos(angle) between the two orientation vectors
                        res_dot_products[k] = self.get_dot_product(query_residue.center, neighbour_residue.center, \
                                                                   query_res_ov, neighbour_res_ov, cos=True)
                        result["neighbours"].append({"loc": neighbour_residue.center, \
                                                     "dot_product": res_dot_products[k], \
                                                     "vector": neighbour_res_ov, \
                                                     "distance": distances[k]})
                        k += 1
                # generate a distribution of dot products
                bins, dp_distributions[i, :] = self.get_distribution(res_dot_products)
                result["distribution"] = dp_distributions[i, :]
                # add result to the results list
                results[i] = result
            # save output files
            pickle.dump((self.surface_neighbourhood_radius, bins, results), open(results_filen, 'wb'))
            np.save(neighbourhood_res_dot_product_file, dp_distributions)

    def get_distribution(self, res_dot_products):
        dp_distribution = np.zeros((self.number_of_bins,))
        # create bins
        bins = np.linspace(-1, 1, self.number_of_bins + 1)
        if max(res_dot_products) == 0 and min(res_dot_products) == 0:
            return bins, dp_distribution
        res_dot_products = [k - 2 if k == 0 else k for k in res_dot_products]
        # determine the bin of each dot product
        indices = np.digitize(res_dot_products, bins)
        if len(indices) != 0:
            # NANs are given bin number one greater than the greatest index. If they exist, max(indices) will be out of range
            max_index = min(self.number_of_bins, max(indices))
            # import pdb; pdb.set_trace()
            # generate dist by counting number of elements in a certain bin
            dp_distribution[:max_index] = np.bincount(indices)[1:max_index + 1] / float(len(indices))
        return bins, dp_distribution

    def get_dot_product(self, query_residue_center, neighbour_residue_center, query_res_ov, neighbour_res_ov,
                        cos=False):
        # get dot product of surface vectors
        if cos:
            # if want just cos(angle)
            raw_dot_product = np.dot(query_res_ov, neighbour_res_ov) / (
            np.linalg.norm(query_res_ov) * np.linalg.norm(neighbour_res_ov))  # actually it's cos(angle) between vectors
        else:
            # full dot product
            raw_dot_product = np.dot(query_res_ov, neighbour_res_ov)
        # determine "sense" of dot product. I.e are surface vectors pointing towards or away from each other?
        neighbour_displacement = neighbour_residue_center - query_residue_center
        perp_neighbour_displacement = neighbour_displacement - query_res_ov * np.dot(query_res_ov,
                                                                                     neighbour_displacement)
        sense = (-1) * np.sign(np.dot(neighbour_res_ov, perp_neighbour_displacement))
        dot_product = sense * raw_dot_product
        return dot_product

    # return vector from center to nearest point on surface
    def get_surface_vector(self, center, surface):
        # find distances b/t all surface points and the center
        distances = np.linalg.norm(surface - center, None, 1)
        # TODO should maybe average over the k closest surfaces in case they are all similar (like a sphere around the residue)?
        surface_point = surface[np.argmin(distances)]
        surface_vector = surface_point - center
        return surface_vector

    # return vector which is the average of the surface normals which are in a neighbourhood of the residue
    # if there are no surface points in the neighbourhood returns (0,0,0)
    def get_ave_normal_vector(self, center, surface, normals, neighbourhood_radius):
        # find distances b/t all surface points and the center
        distances = np.linalg.norm(surface - center, None, 1)
        n_hood_indices = distances < neighbourhood_radius
        vec = np.zeros(3)
        if np.sum(n_hood_indices) != 0:
            vec = np.mean(normals[n_hood_indices, :], axis=0)
        return vec
