import os
import numpy as np

from blrm.configuration import verbose, database_features_residue_id_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.utils.pretty_print import Print

__author__ = 'basir'


class ResidueIDExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, protein_name):
        base_name = database_features_residue_id_directory + protein_name
        return base_name + ".npy", base_name + "_chains.npy"

    def load(self, protein_name):
        id_file_name, chains_file_name = self._get_file_name(protein_name)
        return np.load(id_file_name), np.load(chains_file_name)

    def extract(self):
        id_file_name, chains_file_name = self._get_file_name(self._protein.name)
        if not os.path.exists(id_file_name) or not os.path.exists(chains_file_name):
            if verbose:
                Print.print_info("\t Computing residue IDs for {0}".format(self._protein.name))
            ids = []
            for i, residue in enumerate(self._protein.residues):
                ids.append("{0}-{1}".format(self._protein.name.replace("_", "-"), i))

            np.save(id_file_name, np.array(ids))
            np.save(chains_file_name, np.array(self._protein.get_chains_ids()))
