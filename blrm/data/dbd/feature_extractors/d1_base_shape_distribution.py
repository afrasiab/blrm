from numpy.random.mtrand import choice
import numpy as np
from abc import ABCMeta, abstractmethod

from blrm.data.dbd.feature_extractors.base_shape_distribution import BaseShapeDistributionExtractor

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class D1BaseShapeDistributionExtractor(BaseShapeDistributionExtractor):
    __metaclass__ = ABCMeta

    def _get_file_name(self, protein_name):
        pass

    def extract(self):
        pass

    def _compute_shape_distribution(self, atoms, center):
        n = len(atoms)
        coordinates = np.zeros((n, 3))
        for i, atom in enumerate(atoms):
            coordinates[i] = atom.get_coord()

        center = center.reshape((1, center.size))
        indices = range(n)
        if self.number_of_samples != -1 and self.number_of_samples < n:
            indices = choice(n, self.number_of_samples)

        return BaseShapeDistributionExtractor._compute_shape_distribution(self, coordinates[indices], center)
