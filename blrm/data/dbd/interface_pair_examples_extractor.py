import glob
import numpy as np
import os
import warnings
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import basename
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from scipy.spatial.distance import cdist
from blrm.configuration import database_interface_pair_examples_directory, interaction_thr, verbose, \
    dbd5_pdb_directory, default_soft_margin_parameter, example_id_separator
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.common.pdb import PDB
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.feature_extractors.true_interface_extractor import TrueInterfaceExtractor
from blrm.utils.pretty_print import Print


__author__ = 'basir shariat (basir@rams.colostate.edu)'


class InterfacePairExampleExtractor(AbstractFeatureExtractor):
    def _get_file_name(self, complex_name):
        name = database_interface_pair_examples_directory + complex_name
        return name + "_pos.npy", name + "_neg.npy", name + "_pos_dist.npy", name + "_neg_dist.npy"

    def __init__(self, complex_object_model, **kwargs):
        super(InterfacePairExampleExtractor, self).__init__(None, **kwargs)
        self.complex = complex_object_model

    def load(self, complex_name):
        pos_file, neg_file, pos_dist_file, neg_dist_file = self._get_file_name(complex_name)
        pos, neg, pos_dist, neg_dist = np.load(pos_file), np.load(neg_file), np.load(pos_dist_file), np.load(
            neg_dist_file)
        return pos, neg, pos_dist, neg_dist

    def extract(self):
        code = self.complex.complex_code
        pos_example_file, neg_examples_file, pos_dist_file, neg_dist_file = self._get_file_name(code)
        if not os.path.exists(pos_example_file) \
                or not os.path.exists(neg_examples_file) \
                or not os.path.exists(pos_dist_file) \
                or not os.path.exists(neg_dist_file):
            if verbose:
                Print.print_info("\t Extracting interacting/non-interacting residues for {0}"
                                 .format(self.complex.complex_code))
            ligand_u = self.complex.unbound_formation.ligand
            receptor_u = self.complex.unbound_formation.receptor
            ligand_b = self.complex.bound_formation.ligand
            receptor_b = self.complex.bound_formation.receptor
            bound_ligand_residues = ligand_b.residues
            unbound_ligand_residues = ligand_u.residues
            bound_receptor_residues = receptor_b.residues
            unbound_receptor_residues = receptor_u.residues
            ligand_b2u = self.complex.ligand_bound_to_unbound
            receptor_b2u = self.complex.receptor_bound_to_unbound
            pos = []
            neg = []
            pos_d = []
            neg_d = []
            u_l_interface = TrueInterfaceExtractor(None).load(ligand_u.name)
            u_r_interface = TrueInterfaceExtractor(None).load(receptor_u.name)
            l_full_ids = ResidueFullIDExtractor(None).load(ligand_u.name)
            r_full_ids = ResidueFullIDExtractor(None).load(receptor_u.name)

            for i, b_l_res in enumerate(bound_ligand_residues):
                if i not in ligand_b2u:
                    continue
                for j, b_r_res in enumerate(bound_receptor_residues):
                    if j not in receptor_b2u:
                        continue
                    min_d = cdist(b_l_res.get_coordinates(), b_r_res.get_coordinates()).min()
                    l_i, r_j = ligand_b2u[i], receptor_b2u[j]
                    # skip the non-standard residues and other molecules
                    if unbound_ligand_residues[l_i].is_hetero() or unbound_receptor_residues[r_j].is_hetero():
                        continue

                    e_id = "{}{}{}".format(l_full_ids[l_i], example_id_separator, r_full_ids[r_j])
                    if u_l_interface[l_i] == 1 and u_r_interface[r_j] == 1:
                        pos_d.append(min_d)
                        pos.append((e_id, '+1'))
                    else:

                        neg_d.append(min_d)
                        neg.append((e_id, '-1'))
            np.save(pos_example_file, np.array(pos))
            np.save(neg_examples_file, np.array(neg))
            np.save(pos_dist_file, np.array(pos_d))
            np.save(neg_dist_file, np.array(neg_d))


def read_pdbs(the_complex):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        ligand_bound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_l_b.pdb"))
        receptor_bound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_r_b.pdb"))
        ligand_unbound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_l_u.pdb"))
        receptor_unbound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_r_u.pdb"))
        bound_formation = ProteinPair(ligand_bound, receptor_bound)
        unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
        return ProteinComplex(the_complex, unbound_formation, bound_formation)


def main():
    complexes = [basename(name).replace("_l_b.pdb", "") for name in
                 glob.glob(dbd5_pdb_directory + "*_l_b.pdb")]
    complexes.remove("2B42")
    complexes.remove("3R9A")
    for counter, complex_code in enumerate(complexes):
        Print.print_info("{0}/{1}... processing complex {2}".format(counter + 1, len(complexes), complex_code))
        complex_object_model = read_pdbs(complex_code)
        InterfacePairExampleExtractor(complex_object_model).extract()


if __name__ == '__main__':
    main()
