import glob
import numpy as np
import os
import warnings
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from os.path import basename
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from scipy.spatial.distance import cdist
from blrm.configuration import interaction_thr, verbose, dbd5_pdb_directory, database_examples_by_index_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.common.pdb import PDB
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ExampleIndexExtractor(AbstractFeatureExtractor):
    def __init__(self, complex_object_model, **kwargs):
        super(ExampleIndexExtractor, self).__init__(None, **kwargs)
        self.complex = complex_object_model

    def _get_file_name(self, complex_name):
        return database_examples_by_index_directory + complex_name + ".npy"

    def load(self, complex_name):
        return np.load(self._get_file_name(complex_name))

    def extract(self):
        code = self.complex.complex_code
        file_name = self._get_file_name(code)
        if not os.path.exists(file_name):
            if verbose:
                Print.print_info(
                    "\t Extracting interacting/non-interacting residues for {0}".format(self.complex.complex_code))
            bound_ligand_residues = self.complex.bound_formation.ligand.residues
            bound_receptor_residues = self.complex.bound_formation.receptor.residues
            ligand_u = self.complex.unbound_formation.ligand
            receptor_u = self.complex.unbound_formation.receptor
            unbound_ligand_residues = ligand_u.residues
            unbound_receptor_residues = receptor_u.residues
            ligand_b2u = self.complex.ligand_bound_to_unbound
            receptor_b2u = self.complex.receptor_bound_to_unbound
            e = []
            for i, b_l_res in enumerate(bound_ligand_residues):
                if i not in ligand_b2u:
                    continue
                for j, b_r_res in enumerate(bound_receptor_residues):
                    if j not in receptor_b2u:
                        continue
                    min_distance = cdist(b_l_res.get_coordinates(), b_r_res.get_coordinates()).min()
                    l_i, r_j = ligand_b2u[i], receptor_b2u[j]
                    # skip the non-standard residues and other molecules
                    if unbound_ligand_residues[l_i].is_hetero() or unbound_receptor_residues[r_j].is_hetero():
                        continue
                    e.append((l_i, r_j, 1 if min_distance < interaction_thr else -1, min_distance))
            np.save(file_name, np.array(e))


def read_pdbs(the_complex):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", PDBConstructionWarning)
        ligand_bound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_l_b.pdb"))
        receptor_bound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_r_b.pdb"))
        ligand_unbound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_l_u.pdb"))
        receptor_unbound = Protein(*PDB.read_pdb_file(dbd5_pdb_directory + the_complex + "_r_u.pdb"))
        bound_formation = ProteinPair(ligand_bound, receptor_bound)
        unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
        return ProteinComplex(the_complex, unbound_formation, bound_formation)


def main():
    complexes = [basename(name).replace("_l_b.pdb", "") for name in
                 glob.glob(dbd5_pdb_directory + "*_l_b.pdb")]
    for counter, complex_code in enumerate(complexes):
        Print.print_info("{0}/{1}... processing complex {2}".format(counter + 1, len(complexes), complex_code))
        complex_object_model = read_pdbs(complex_code)
        ExampleIndexExtractor(complex_object_model).extract()


if __name__ == '__main__':
    main()
