import cPickle
import glob

import os
from os.path import basename

from blrm.configuration import full_id_map_directory, verbose, dbd5_pdb_directory
from blrm.data.abstract_feature_extractor import AbstractFeatureExtractor
from blrm.data.dbd.examples_extractor import read_pdbs
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class ComplexFullIDMapper(AbstractFeatureExtractor):
    def _get_file_name(self, complex_name):
        name = full_id_map_directory + complex_name
        return name + ".cpickle"

    def __init__(self, complex_object_model, **kwargs):
        super(ComplexFullIDMapper, self).__init__(None, **kwargs)
        self.complex = complex_object_model

    def load(self, complex_name):
        mapper_file = self._get_file_name(complex_name)
        return cPickle.load(open(mapper_file))

    def extract(self):
        code = self.complex.complex_code
        if not os.path.exists(self._get_file_name(code)):
            if verbose:
                Print.print_info(
                    "\t Extracting residue ID maps for {0}".format(self.complex.complex_code))
            bound_ligand_residues = self.complex.bound_formation.ligand.residues
            bound_receptor_residues = self.complex.bound_formation.receptor.residues
            ligand_u = self.complex.unbound_formation.ligand
            receptor_u = self.complex.unbound_formation.receptor
            unbound_ligand_residues = ligand_u.residues
            unbound_receptor_residues = receptor_u.residues
            ligand_b2u = self.complex.ligand_bound_to_unbound
            receptor_b2u = self.complex.receptor_bound_to_unbound
            l_map = {}
            for index in ligand_b2u:
                b_residue = bound_ligand_residues[index].residue
                u_residue = unbound_ligand_residues[ligand_b2u[index]].residue
                u_key = "{}-{}-{}".format(self.complex.unbound_formation.ligand.name, u_residue.parent.id, u_residue.id[1])
                b_key = "{}-{}-{}".format(self.complex.bound_formation.ligand.name, b_residue.parent.id, b_residue.id[1])
                l_map[u_key] = b_key
            r_map = {}
            for index in receptor_b2u:
                b_residue = bound_receptor_residues[index].residue
                u_residue = unbound_receptor_residues[receptor_b2u[index]].residue
                u_key = "{}-{}-{}".format(self.complex.unbound_formation.receptor.name, u_residue.parent.id, u_residue.id[1])
                b_key = "{}-{}-{}".format(self.complex.bound_formation.receptor.name, b_residue.parent.id, b_residue.id[1])
                r_map[u_key] = b_key
            final_map = l_map.copy()
            final_map.update(r_map)
            cPickle.dump(final_map, open(self._get_file_name(code), "w"))


def main():
    complexes = [basename(name).replace("_l_b.pdb", "") for name in
                 glob.glob(dbd5_pdb_directory + "*_l_b.pdb")]
    for counter, complex_code in enumerate(complexes):
        Print.print_info("{0}/{1}... processing complex {2}".format(counter + 1, len(complexes), complex_code))
        complex_object_model = read_pdbs(complex_code)
        ComplexFullIDMapper(complex_object_model).extract()


if __name__ == '__main__':
    main()
