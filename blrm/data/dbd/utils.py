from blrm.configuration import g_profile, g_exp, g_RD, g_SD, g_HSAAC, g_CX, g_p_exp, g_surf_curv_comb, g_surf_dp, g_res_dp, g_surf_curv_rmse, g_surf_curv, \
    g_res_dp2, g_surf_dp2, g_surf_curv_comb2, g_surf_curv2, g_surf_curv_rmse2
from blrm.data.dbd.feature_extractors.aa_onehot_extractor import AAOneHotExtractor
from blrm.data.dbd.feature_extractors.d1_category_shape_distribution import D1CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_ml_plain_shape_distribution import D1MultiLevelShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_plain_shape_distribution import D1PlainShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_ml_plain_shape_distribution import D2MultiLevelShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_plain_shape_distribution import D2PlainShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_atoms_shape_distribution import \
    D1SurfaceAtomsShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_shape_distribution import D1SurfaceShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_surface_category_shape_distribution import \
    D1SurfaceCategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_category_shape_distribution import D2CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_sureface_atoms_shape_distribution import \
    D2SurfaceAtomsShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_sureface_shape_distribution import D2SurfaceShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_surface_category_shape_distribution import \
    D2SurfaceCategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.half_sphere_amino_acid_composition_extractor import \
    HalfSphereAminoAcidCompositionExtractor
from blrm.data.dbd.feature_extractors.neighbourhood_residue_dot_product_extractor2 import \
    NeighbourhoodResidueDotProductExtractor2
from blrm.data.dbd.feature_extractors.neighbourhood_surface_dot_product_extractor2 import \
    NeighbourhoodSurfaceDotProductExtractor2
from blrm.data.dbd.feature_extractors.predicted_ss_extractor import PredictedSSExtractor
from blrm.data.dbd.feature_extractors.profile_extractor import ProfileExtractor
from blrm.data.dbd.feature_extractors.protrusion_index_extractor import ProtrusionIndexExtractor
from blrm.data.dbd.feature_extractors.residue_depth_extractor import ResidueDepthExtractor
from blrm.data.dbd.feature_extractors.short_window_profile_extractor import ShortWindowProfileExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base2 import \
    SurfaceAtomsCurvatureExtractorBase2
from blrm.data.dbd.feature_extractors.wpsfm_extractor import WPSFMExtractor
from blrm.data.dbd.feature_extractors.wpssm_extractor import WPSSMExtractor

from blrm.data.dbd.feature_extractors.neighbourhood_residue_dot_product_extractor import NeighbourhoodResidueDotProductExtractor
from blrm.data.dbd.feature_extractors.neighbourhood_surface_dot_product_extractor import NeighbourhoodSurfaceDotProductExtractor
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor import SurfaceAtomsCurvatureExtractor
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_rmse_extractor import SurfaceAtomsCurvatureRMSEExtractor
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base import SurfaceAtomsCurvatureExtractorBase

from blrm.data.dbd.feature_extractors.neighbourhood_residue_dot_product_extractor2 import NeighbourhoodResidueDotProductExtractor2
from blrm.data.dbd.feature_extractors.neighbourhood_surface_dot_product_extractor2 import NeighbourhoodSurfaceDotProductExtractor2
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor2 import SurfaceAtomsCurvatureExtractor2
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_rmse_extractor2 import SurfaceAtomsCurvatureRMSEExtractor2
from blrm.data.dbd.feature_extractors.surface_atoms_curvature_extractor_base2 import SurfaceAtomsCurvatureExtractorBase2

from blrm.model.enums import Features

feature_dict = {
    Features.AA_ONE_HOT:
        (AAOneHotExtractor, g_profile, "Amino Acid"),
    Features.WINDOWED_POSITION_SPECIFIC_SCORING_MATRIX:
        (WPSSMExtractor, g_profile, "WPSSM"),
    Features.WINDOWED_POSITION_SPECIFIC_FREQUENCY_MATRIX:
        (WPSFMExtractor, g_profile, "WPSFM"),
    Features.PROFILE:
        (ProfileExtractor, g_profile, "Profile"),
    Features.RELATIVE_ACCESSIBLE_SURFACE_AREA:
        (StrideSecondaryStructureExtractor, g_exp, "rASA"),
    Features.RESIDUE_DEPTH:
        (ResidueDepthExtractor, g_RD, "Residue Depth"),
    Features.D2_PLAIN_SHAPE_DISTRIBUTION:
        (D2PlainShapeDistributionExtractor, g_SD, "D2 Plain SD"),
    Features.D1_PLAIN_SHAPE_DISTRIBUTION:
        (D1PlainShapeDistributionExtractor, g_SD, "D1 Plain SD"),
    Features.D2_MULTI_LEVEL_SHAPE_DISTRIBUTION:
        (D2MultiLevelShapeDistributionExtractor, g_SD, "D2 Multi-level SD"),
    Features.D1_MULTI_LEVEL_SHAPE_DISTRIBUTION:
        (D1MultiLevelShapeDistributionExtractor, g_SD, "D1 Multi-level SD"),
    Features.D1_SURFACE_SHAPE_DISTRIBUTION:
        (D1SurfaceShapeDistributionExtractor, g_SD, "D1 Surface residue SD"),
    Features.D2_SURFACE_SHAPE_DISTRIBUTION:
        (D2SurfaceShapeDistributionExtractor, g_SD, "D2 Surface residue SD"),
    Features.D1_SURFACE_ATOM_SHAPE_DISTRIBUTION:
        (D1SurfaceAtomsShapeDistributionExtractor, g_SD, "D1 Surface Atom SD"),
    Features.D2_SURFACE_ATOM_SHAPE_DISTRIBUTION:
        (D2SurfaceAtomsShapeDistributionExtractor, g_SD, "D2 Surface Atom SD"),
    Features.D2_CATEGORY_SHAPE_DISTRIBUTION:
        (D2CategoryShapeDistributionExtractor, g_SD, "D2 Category SD"),
    Features.D1_SURFACE_CATEGORY_SHAPE_DISTRIBUTION:
        (D1SurfaceCategoryShapeDistributionExtractor, g_SD, "D1 Surface Category SD"),
    Features.D2_SURFACE_CATEGORY_SHAPE_DISTRIBUTION:
        (D2SurfaceCategoryShapeDistributionExtractor, g_SD, "D2 Surface Category SD"),
    Features.D1_CATEGORY_SHAPE_DISTRIBUTION:
        (D1CategoryShapeDistributionExtractor, g_SD, "D1 Category SD"),
    Features.HALF_SPHERE_EXPOSURE:
        (HalfSphereAminoAcidCompositionExtractor, g_HSAAC, "Half Sphere Amino Acid Composition"),
    Features.PROTRUSION_INDEX:
        (ProtrusionIndexExtractor, g_CX, "Protrusion Index"),
    Features.PREDICTED_RELATIVE_ACCESSIBLE_SURFACE_AREA:
        (PredictedSSExtractor, g_p_exp, "Predicted rASA"),
    Features.SHORT_PROFILE:
        (ShortWindowProfileExtractor, g_profile, "Short Profile"),
    Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT:
        (NeighbourhoodResidueDotProductExtractor, g_res_dp, "Neighbourhood Residue Dot Product"),
    Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT:
        (NeighbourhoodSurfaceDotProductExtractor, g_surf_dp, "Neighbourhood Surface Dot Product"),
    Features.SURFACE_ATOMS_CURVATURE_COMBINED:
        (SurfaceAtomsCurvatureExtractorBase, g_surf_curv_comb, "Surface Atoms Curvature Combined"),
    Features.SURFACE_ATOMS_CURVATURE:
        (SurfaceAtomsCurvatureExtractor, g_surf_curv, "Surface Atoms Curvature"),
    Features.SURFACE_ATOMS_CURVATURE_RMSE:
        (SurfaceAtomsCurvatureRMSEExtractor, g_surf_curv_rmse, "Surface Atoms Curvature RMSE"),
    Features.NEIGHBOURHOOD_RESIDUE_DOT_PRODUCT2:
        (NeighbourhoodResidueDotProductExtractor2, g_res_dp2, "Neighbourhood Residue Dot Product2"),
    Features.NEIGHBOURHOOD_SURFACE_DOT_PRODUCT2:
        (NeighbourhoodSurfaceDotProductExtractor2, g_surf_dp2, "Neighbourhood Surface Dot Product2"),
    Features.SURFACE_ATOMS_CURVATURE_COMBINED2:
        (SurfaceAtomsCurvatureExtractorBase2, g_surf_curv_comb2, "Surface Atoms Curvature Combined2"),
    Features.SURFACE_ATOMS_CURVATURE2:
        (SurfaceAtomsCurvatureExtractor2, g_surf_curv2, "Surface Atoms Curvature2"),
    Features.SURFACE_ATOMS_CURVATURE_RMSE2:
        (SurfaceAtomsCurvatureRMSEExtractor2, g_surf_curv_rmse2, "Surface Atoms Curvature RMSE2"),
}

#
# def shared_dataset(data_xy, borrow=True):
#     data_x, data_y = data_xy
#     shared_x = theano.shared(numpy.asarray(data_x,
#                                            dtype=theano.config.floatX),
#                              borrow=borrow)
#     shared_y = theano.shared(numpy.asarray(data_y,
#                                            dtype=theano.config.floatX),
#                              borrow=borrow)
#     return shared_x, T.cast(shared_y, 'int32')