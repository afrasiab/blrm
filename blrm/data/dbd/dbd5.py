from __future__ import print_function
from __future__ import print_function
import glob
from sklearn import linear_model

import numpy as np
import os
import warnings
from Bio.PDB.PDBExceptions import PDBConstructionWarning
from PyML.containers.pairData import PairDataSet
from PyML.containers.vectorDatasets import VectorDataSet
from PyML.utils.misc import ProgressBar
from os.path import basename

from blrm.data.dbd.example_index_extractor import ExampleIndexExtractor
from blrm.data.dbd.feature_extractors.aa_onehot_extractor import AAOneHotExtractor
from blrm.data.dbd.feature_extractors.d1_category_shape_distribution import D1CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_ml_plain_shape_distribution import D2MultiLevelShapeDistributionExtractor
from blrm.model.protein import Protein
from blrm.model.protein_complex import ProteinComplex
from blrm.model.protein_pair import ProteinPair
from sklearn.preprocessing import normalize
from blrm.configuration import database_pdb_directory, verbose, data_tmp_directory, \
    default_soft_margin_parameter, interaction_thr, \
    default_smooth_soft_margin_sigma, example_id_separator, examples_vectors_directory, default_fixed_neighbours_number
from blrm.configuration import examples_vectors_directory
from blrm.configuration import seed
from blrm.data.abstract_dataset import AbstractDatabase
from blrm.data.common.pdb import PDB
from blrm.data.dbd.examples_extractor import ExampleExtractor
from blrm.data.dbd.feature_extractors.b_value_extractor import BValueExtractor
from blrm.data.dbd.feature_extractors.d1_ml_plain_shape_distribution import D1MultiLevelShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_plain_shape_distribution import D1PlainShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_atoms_shape_distribution import \
    D1SurfaceAtomsShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_sureface_shape_distribution import D1SurfaceShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d1_surface_category_shape_distribution import \
    D1SurfaceCategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_category_shape_distribution import D2CategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_plain_shape_distribution import D2PlainShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_sureface_atoms_shape_distribution import \
    D2SurfaceAtomsShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_sureface_shape_distribution import D2SurfaceShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.d2_surface_category_shape_distribution import \
    D2SurfaceCategoryShapeDistributionExtractor
from blrm.data.dbd.feature_extractors.fixed_neighbourhood_extractor import FixedNeighbourhoodExtractor
from blrm.data.dbd.feature_extractors.half_sphere_amino_acid_composition_extractor import \
    HalfSphereAminoAcidCompositionExtractor
from blrm.data.dbd.feature_extractors.patch_extractor import PatchExtractor
from blrm.data.dbd.feature_extractors.predicted_ss_extractor import PredictedSSExtractor
from blrm.data.dbd.feature_extractors.profile_extractor import ProfileExtractor
from blrm.data.dbd.feature_extractors.protein_length_extractor import ProteinLengthExtractor
from blrm.data.dbd.feature_extractors.protrusion_index_extractor import ProtrusionIndexExtractor
from blrm.data.dbd.feature_extractors.residue_depth_extractor import ResidueDepthExtractor
from blrm.data.dbd.feature_extractors.residue_full_id_extractor import ResidueFullIDExtractor
from blrm.data.dbd.feature_extractors.residue_id_extractor import ResidueIDExtractor
from blrm.data.dbd.feature_extractors.residue_neighbourhood_extractor import ResidueNeighbourhoodExtractor
from blrm.data.dbd.feature_extractors.secondary_srtucture_extractor import DSSPSecondaryStructureExtractor
from blrm.data.dbd.feature_extractors.stride_secondary_srtucture_extractor import StrideSecondaryStructureExtractor
from blrm.data.dbd.orderless_examples_extractor import OrderlessExampleExtractor
from blrm.data.dbd.patch_examples_extractor import PatchExampleExtractor
from blrm.data.dbd.utils import feature_dict
from blrm.model.enums import Features
from blrm.model.representation.naa_residues import NearestAATypeResidues
from blrm.model.representation.pairwise_explicit_represetation import PairwiseKernel
from blrm.utils.pretty_print import Print

__author__ = 'basir shariat (basir@rams.colostate.edu)'


class DBD5(AbstractDatabase):
    def __init__(self, complexes=None, directory=database_pdb_directory, check_features=True, **kwargs):
        self.name = "Docking Benchmark Dataset 5"
        # if a subset of the data set is specified only use them otherwise use all complexes.
        self.directory = directory
        if complexes is not None:
            self.complexes = complexes
        else:
            self.complexes = [basename(name).replace("_l_b.pdb", "") for name in
                              glob.glob(self.directory + "*_l_b.pdb")]
        self.complexes.sort()
        if check_features:
            Print.print_info("Extracting features ...")
            for counter, complex_code in enumerate(self.complexes):
                Print.print_info(
                    "{0}/{1} processing complex {2}".format(counter + 1, len(self.complexes), complex_code))
                complex_object_model = self.__read_pdbs(complex_code)
                self.__extract_features(complex_object_model, **kwargs)

    @staticmethod
    def __extract_features(complex_object_model, **kwargs):
        proteins = [complex_object_model.unbound_formation.ligand, complex_object_model.unbound_formation.receptor]
        for protein in proteins:
            # ResidueNeighbourhoodExtractor(protein).extract()
            # ProteinLengthExtractor(protein).extract()
            # AAOneHotExtractor(protein).extract()
            # ProfileExtractor(protein).extract()
            # ResidueIDExtractor(protein).extract()
            # ResidueDepthExtractor(protein, **kwargs).extract()
            # HalfSphereAminoAcidCompositionExtractor(protein, **kwargs).extract()
            # ProtrusionIndexExtractor(protein).extract()
            # StrideSecondaryStructureExtractor(protein).extract()
            # BValueExtractor(protein).extract()
            # D1PlainShapeDistributionExtractor(protein, **kwargs).extract()
            # D2PlainShapeDistributionExtractor(protein, **kwargs).extract()
            # D1SurfaceCategoryShapeDistributionExtractor(protein, **kwargs).extract()
            # D2SurfaceCategoryShapeDistributionExtractor(protein, **kwargs).extract()
            # D1SurfaceShapeDistributionExtractor(protein, **kwargs).extract()
            # D2SurfaceShapeDistributionExtractor(protein, **kwargs).extract()
            # D1CategoryShapeDistributionExtractor(protein, **kwargs).extract()
            # D2CategoryShapeDistributionExtractor(protein, **kwargs).extract()
            # D1MultiLevelShapeDistributionExtractor(protein, **kwargs).extract()
            # D2MultiLevelShapeDistributionExtractor(protein, **kwargs).extract()
            # DSSPSecondaryStructureExtractor(protein, **kwargs).extract()
            # PatchExtractor(protein, **kwargs).extract()
            FixedNeighbourhoodExtractor(protein, **kwargs).extract()
            ResidueFullIDExtractor(protein, **kwargs).extract()
            # PredictedSSExtractor(protein, **kwargs).extract()
            # D1SurfaceAtomsShapeDistributionExtractor(protein, **kwargs).extract()
            # D2SurfaceAtomsShapeDistributionExtractor(protein, **kwargs).extract()
        ExampleExtractor(complex_object_model).extract()
        ExampleIndexExtractor(complex_object_model).extract()
        OrderlessExampleExtractor(complex_object_model).extract()

    def __read_pdbs(self, the_complex):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", PDBConstructionWarning)
            ligand_bound = Protein(*PDB.read_pdb_file(self.directory + the_complex + "_l_b.pdb"))
            receptor_bound = Protein(*PDB.read_pdb_file(self.directory + the_complex + "_r_b.pdb"))
            ligand_unbound = Protein(*PDB.read_pdb_file(self.directory + the_complex + "_l_u.pdb"))
            receptor_unbound = Protein(*PDB.read_pdb_file(self.directory + the_complex + "_r_u.pdb"))
            bound_formation = ProteinPair(ligand_bound, receptor_bound)
            unbound_formation = ProteinPair(ligand_unbound, receptor_unbound)
            return ProteinComplex(the_complex, unbound_formation, bound_formation)

    def __get_pyml_dataset_graph(self, features, complexes, ratio, dataset_message):
        p = ProgressBar(dataset_message)
        pattern_ids = []
        examples = []
        n = len(complexes)
        total_residue_number = 0
        for i, complex_code in enumerate(complexes):
            if verbose:
                p.progress((1 + i) / float(n), custom_message=" (creating folds)")
            pos, neg = ExampleExtractor(None).load(complex_code)
            np.random.shuffle(neg)
            end_index = min(pos.shape[0] * ratio, neg.shape[0]) if ratio > 0 else neg.shape[0]
            e = np.vstack((pos, neg[:end_index, :]))
            examples.extend(e)
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueIDExtractor(None).load(protein)[0]
                pattern_ids.extend(residue_ids)
                total_residue_number += len(residue_ids)

        feature_size = NearestAATypeResidues(None, None).load_representation(complexes[0] + "_l_u").shape[1]
        # print(feature_size)
        final_vector = np.zeros((total_residue_number, feature_size))
        current_index = 0
        for complex_code in complexes:
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                feature_array = NearestAATypeResidues(None, None).load_representation(protein)
                end_index = current_index + feature_array.shape[0]
                final_vector[current_index:end_index, :] = feature_array
                current_index = end_index
        vector_dataset = VectorDataSet(final_vector, patternID=pattern_ids)
        vector_dataset.attachKernel('gaussian', gamma=0.5)
        p = PairDataSet(examples, data=vector_dataset)
        return p

    def __get_pyml_dataset(self, features, complexes, ratio, dataset_message, gap_dist=0, **kwargs):
        p = ProgressBar(dataset_message)
        pattern_ids = []
        examples = []
        c_list = []
        n = len(complexes)
        total_number_of_neg_examples = 0
        total_residue_number = 0
        for i, complex_code in enumerate(complexes):
            if verbose:
                p.progress((1 + i) / float(n), custom_message="(collecting examples)")
            pos, neg, pos_d, neg_d = ExampleExtractor(None).load(complex_code)
            neg_indices = range(len(neg))
            np.random.shuffle(neg_indices)
            neg = neg[neg_indices]
            neg_d = neg_d[neg_indices]
            if gap_dist != 0:
                neg = neg[np.where(neg_d > gap_dist)[0]]
            end_index = min(pos.shape[0] * ratio, neg.shape[0]) if ratio > 0 else neg.shape[0]
            total_number_of_neg_examples += end_index
            # these are the distances and will becomes soft-margin cs later on
            c_list.extend(pos_d.tolist())
            c_list.extend(neg_d[:end_index].tolist())
            e = np.vstack((pos, neg[:end_index]))
            examples.extend(e)
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueFullIDExtractor(None).load(protein)
                pattern_ids.extend(residue_ids)
                total_residue_number += len(residue_ids)
        feature_sizes = [(feature_dict[feature][0])(None).load(complexes[0] + "_l_u").shape[1] for feature in features]

        neg_ratio = total_number_of_neg_examples / float(len(examples))
        p_c0 = default_soft_margin_parameter * neg_ratio * 1000
        n_c0 = default_soft_margin_parameter * (1 - neg_ratio)
        for i, e in enumerate(examples):
            if e[1] == '+1':
                c_list[i] = p_c0
            else:
                c_list[i] = n_c0 * (1 - np.exp(-(c_list[i] - interaction_thr) / default_smooth_soft_margin_sigma))
        final_feature_vector = None
        for i, feature in enumerate(features):
            if verbose:
                p.progress((i + 1) / float(len(features)), custom_message="(vector repr.)")
            extractor, gamma, desc = feature_dict[feature]
            final_vector = np.zeros((total_residue_number, feature_sizes[i]))
            current_index = 0
            for complex_code in complexes:
                for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                    feature_array = extractor(None).load(protein)
                    end_index = current_index + feature_array.shape[0]
                    final_vector[current_index:end_index, :] = feature_array
                    current_index = end_index
            final_feature_vector = np.copy(final_vector) if i == 0 else np.hstack(
                (np.copy(final_feature_vector), np.copy(final_vector)))

        nan_indices = np.any(np.isnan(final_feature_vector), axis=1)
        non_nan_indices = list(np.nonzero(~nan_indices))[0]
        non_nan_pattern_ids = [pattern_ids[i] for i in non_nan_indices]
        nan_pattern_ids = set(pattern_ids) - set(non_nan_pattern_ids)
        n_nan = len(nan_pattern_ids)
        # Print.print_info("Number of residues with NaN in their feature representation {0}".format(n_nan))

        if n_nan > 0:
            ee = []
            for example in examples:
                [l, r] = example[0].split(":")
                if l not in nan_pattern_ids and r not in nan_pattern_ids:
                    ee.append(example)
            examples = ee

        final_feature_vector = final_feature_vector[non_nan_indices]
        final_feature_vector = normalize(final_feature_vector)

        vector_dataset = VectorDataSet(final_feature_vector, patternID=non_nan_pattern_ids)
        # vector_dataset.attachKernel('gaussian', gamma=kwargs['gamma'])
        p = PairDataSet(examples, data=vector_dataset)
        # p.registerAttribute('C', c_list)
        # print("")
        return p

    def __get_pyml_dataset_rep(self, name, features, complexes, ratio, dataset_message, sparsity=70):
        p = ProgressBar(dataset_message)
        pattern_ids = []
        examples = []
        n = len(complexes)
        total_residue_number = 0
        for i, complex_code in enumerate(complexes):
            if verbose:
                p.progress((1 + i) / float(n), custom_message="(creating folds)")
            pos, neg = ExampleExtractor(None).load(complex_code)
            np.random.shuffle(neg)
            end_index = min(pos.shape[0] * ratio, neg.shape[0]) if ratio > 0 else neg.shape[0]
            e = np.vstack((pos, neg[:end_index, :]))
            examples.extend(e)
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueIDExtractor(None).load(protein)[0]
                pattern_ids.extend(residue_ids)
                total_residue_number += len(residue_ids)

        transformed_rep_file = "{}{}-{}.npy".format(data_tmp_directory, name, sparsity)
        if not os.path.exists(transformed_rep_file):
            x = self.get_raw_vector(features, complexes, dataset_message="Extracting Residue Vectors")[0]
            # print("!!!!! x size : {}".format(x.shape))
            # sparse_ae = WTAAE(x, self.name, complexes, features, sparsity)
            # new_x = sparse_ae.encode(x)
            # print("!!!!! new_x size : {}".format(new_x.shape))
            # np.save(transformed_rep_file, new_x)
        vv = np.load(transformed_rep_file)
        # print("<<< vec:{} -- ids:{}".format(vv.shape, len(pattern_ids)))
        v = VectorDataSet(vv, patternID=pattern_ids)
        # print(v)
        v.attachKernel('gaussian', gamma=0.9)
        p = PairDataSet(examples, data=v)
        # print(p)
        return p

    @staticmethod
    def get_bags(features, complexes, ratio, gap=0, neighbours=False):
        complex_examples = {}
        i = 1.
        n_p_t = 0
        n_n_t = 0
        for a_complex in complexes:
            i += 1
            ligand = normalize(np.nan_to_num(np.hstack([feature_dict[f][0](None).load(a_complex + "_l_u") for f in
                                                        features])), axis=1)
            receptor = normalize(np.nan_to_num(np.hstack([feature_dict[f][0](None).load(a_complex + "_r_u") for f in
                                                          features])), axis=1)
            # print(ligand.shape[0]+receptor.shape[0])
            e = ExampleIndexExtractor(None).load(a_complex)
            p =  np.array(np.where(e[:, 2] == 1)[0])
            n = np.array(np.where(e[:, 2] == -1)[0]) # np.array(list(set(np.where(e[:, 2] == -1)[0]) & set(np.where(e[:, 3] > gap)[0])), dtype=np.int32) if ratio != -1 else
            # np.random.shuffle(n)
            n_d = list(e[np.array(np.where(e[:, 2] == -1)[0]), 3])
            n_m = np.zeros((n.shape[0], 2), dtype=np.int)
            n_m[:, 0] = n

            bins = np.digitize(n_d, np.linspace(6, np.max(n_d)))
            n_m[:, 1] = bins
            r = (p.shape[0]*ratio)/float(n.shape[0])
            neg_indices = []
            for i in range(max(bins)):
                indices = np.where(n_m[:, 1] ==i)[0]
                np.random.shuffle(indices)
                neg_indices.extend(n_m[indices[:int(len(indices)*r)], 0].tolist())

            n_p_t += p.shape[0]
            # n = n[neg_indices, :]
            # dd = e[n, 3]
            # h, bins = np.histogram(dd, bins=np.linspace(6, np.max(dd)))
            # print("\t".join(str(v) for v in bins.tolist()))
            # print("\t".join(str(v) for v in h.tolist()))
            # # distances.extend(dd)
            # # np.random.shuffle(n)
            #
            # # end_index = min(len(p) * ratio, len(n)) if ratio > 0 else len(n)
            # h, bins = np.histogram(e[neg_indices, 3], bins=np.linspace(6, np.max(dd)))
            # print("\t".join(str(v) for v in bins.tolist()))
            # print("\t".join(str(v) for v in h.tolist()))
            # print(list(bins))
            n_n_t += e[neg_indices].shape[0]
            examples = np.vstack((e[p], e[neg_indices])) if ratio != -1 else np.vstack((e[p], e[n]))
            if neighbours:
                l_n, r_n = FixedNeighbourhoodExtractor(None).load(a_complex + "_l_u"), FixedNeighbourhoodExtractor(None).load(a_complex + "_r_u")
                n_neighborhood = default_fixed_neighbours_number
                d = ligand.shape[1]
                ligand_n = np.zeros((ligand.shape[0], d * (n_neighborhood+1)))
                receptor_n = np.zeros((receptor.shape[0], d * (n_neighborhood+1)))
                ligand_n[:, :d] = ligand
                receptor_n[:, :d] = receptor
                for protein, neighbourhoods, patches in [(ligand, l_n, ligand_n), (receptor, r_n, receptor_n)]:
                    for i in range(protein.shape[0]):
                        counter = 1
                        for j in neighbourhoods[i, :]:
                            patches[i, counter*d:(counter+1)*d] = protein[int(j), :]
                            counter += 1
                complex_examples[a_complex] = (ligand_n, receptor_n, examples.astype(int))
            else:
                complex_examples[a_complex] = (ligand, receptor, examples.astype(int))
        # print("Number of positives {}".format(n_p_t))
        # print("Number of negatives {}".format(n_n_t))
        return complex_examples

    @staticmethod
    def impute_nans(x):
        nan_columns = np.where(np.isnan(x).any(axis=0))[0]
        non_nan_columns = np.where(~np.isnan(x).any(axis=0))[0]
        nan_rows = np.where(np.isnan(x).any(axis=1))[0]
        non_nan_rows = np.where(~np.isnan(x).any(axis=1))[0]
        if len(nan_rows) != 0:
            print(len(nan_rows))
        for i in nan_columns:
            estimator = linear_model.LinearRegression()
            estimator.fit(x[np.ix_(non_nan_rows, non_nan_columns)], x[np.ix_(non_nan_rows, [i])])
            for nan_row in nan_rows:
                if np.isnan(x[nan_row, i]):
                    x[nan_row, i] = estimator.predict(x[np.ix_([nan_row], non_nan_columns)])
        #
        # # The coefficients
        # print('Coefficients: \n', estimator.coef_)
        # # The mean squared error
        # print("Mean squared error: %.2f"
        #       % np.mean((estimator.predict(diabetes_X_test) - diabetes_y_test) ** 2))
        return x

    # @staticmethod
    # def get_bags(features, complexes, ratio, gap=0, neighbours=False):
    #     complex_examples = {}
    #     for a_complex in complexes:
    #         l_x = np.hstack([feature_dict[f][0](None).load(a_complex + "_l_u") for f in features])
    #         r_x = np.hstack([feature_dict[f][0](None).load(a_complex + "_r_u") for f in features])
    #         ligand = normalize(DBD5.impute_nans(l_x), axis=1)
    #         receptor = normalize(DBD5.impute_nans(r_x), axis=1)
    #         e = ExampleIndexExtractor(None).load(a_complex)
    #         p = np.array(np.where(e[:, 2] == 1)[0], dtype=np.int32)
    #         n = np.array(list(set(np.where(e[:, 2] == -1)[0]) & set(np.where(e[:, 3] > gap)[0])), dtype=np.int32)
    #         # np.random.shuffle(n)
    #         end_index = min(len(p) * ratio, len(n)) if ratio > 0 else len(n)
    #         examples = np.vstack((e[p], e[n[:end_index]]))
    #         if neighbours:
    #             n = ResidueNeighbourhoodExtractor(None)
    #             l_n, r_n = n.load(a_complex + "_l_u"), n.load(a_complex + "_r_u")
    #             complex_examples[a_complex] = (ligand, receptor, examples.astype(int), l_n, r_n)
    #         else:
    #             complex_examples[a_complex] = (ligand, receptor, examples.astype(int))
    #     return complex_examples

    def get_pairs(self, features, complexes, ratio, dataset_message=None, gap=False,
                  example_extractor=ExampleExtractor):
        if dataset_message is not None:
            p = ProgressBar(dataset_message + " complex-wise ")
        m = len(complexes)
        pk = PairwiseKernel()
        feature_sizes = sum(
            [(feature_dict[feature][0])(None).load(complexes[0] + "_l_u").shape[1] for feature in features])
        x_l = []
        y_l = []
        for i, complex_code in enumerate(complexes):
            if verbose and dataset_message is not None:
                p.progress((1. + i) / m, custom_message=" (collecting information)")
            ids = {}
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueFullIDExtractor(None).load(protein)
                n = len(residue_ids)
                final_vector = np.zeros((n, feature_sizes))
                current_index = 0
                for feature in features:
                    extractor = feature_dict[feature][0]
                    feature_array = extractor(None).load(protein)
                    end_index = current_index + feature_array.shape[1]
                    final_vector[:, current_index:end_index] = feature_array
                    current_index = end_index
                non_nan_indices = list(np.nonzero(~np.any(np.isnan(final_vector), axis=1)))[0]
                ids.update(dict(zip(list(residue_ids[non_nan_indices]), list(np.copy(final_vector[non_nan_indices])))))
            pos, neg, pos_d, neg_d = example_extractor(None).load(complex_code)
            if gap:
                neg = neg[neg_d > 30]
            n_examples = pos.shape[0] + pos.shape[0]
            neg_indices = range(len(neg))
            np.random.shuffle(neg_indices)
            end_index = min(pos.shape[0] * ratio, neg.shape[0]) if ratio > 0 else neg.shape[0]
            if len(pos) != 0:
                examples = np.vstack((neg[neg_indices[:end_index], :], pos))
            else:
                examples = neg[neg_indices[:end_index], :]
            indices = range(examples.shape[0])
            np.random.shuffle(indices)
            examples = examples[indices]
            # p = ProgressBar("{}/{} creating {} features rep".format(i+1, len(complexes), complex_code))
            for e_i, example in enumerate(examples):
                p1_id, p2_id = example[0].split(example_id_separator)
                label = int(example[1])
                if p1_id in ids and p2_id in ids:
                    x_l.append(normalize(pk.compute(ids[p1_id], ids[p2_id])).flatten())
                    y_l.append(label)
        return np.array(x_l), np.array(y_l).reshape(-1, 1)

    def get_raw_vector(self, features, complexes, dataset_message):
        p = ProgressBar(dataset_message)
        pattern_ids = []
        n = len(complexes)
        total_residue_number = 0
        for i, complex_code in enumerate(complexes):
            if verbose:
                p.progress((1 + i) / float(n), custom_message=" (collecting ids)")
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueIDExtractor(None).load(protein)[0]
                pattern_ids.extend(residue_ids)
                total_residue_number += len(residue_ids)

        feature_sizes = [(feature_dict[feature][0])(None).load(complexes[0] + "_l_u").shape[1] for feature in features]
        vectors = None
        for i, feature in enumerate(features):
            if verbose:
                p.progress((i + 1) / float(len(features)), custom_message=" (creating vectors)")
            extractor, gamma, _ = feature_dict[feature]
            final_vector = np.zeros((total_residue_number, feature_sizes[i]))
            current_index = 0
            for complex_code in complexes:
                for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                    feature_array = extractor(None).load(protein)
                    end_index = current_index + feature_array.shape[0]
                    final_vector[current_index:end_index, :] = feature_array
                    current_index = end_index
            vectors = np.copy(final_vector) if vectors is None else np.hstack((vectors, final_vector))
        # print("<<< vec:{} -- ids:{}".format(vectors.shape, len(pattern_ids)))
        return vectors, pattern_ids

    def get_pyml_train_test_rep(self, features, train_complexes, test_complexes):
        # StackedDenoisingAutoEncoder("").train(x)

        tr = self.__get_pyml_dataset_graph(features, train_complexes, 10, "Extracting WTA repr. for training")
        # print("after train")
        ts = self.__get_pyml_dataset_graph(features, test_complexes, 10, "Extracting WTA repr. for testing")
        return tr, ts

    def get_pyml_train_test(self, features, train_complexes, test_complexes, **kwargs):
        if verbose:
            Print.print_info("Creating the training and testing datasets")
        train = self.__get_pyml_dataset(features, train_complexes, 10, "\tTraining dataset", **kwargs)
        test = self.__get_pyml_dataset(features, test_complexes, -1, "\tTesting dataset", **kwargs)
        return train, test

    def get_pyml_cv_from_folds(self, features, comps, n_folds=5, ratio=10, gap=False, **kwargs):
        folds_dict, train, test = self.get_complex_folds_dict(comps, n_folds)
        pb = ProgressBar("Dataset for {0} fold CV".format(n_folds))
        pattern_ids = []
        examples = []
        n = len(comps)
        total_residue_number = 0
        for i, complex_code in enumerate(comps):
            if verbose:
                pb.progress((1 + i) / float(n), custom_message=" (creating folds)")
            pos, neg, _, neg_d = ExampleExtractor(None).load(complex_code)
            sel_neg = neg[np.where(neg_d > 10)[0]]
            np.random.shuffle(neg)
            e = np.vstack((pos, neg))
            examples.extend(e)
            for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                residue_ids = ResidueFullIDExtractor(None).load(protein)
                pattern_ids.extend(residue_ids)
                total_residue_number += len(residue_ids)

            for j in range(n_folds):
                if j == folds_dict[complex_code]:
                    test[j].extend(e[:, 0].tolist())
                else:
                    negs = sel_neg if gap else neg
                    train_e = np.vstack((pos, negs))[:pos.shape[0] * ratio, 0]
                    train[j].extend(train_e.tolist())
                    # train[j].extend(np.vstack((pos, sel_neg))[:, 0].tolist())
        feature_sizes = [(feature_dict[feature][0])(None).load(comps[0] + "_l_u").shape[1] for feature in features]
        final_feature_vector = None
        for i, feature in enumerate(features):
            if verbose:
                pb.progress((i + 1) / float(len(features)), custom_message=" (paired datasets)")
            extractor, gamma, _ = feature_dict[feature]
            feature_vector = np.zeros((total_residue_number, feature_sizes[i]))
            current_index = 0
            for complex_code in comps:
                for protein in [complex_code + "_l_u", complex_code + "_r_u"]:
                    feature_array = extractor(None).load(protein)
                    end_index = current_index + feature_array.shape[0]
                    feature_vector[current_index:end_index, :] = feature_array
                    current_index = end_index
            final_feature_vector = np.copy(feature_vector) if i == 0 else np.hstack(
                (np.copy(final_feature_vector), np.copy(feature_vector)))

        # removing examples and residues with nan in features
        nan_indices = np.any(np.isnan(final_feature_vector), axis=1)
        non_nan_indices = list(np.nonzero(~nan_indices))[0]
        non_nan_pattern_ids = [pattern_ids[i] for i in non_nan_indices]
        nan_pattern_ids = set(pattern_ids) - set(non_nan_pattern_ids)
        n_nan = len(nan_pattern_ids)
        Print.print_info("Number of residues with NaN in their feature representation {0}".format(n_nan))

        if n_nan > 0:
            ee = []
            for example in examples:
                [l, r] = example[0].split(":")
                if l not in nan_pattern_ids and r not in nan_pattern_ids:
                    ee.append(example)
            examples = ee

        final_feature_vector = final_feature_vector[non_nan_indices]
        final_feature_vector = normalize(final_feature_vector)

        vector_dataset = VectorDataSet(final_feature_vector, patternID=non_nan_pattern_ids)
        vector_dataset.attachKernel('gaussian', gamma=kwargs['gamma'])
        p = PairDataSet(examples, data=vector_dataset)
        print("")
        return p, train, test

    @staticmethod
    def get_complex_folds_dict(complexes, n_folds):
        folds_dict = {}
        n = len(complexes)
        folds_size = [n / n_folds] * n_folds
        for i in range(n - ((n / n_folds) * n_folds)):
            folds_size[i] += 1
        complex_counter = 0
        train = []
        test = []
        for f, fold_size in enumerate(folds_size):
            for i in range(fold_size):
                folds_dict[complexes[complex_counter]] = f
                complex_counter += 1
            train.append([])
            test.append([])
        return folds_dict, train, test
