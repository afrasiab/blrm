# -*- coding: utf-8 -*-
"""
Created on Sun Sep 22 22:24:09 2013
Get the list of indices of residues from ligand and receptor which are in contact in a single complex file
Code for extracting contacting pairs from the cid_l_u and cid_r_u pdb files given the complex file which contains only the chains in the unbound files but may exhibit symmetry
For hetero complexes only
@author: root
"""
from getPair import *


def getPosexFromPDB_inner(cfile, lfile, rfile, dthr=6.0, sthr=0.95):
    (_, cR, cpp, cseq, cS2Ri) = readPDB(cfile)
    (_, lR, _, lseq, lS2Ri) = readPDB(lfile)
    (_, rR, _, rseq, rS2Ri) = readPDB(rfile)
    cRdict = dict(zip(cR, range(len(cR))))
    B2U = np.zeros(len(cR))
    B2U[:] = np.nan
    for p in cpp:  # Process each chain (or its broken component) separately
        pS2Ri = [cRdict[r] for r in p]
        bs = p.get_sequence().tostring()
        # The current chain from the complex pdb file must have sufficient sequence identity to either the ligand or the receptor for its interactions to be useful
        rscore = Bio.pairwise2.align.globalxx(rseq, bs)[0][2] / np.min((len(rseq), len(bs)))
        lscore = Bio.pairwise2.align.globalxx(lseq, bs)[0][2] / np.min((len(lseq), len(bs)))
        print cfile, lfile, rfile, lscore, rscore

        # pdb.set_trace()
        if (lscore < sthr and rscore < sthr):
            continue
        print cfile, lfile, rfile, lscore, rscore
        _, rb2u = mapU2B(rseq, rS2Ri, len(rR), bs, pS2Ri, len(cR))
        _, lb2u = mapU2B(lseq, lS2Ri, len(lR), bs, pS2Ri, len(cR))
        l = 1

        if np.sum(np.isnan(rb2u)) < np.sum(
                np.isnan(lb2u)):  # Choose the alignment of the receptor if its better than that of the ligand
            b2u = rb2u
        else:
            b2u = lb2u
            l = -1
        B2U[pS2Ri] = l * (np.array(b2u)[pS2Ri] + 1)  # added 1 to prevent 0 and -0, ligand indices are made negative
    # pdb.set_trace()
    c = getCoords(cR)
    C = getDist(c, c, dthr)[0]
    P = []
    for (i, j, _) in C:
        try:
            xi = B2U[i]
            xj = B2U[j]
        except KeyError:
            continue
        if xi < 0 and xj > 0:  # one should belong to the ligand, the other to the receptor
            P.append((-xi - 1, xj - 1))
        elif xi > 0 and xj < 0:
            P.append((-xj - 1, xi - 1))
    P = np.unique(P)  # Remove any redundant examples
    # pdb.set_trace()
    return P  # the returning indices are wrt the unbound L.R and R.R (lig,rec)


def getPosexFromPDB(bdir, cid, dthr=6.0):
    cfile = bdir + cid + '.pdb'
    lfile = bdir + cid + '_l_u.pdb'
    rfile = bdir + cid + '_r_u.pdb'
    return getPosexFromPDB_inner(cfile, lfile, rfile, dthr)


if __name__ == "__main__":
    bdir = '../CAPRI/'
    cid = '3E8L'
    P = getPosexFromPDB(bdir, cid, dthr=6.0)
